<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Notifications\AccountResetPasswordNotification;

class AccountOfficer extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','phone_number', 'email',  'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password','remember_token',
    ];
  //Send password reset notification
  public function sendPasswordResetNotification($token)
  {
      $this->notify(new AccountResetPasswordNotification($token));
  }

  public  function enrolls(){
      return $this->hasMany('App\Enroll');
  }


}
