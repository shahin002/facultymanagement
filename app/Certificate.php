<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    protected $fillable = ['name', 'money', 'description'];

    public function certificateRequests()
    {
        return $this->hasMany('App\CertificateRequest');
    }
}
