<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CertificateRequest extends Model
{
    public function certificate(){
        return $this->belongsTo('App\Certificate');
    }
    public function student(){
        return $this->belongsTo('App\Student');
    }
}
