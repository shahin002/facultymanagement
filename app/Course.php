<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function enrolls(){
        return $this->belongsToMany('App\Enroll','course_enroll');
    }
    public function courseEnrolls(){
        return $this->hasMany('App\CourseEnroll');
    }
    public function department(){
        return $this->belongsTo('App\Department');
    }
    /*public function practical(){
        return $this->hasOne('App\PracticalTeacher');
    }
    public function theoretical(){
        return $this->hasOne('App\TheoriticalTeacher');
    }*/
}
