<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseEnroll extends Model
{
    protected $table='course_enroll';

    public function course(){
        return $this->belongsTo('App\Course');
    }
    public function enroll(){
        return $this->belongsTo('App\Enroll');
    }
}
