<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Notifications\AdminResetPasswordNotification;

class DeanOfficer extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'designation','phone_number', 'email',  'password',
    ];
    public function getEmailForPasswordReset()
    {
        return $this->email;
    }


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password','remember_token',
    ];
  //Send password reset notification
  public function sendPasswordResetNotification($token)
  {
      $this->notify(new AdminResetPasswordNotification($token));
  }


}
