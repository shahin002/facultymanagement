<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public function chairman(){
        return $this->hasOne('App\DepartmentChairman');
    }
}
