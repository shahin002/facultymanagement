<?php
namespace App;

use App\Notifications\ChairmanResetPasswordNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class DepartmentChairman extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','phone_number', 'email',  'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password','remember_token',
    ];

    public function department(){
        return $this->belongsTo('App\Department');
    }
  //Send password reset notification
   public function sendPasswordResetNotification($token)
   {
       $this->notify(new ChairmanResetPasswordNotification($token));
   }


}
