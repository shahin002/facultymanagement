<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enroll extends Model
{
    //
    public function enrollSemister(){
        return $this->belongsTo('App\EnrollSemister');
    }

    public function courses(){
        return $this->belongsToMany('App\Course','course_enroll');
    }

    public function student(){
        return $this->belongsTo('App\Student');
    }

    public function accountOfficer(){
        return $this->belongsTo('App\AccountOfficer');
    }
}
