<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnrollSemister extends Model
{
    //
    public function enrolls(){
        return $this->hasMany('App\Enroll');
    }
}
