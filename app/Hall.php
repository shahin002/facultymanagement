<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hall extends Model
{
    //
    public function hallOfficer(){
        return $this->belongsTo('App\HallOfficer');
    }
    public function students(){
        return $this->hasMany('App\Student');
    }
}
