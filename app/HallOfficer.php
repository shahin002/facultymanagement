<?php

namespace App;

use App\Notifications\ProvostResetPasswordNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class HallOfficer extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','phone_number', 'email',  'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password','remember_token',
    ];
  //Send password reset notification
   public function sendPasswordResetNotification($token)
   {
       $this->notify(new ProvostResetPasswordNotification($token));
   }

public function hall(){
    return $this->hasOne('App\Hall');
}
}
