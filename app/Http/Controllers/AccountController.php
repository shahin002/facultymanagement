<?php

namespace App\Http\Controllers;
use App\Enroll;
use App\Course;
use App\AccountOfficer;
use App\EnrollSemister;
use App\CourseMoney ;
use App\traits\getCurrentEnrollSemister;
use App\traits\signature;
use Image;


use Illuminate\Http\Request;

class AccountController extends Controller
{
    use getCurrentEnrollSemister, signature;
    public function __construct()
    {
        $this->middleware('accountLogin');
        if (!$this->getCurrentEnrollSemister()) {
            echo 'Dean Officer does not add Current Semister. Please Contact with Dean Officer';
            exit();
        }
    }

    //Account homepage
    public function index(){
        $semester = $this->getLastEnrollSemister();
        if($semester){
            $enrolls = Enroll::where('enroll_semister_id',$semester->id)->with('student')->get();
        }else{
            $enrolls = false;
        }

        return view('users.account.dashboard.home', compact('enrolls','semester'));
    }
            //manage Enrolls
    public function manageEnroll($semesterId){
        $semester = $this->getLastEnrollSemister();
        if($semester){
            $enrolls = Enroll::where('enroll_semister_id',$semester->id)->where('semister',$semesterId)->with('student')->get();
        }else{
            $enrolls = false;
        }

        return view('users.account.dashboard.home', compact('enrolls','semester','semesterId'));


    }
    //view Edit Enroll Form
    public function viewEditEnrollForm(Enroll $enroll){
        $receipts = CourseMoney::where('enroll_id', $enroll->id)->get();

        return view('users.account.dashboard.accountStatus', compact('enroll', 'receipts'));
    }

    //change Enroll Status
    public function changeEnrollStatus(Enroll $enroll){
        if($enroll->account_status== 1){
            $enroll->account_status = 0;
            $enroll->account_officer_id = null;
            $mgs = "Enroll Disapproved";
        }else{
            $enroll->account_status = 1;
            $enroll->account_officer_id = auth('account')->user()->id;
            $mgs = "Enroll Approved";
        }
        $enroll->save();
        return redirect()->back()->with('mgs', $mgs) ;
    }


    //Update Login info form
    public function profileForm(){
        return view('users/account/dashboard/editProfile');
    }

    //add signature form
    public  function showAddSignatureForm(){
        return view('users.account.dashboard.signature');
    }

    //add signature
    public function addSignature(Request $request){
        $account = auth('account')->user();
        $this->signature($request, $account);
        return redirect(route('account.home'))->with('mgs','Signature Uploaded!');
    }

    //Update  login info
    public function updateProfile(Request $request){
        $this->validate($request, array(
            'email' => 'required',
            'password' => 'required|min:6|confirmed',
        ));
        $student = auth('account')->user();
        $emails = AccountOfficer::select('email')->get();
        foreach($emails as $email){
            if($request->email == $email->email and $student->email != $email->email){
            return redirect()->back()->with('mgsErr','Email not Available') ;

            }
        }
            $student->email = $request->email;
            $student->password = bcrypt($request->password);
            if($student->save()){
                return redirect(route('account.home'))->with('mgs','Successfully Updates!') ;
            }
        
    }


}

