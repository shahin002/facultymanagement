<?php


namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//Trait
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;

class AccountForgotController extends Controller
{
    //Sends Password Reset emails
    use SendsPasswordResetEmails;
    

    public function showResetForm()
    {
        return view('users/account/auth/sendReset');
    }


    public function __construct()
    {
        $this->middleware('guestAccount');
    }
    public function broker()
    {
         return Password::broker('account');
    }
       protected function sendResetLinkResponse($response)
    {
        return redirect()->back()->with('status', trans($response))->with('mgs','Reset Code Sent!');
    }
}