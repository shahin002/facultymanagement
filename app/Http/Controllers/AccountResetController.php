<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;

class AccountResetController extends Controller
{
    //trait for handling reset Password
    use ResetsPasswords;
    protected $redirectTo = '/account';

    public function showResetForm(Request $request, $token = null)
    {

        return view('users.account.auth.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    public function broker()
    {
        return Password::broker('account');
    }

    protected function guard()
    {
        return Auth::guard('account');
    }
}