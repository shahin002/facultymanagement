<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ChairmanForgotController extends Controller
{
    //Sends Password Reset emails
    use SendsPasswordResetEmails;


    public function showResetForm()
    {
        return view('users.chairman..auth.sendReset');
    }


    public function __construct()
    {
        $this->middleware('guestDepartmentChairman');
    }
    public function broker()
    {
        return Password::broker('chairman');
    }
    protected function sendResetLinkResponse($response)
    {
        return redirect()->back()->with('status', trans($response))->with('mgs','Reset Code Sent!');
    }
}