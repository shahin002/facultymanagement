<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Auth;

class ChairmanResetController extends Controller
{
    //trait for handling reset Password
    use ResetsPasswords;
    protected $redirectTo = '/chairman';

    public function showResetForm(Request $request, $token = null)
    {

        return view('users.chairman.auth.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    public function broker()
    {
        return Password::broker('chairman');
    }

    protected function guard()
    {
        return Auth::guard('dhead');
    }
}