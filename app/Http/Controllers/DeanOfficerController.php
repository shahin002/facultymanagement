<?php

namespace App\Http\Controllers;

use App\AccountOfficer;
use App\Certificate;
use App\CertificateRequest;
use App\CourseEnroll;
use App\PracticalTeacher;
use App\Setting;
use App\StudentCourseMark;
use App\Teacher;
use App\TheoriticalTeacher;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Image;
use PDF;
use Illuminate\Http\Request;
use App\EnrollSemister;
use App\Student;
use App\DeanOfficer;
use App\StudentInfo;
use App\Enroll;
use App\Course;
use App\traits\getCurrentEnrollSemister;
use App\traits\semisterToName;
use App\traits\signature;
use App\HallOfficer;
Use App\Hall;
use App\Department;
use App\DepartmentChairman;
use Illuminate\Support\Facades\Auth;


class DeanOfficerController extends Controller
{
    use getCurrentEnrollSemister, semisterToName, signature;

    public function __construct()
    {
        $this->middleware('adminLogin');
    }

    //admin homepage
    public function index()
    {
        $data = array();
        $data['exam_cntroller'] = @(new Setting())->get('exam_controller');
        $data['dean'] = @(new Setting())->get('dean');
        return view('users.deanOfficer.dashboard.home')->with($data);
    }

    public function showAddSemisterForm()
    {
        return view('users.deanOfficer.dashboard.addSemister');
    }

    public function saveEnrollSemister(Request $request)
    {
        if ($e = EnrollSemister::orderBy('id', 'desc')->first()) {
            if ($e->year >= date('Y') and $e->month >= $request->month) {
                return redirect()->back()->with('mgsErr', 'Semister Allready Exist!');
            }
        }

        $this->validate($request, array(
            'year' => 'required',
            'month' => 'required',
        ));

        $enrollSemister = new EnrollSemister();

        $enrollSemister->year = date('Y');
        $enrollSemister->month = $request->month;
        $enrollSemister->session = 0;
        $enrollSemister->status = 1;
        $enrollSemister->admit_status = 0;
        $enrollSemister->save();
        return redirect(route('view.semister'))->with('mgs', 'Semester Successfully Added!');
    }

    //view current semester
    public function viewEnrollSemister()
    {
        $enrollSemister = $this->getLastEnrollSemister();
        //checking if any enrollment in database
        if (!$enrollSemister) {
            return redirect(route('admin.home'))->with('mgsErr', 'No Semsiter Running right now!');
        }

        return view('users.deanOfficer.dashboard.currentSemister', compact('enrollSemister'));
    }

    // Update EnrollSemister Status
    public function updateEnrollSemisterStatus(EnrollSemister $semister)
    {
        if ($semister->status == 1) {
            $semister->status = 0;
            $semister->save();
            return redirect(route('view.semister'))->with('mgs', 'Enrollment End Successfully!');
        } else {
            $semister->status = 1;
            $semister->save();
            return redirect(route('view.semister'))->with('mgs', 'Enrollment Start Successfully!');
        }


    }

    //Set Final Examination Date
    public function setExamDate(EnrollSemister $semister, Request $request)
    {
        $this->validate($request, array(
            'exam_date' => 'required'
        ));
        $semister->exam_date = $request->exam_date;
        $semister->save();
        return redirect()->back()->with('mgs', 'Exam Date Added!');
    }

    //update admit card download status
    public function updateAdmitStatus(EnrollSemister $semister)
    {
        if ($semister->admit_status == 1) {
            $semister->admit_status = 0;
            $semister->save();
            return redirect(route('view.semister'))->with('mgs', 'Admit Card Download End!');
        } else {
            $semister->admit_status = 1;
            $semister->save();
            return redirect(route('view.semister'))->with('mgs', 'Now Students can Download Admit Card!');
        }
    }

    //manage Students
    public function manageStudent()
    {
        $students = Student::orderBy('id', 'desc')->get();
        return view('users.deanOfficer.dashboard.manageStudent', compact('students'));
    }

    //Add Students
    public function addStudent()
    {
        $halls = Hall::all();
        return view('users.deanOfficer.dashboard.addStudent', compact('halls'));
    }

    //Save Students
    public function saveStudent(Request $request)
    {

        $this->validate($request, array(
            'name' => 'required|string|max:255',
//            'father_name' => 'required|string|max:255',
//            'mother_name' => 'required|string|max:255',
            'roll_no' => 'required',
            'reg_no' => 'required',
            'hall_id' => 'required',
            'session' => 'required|string|max:255',
            'gender' => 'required',
//            'phone_number' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:students',
            'password' => 'required|string|min:6|confirmed',
        ));
//        dd($request->all());
        $student = Student::create([
            'name' => $request['name'],
//            'father_name' => $request['father_name'],
//            'mother_name' => $request['mother_name'],
            'roll_no' => $request['roll_no'],
            'reg_no' => $request['reg_no'],
            'hall_id' => $request['hall_id'],
            'session' => $request['session'],
            'gender' => $request['gender'],
//            'phone_number' => $request['phone_number'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
        ]);
        if ($student) {
            return redirect()->route('manage.student')->with('mgs', 'Student Added Successfully!');
        } else {
            return redirect()->back()->with('mgsErr', 'Error In adding Student Student');

        }
    }

    //view single Student form by id
    public function viewStudent(Student $student)
    {
        $halls = Hall::all();
        return view('users.deanOfficer.dashboard.viewStudent', compact('student', 'halls'));
    }

    //update student 
    public function updateStudent(Student $student, Request $request)
    {
        if ($student->studentInfo) {
            $info = StudentInfo::find($student->studentInfo->id);
//            dd($info);
            $this->validate($request, array(
                'father_name' => 'required',
                'mother_name' => 'required',
                'phone_number' => 'required',
                'ssc_roll' => 'required',
                'ssc_reg' => 'required',
                'ssc_gpa' => 'required',
                'ssc_institute' => 'required',
                'ssc_board' => 'required',
                'ssc_year' => 'required',
                'hsc_roll' => 'required',
                'hsc_reg' => 'required',
                'hsc_gpa' => 'required',
                'hsc_institute' => 'required',
                'hsc_board' => 'required',
                'hsc_year' => 'required',
            ));
            $info->father_name = $request->father_name;
            $info->mother_name = $request->mother_name;
            $info->phone_number = $request->phone_number;
            $info->ssc_roll = $request->ssc_roll;
            $info->ssc_reg = $request->ssc_reg;
            $info->ssc_gpa = $request->ssc_gpa;
            $info->ssc_institute = $request->ssc_institute;
            $info->ssc_board = $request->ssc_board;
            $info->ssc_year = $request->ssc_year;
            $info->hsc_roll = $request->hsc_roll;
            $info->hsc_reg = $request->hsc_reg;
            $info->hsc_gpa = $request->hsc_gpa;
            $info->hsc_institute = $request->hsc_institute;
            $info->hsc_board = $request->hsc_board;
            $info->hsc_year = $request->hsc_year;
            $info->save();
        }
        $this->validate($request, array(
            'name' => 'required',
            'roll_no' => 'required',
            'reg_no' => 'required',
            'hall_id' => 'required',
            'session' => 'required',
            'gender' => 'required',
            'email' => 'required|string|email|max:255',
        ));
        $student->name = $request->name;
        $student->roll_no = $request->roll_no;
        $student->reg_no = $request->reg_no;
        $student->hall_id = $request->hall_id;
        $student->session = $request['session'];
        $student->gender = $request->gender;
        $student->email = $request->email;
        $student->save();
        return redirect()->back()->with('mgs', 'Student Information Successufully Updated!');
    }

    //manage Enrolls
    public function manageEnroll($semesterId)
    {
//        dd($semesterId);
        $semester = $this->getLastEnrollSemister();
        if ($semester) {
            $enrolls = Enroll::where('enroll_semister_id', $semester->id)->where('semister', $semesterId)->with('student')->get();
        } else {
            return redirect()->back()->with('mgsErr', 'Enrollment is not Starting Yet!');
        }

        return view('users.deanOfficer.dashboard.manageenroll', compact('enrolls', 'semester', 'semesterId'));

    }

    //

    //view single enroll form by id
    public function viewEnroll(Enroll $enroll)
    {
        $semester = $this->semisterToName($enroll->semister);
        $adminSignature = DeanOfficer::find(1)->signature;
        return view('users.deanOfficer.dashboard.viewEnroll', compact('enroll', 'semester', 'adminSignature'));
    }

    //view Edit Enroll Form
    public function viewEditEnrollForm(Enroll $enroll)
    {
        return view('users.deanOfficer.dashboard.editEnrollCourse', compact('enroll'));
    }

    //get course
    public function getCourses()
    {
        $enrollSemister = $this->getLastEnrollSemister();
        if ($enrollSemister->month == 0) {
            $courses = Course::whereNotIN('semister', [2, 4, 6, 8])->get();
        } else {
            $courses = Course::whereNotIN('semister', [1, 3, 5, 7])->get();
        }
        return $courses;
    }

    //update Enroll
    public function updateEnroll(Enroll $enroll, Request $request)
    {
        //validate form data
        $this->validate($request, array(
            'previous_gpa' => 'required',
            'cgpa' => 'required',
            'student_status' => 'required',
        ));

        //Updating data to database
        $enroll->previous_gpa = $request->previous_gpa;
        $enroll->cgpa = $request->cgpa;
        $enroll->student_status = $request->student_status;

        if ($request->courses) {
            $this->validate($request, array(
                'semister' => 'required',
            ));
            $enroll->semister = $request->semister;
            $enroll->courses()->sync($request->courses, true);
            $enroll->has_retake = 0;
            foreach ($enroll->courses as $course) {
                if ($course->semister < $enroll->semister) {
                    $enroll->has_retake = 1;
                    break;
                }
            }
        }

        $enroll->save();
        return redirect(route('manage.enroll', $enroll->semister))->with('mgs', "Successfully Upadated!");
    }

    public function manageCourse()
    {
        $courses = Course::orderBy('semister', 'asc')->get();
        $depts = Department::all();
        return view('users.deanOfficer.dashboard.manageCourse', compact('courses', 'depts'));
    }

    //save course
    public function saveCourse(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required',
            'code' => 'required',
            'department_id' => 'required',
            'type' => 'required',
            'credit' => 'required',
            'semister' => 'required',
        ));
        $course = new Course();
        $course->department_id = $request->department_id;
        $course->name = $request->name;
        $course->code = $request->code;
        $course->type = $request->type;
        $course->credit = $request->credit;
        $course->semister = $request->semister;
        $course->save();
        return redirect()->back()->with('mgs', 'Course Successfully Added!');
    }

    //show edit course form
    public function viewEditCourseForm(Course $course)
    {
        return view('users.deanOfficer.dashboard.editCourse', compact('course'));
    }

    //update Course
    public function updateCourse(Course $course, Request $request)
    {
        $this->validate($request, array(
            'name' => 'required',
            'code' => 'required',
            'type' => 'required',
            'credit' => 'required',
            'semister' => 'required',
        ));

        $course->name = $request->name;
        $course->code = $request->code;
        $course->type = $request->type;
        $course->credit = $request->credit;
        $course->semister = $request->semister;
        $course->save();
        return redirect(route('manage.course'))->with('mgs', 'Course Successfully Updated!');
    }

//    accoutn officers list
    public function accountOfficerList()
    {
        $data = array();
        $data['accountOfficers'] = AccountOfficer::all();
        return view('users.deanOfficer.dashboard.account_officer_list')->with($data);
    }

    //add account officer form
    public function showCreateAccountForm()
    {
        return view('users/deanOfficer/dashboard/addAccount');

    }

    // save new Account Officer
    public function createAccount(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required',
            'phone_number' => 'required',
            'email' => 'required|unique:account_officers',
            'password' => 'required'
        ));

        $hallOfficer = new AccountOfficer();

        $hallOfficer->name = $request->name;
        $hallOfficer->phone_number = $request->phone_number;
        $hallOfficer->email = $request->email;
        $hallOfficer->password = bcrypt($request->password);
        $hallOfficer->save();
        return redirect()->back()->with('mgs', 'New Account Officer added successfully!');
    }


    //add new Hall Officer form
    public function showCreateProvostForm()
    {
        return view('users/deanOfficer/dashboard/addHallOfficer');
    }

    // save new Hall Officer
    public function createProvost(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required',
            'phone_number' => 'required',
            'email' => 'required',
            'password' => 'required'
        ));

        $hallOfficer = new HallOfficer();

        $hallOfficer->name = $request->name;
        $hallOfficer->phone_number = $request->phone_number;
        $hallOfficer->email = $request->email;
        $hallOfficer->password = bcrypt($request->password);
        $hallOfficer->save();
        return redirect()->back()->with('mgs', 'New Hall Provost added successfully!');
    }

    //view all Hall Officer
    public function viewProvost()
    {
        $hallOfficers = HallOfficer::all();
        return view('users/deanOfficer/dashboard/viewHallOfficer', compact('hallOfficers'));
    }

    //manage Hall
    public function manageHall()
    {
        $halls = Hall::all();
        return view('users/deanOfficer/dashboard/manageHall', compact('halls'));
    }

    //show hall Edit form
    public function showHallEditForm(Hall $hall)
    {
        $hallOfficers = HallOfficer::all();
        return view('users/deanOfficer/dashboard/editHall', compact('hall', 'hallOfficers'));
    }

    //Updating Hall Information
    public function updateHall(Hall $hall, Request $request)
    {
        $this->validate($request, array(
            'name' => 'required',
            'hall_officer_id' => 'required'
        ));
        $hall->name = $request->name;
        $hall->hall_officer_id = $request->hall_officer_id;
        $hall->save();
        return redirect(route('manage.hall'))->with('mgs', 'Hall Information Updated!');
    }

    //department List
    public function departmentList()
    {
        $data = array();
        $data['departments'] = Department::all();
        return view('users.deanOfficer.dashboard.dept_list')->with($data);
    }

    //add department form
    public function showCreateDepartmentForm()
    {
        return view('users/deanOfficer/dashboard/addDepartment');
    }

    //creating a new Department
    public function createDepartment(Request $request)
    {
        $this->validate($request, array(
            'dept_name' => 'required',
            'sort_name' => 'required',
            'chairman_name' => 'required',
            'phone_number' => 'required',
            'email' => 'required|unique:department_chairmen',
            'password' => 'required|string|min:6|',
        ));

        $department = new Department();
        $department->name = $request->dept_name;
        $department->sort_name = $request->sort_name;
        $department->save();

        $chairman = new DepartmentChairman();
        $chairman->name = $request->chairman_name;
        $chairman->phone_number = $request->phone_number;
        $chairman->email = $request->email;
        $chairman->password = bcrypt($request->password);
        $chairman->department_id = $department->id;
        $chairman->save();

        return redirect()->back()->with('mgs', $department->name . ' Department Added Successfully!');
    }

//    edit profile
    public function editProfileForm()
    {
        return view('users.deanOfficer.dashboard.editProfileForm');
    }

    public function updateOfficerProfile(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:dean_officers,email,' . Auth::guard('admin')->user()->id,
            'designation' => 'required',
            'phone_number' => 'required',

        ]);
        $user = Auth::guard('admin')->user();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->designation = $request->designation;
        $user->phone_number = $request->phone_number;
        if ($user->save()) {
            return redirect(route('admin.home'))->with('mgs', 'Successfully Updates!');
        }
    }

    //Update Login info form
    public function profileForm()
    {
        return view('users/deanOfficer/dashboard/editProfile');
    }

    //Update  login info
    public function updateProfile(Request $request)
    {
        $this->validate($request, array(
            'email' => 'required',
            'password' => 'required|min:6|confirmed',
        ));
        $admin = auth('admin')->user();
        $emails = DeanOfficer::select('email')->get();
        foreach ($emails as $email) {
            if ($request->email == $email->email and $admin->email != $email->email) {
                return redirect()->back()->with('mgsErr', 'Email not Available');

            }
        }
        $admin->email = $request->email;
        $admin->password = bcrypt($request->password);
        if ($admin->save()) {
            return redirect(route('admin.home'))->with('mgs', 'Successfully Updates!');
        }

    }

    //add signature form
    public function showAddSignatureForm()
    {
        return view('users.deanOfficer.dashboard.signature');
    }

    //add signature
    public function addSignature(Request $request)
    {
        $admin = auth('admin')->user();
        $this->signature($request, $admin);
        return redirect(route('admin.home'))->with('mgs', 'Signature Uploaded!');
    }

    /*Theory Course by Semester Id*/
    public function theoryCourseTeacherBySemesterId($semesterId)
    {
        $semister = $this->getLastEnrollSemister();
        if ($semister) {
            $teachers = TheoriticalTeacher::where('enroll_semister_id', $semister->id)->with(['course' => function ($query) use ($semesterId) {
                $query->where('semister', $semesterId);
            }])->get()->where('course', '!=', null);
            return view('users.deanOfficer.dashboard.courseTeachers.viewTheoreticalTeacher', compact('semister', 'teachers', 'semesterId'));
        } else {
            return back()->with('mgsErr', 'Please Start Semister First');
        }

    }
    /*End Theory Course by Semester Id*/

    /*Theory Course by prev Semester Id*/
    public function prevtheoryCourseTeacherBySemesterId($semesterId)
    {
        $semister = $this->getPrevEnrollSemister();
        if ($semister) {
            $teachers = TheoriticalTeacher::where('enroll_semister_id', $semister->id)->with(['course' => function ($query) use ($semesterId) {
                $query->where('semister', $semesterId);
            }])->get()->where('course', '!=', null);
            return view('users.deanOfficer.dashboard.prev.viewTheoreticalTeacher', compact('semister', 'teachers', 'semesterId'));
        } else {
            return back()->with('mgsErr', 'No Prev Semester Available');
        }

    }
    /*End Theory Course by Semester Id*/

    /*Practical Course by Semester Id*/
    public function practicalCourseTeacherBySemesterId($semesterId)
    {
        $semister = $this->getLastEnrollSemister();
        if ($semister) {
            $teachers = PracticalTeacher::where('enroll_semister_id', $semister->id)->with(['course' => function ($query) use ($semesterId) {
                $query->where('semister', $semesterId);
            }])->get()->where('course', '!=', null);

            return view('users.deanOfficer.dashboard.courseTeachers.viewIntPracticalTeacher', compact('semister', 'teachers', 'semesterId'));
        } else {
            return back()->with('mgsErr', 'Please Start Semister First');
        }

    }
    /*End Practical Course by Semester Id*/
    /*Practical Course by Prev Semester Id*/
    public function prevpracticalCourseTeacherBySemesterId($semesterId)
    {
        $semister = $this->getPrevEnrollSemister();
        if ($semister) {
            $teachers = PracticalTeacher::where('enroll_semister_id', $semister->id)->with(['course' => function ($query) use ($semesterId) {
                $query->where('semister', $semesterId);
            }])->get()->where('course', '!=', null);

            return view('users.deanOfficer.dashboard.prev.viewIntPracticalTeacher', compact('semister', 'teachers', 'semesterId'));
        } else {
            return back()->with('mgsErr', 'No Prev Semester Available');
        }

    }
    /*End Practical Course by Prev Semester Id*/

    /*View Students Marks*/
    public function viewStudentsMark($course_id)
    {
        $semester = $this->getLastEnrollSemister();
        $course = Course::where('id', $course_id)->with('enrolls.student')->first();


        $courseEnrolls = $course->enrolls->where('enroll_semister_id', $semester->id);
        $courseEnrolls->each(function ($item, $key) use ($course, $semester) {
            $marks = StudentCourseMark::where('student_id', $item->student_id)->where('course_id', $course->id)->where('enroll_semister_id', $semester->id)->first();
            $item->marks = $marks;
        });
        $data['courseEnrolls'] = $courseEnrolls->sortBy('student.roll_no');
        $data['course'] = $course;
        return view('users.deanOfficer.dashboard.studentsMarksView.studentmarksview')->with($data);
    }

    /*View Prev Students Marks*/
    public function prevviewStudentsMark($course_id)
    {
        $semester = $this->getPrevEnrollSemister();
        if ($semester) {
            $course = Course::where('id', $course_id)->with('enrolls.student')->first();


            $courseEnrolls = $course->enrolls->where('enroll_semister_id', $semester->id);
            $courseEnrolls->each(function ($item, $key) use ($course, $semester) {
                $marks = StudentCourseMark::where('student_id', $item->student_id)->where('course_id', $course->id)->where('enroll_semister_id', $semester->id)->first();
                $item->marks = $marks;
            });
            $data['courseEnrolls'] = $courseEnrolls->sortBy('student.roll_no');
            $data['course'] = $course;
            return view('users.deanOfficer.dashboard.studentsMarksView.studentmarksview')->with($data);
        } else {
            return back()->with('mgsErr', 'No Prev Semester Available');
        }

    }

    /*Download Students Marks*/
    public function downloadStudentsMark($course_id)
    {
        $semester = $this->getLastEnrollSemister();

        $course = Course::where('id', $course_id)->with('enrolls.student')->first();
        $semesterName = $this->semisterToName($course->semister);
        if ($course->type == 'Theoretical') {
            $courseTeachers = TheoriticalTeacher::where('enroll_semister_id', $semester->id)->where('course_id', $course_id)->with('theoryAssignedTeachers.teacher')->first();
            $courseTeachers = $courseTeachers->theoryAssignedTeachers->pluck('teacher.signature');
        } else {
            $courseTeachers = PracticalTeacher::where('enroll_semister_id', $semester->id)->where('course_id', $course_id)->with('practicalAssignedTeacher.teacher')->first();
            $courseTeachers = $courseTeachers->practicalAssignedTeacher->pluck('teacher.signature');
        }
        $courseEnrolls = $course->enrolls->where('enroll_semister_id', $semester->id);
        $courseEnrolls->each(function ($item, $key) use ($course, $semester) {
            $marks = StudentCourseMark::where('student_id', $item->student_id)->where('course_id', $course->id)->where('enroll_semister_id', $semester->id)->first();
            $item->marks = $marks;
        });
        $data['courseTeachers'] = $courseTeachers;
        $data['courseEnrolls'] = $courseEnrolls->sortBy('student.roll_no');
        $data['course'] = $course;
        $data['semesterName'] = $semesterName;
        $data['semester'] = $semester;
        $view = view('pdf.students_mark')->with($data)->render();
        $pdf = PDF::loadHTML($view, [
            'format' => 'Legal'
        ]);
        return $pdf->stream(time() . 'students_mark.pdf');
    }

    /*Download Prev Students Marks*/
    public function prevdownloadStudentsMark($course_id)
    {
        $semester = $this->getPrevEnrollSemister();
        if ($semester) {
            $course = Course::where('id', $course_id)->with('enrolls.student')->first();
            $semesterName = $this->semisterToName($course->semister);
            if ($course->type == 'Theoretical') {
                $courseTeachers = TheoriticalTeacher::where('enroll_semister_id', $semester->id)->where('course_id', $course_id)->with('theoryAssignedTeachers.teacher')->first();
                $courseTeachers = $courseTeachers->theoryAssignedTeachers->pluck('teacher.signature');
            } else {
                $courseTeachers = PracticalTeacher::where('enroll_semister_id', $semester->id)->where('course_id', $course_id)->with('practicalAssignedTeacher.teacher')->first();
                $courseTeachers = $courseTeachers->practicalAssignedTeacher->pluck('teacher.signature');
            }
            $courseEnrolls = $course->enrolls->where('enroll_semister_id', $semester->id);
            $courseEnrolls->each(function ($item, $key) use ($course, $semester) {
                $marks = StudentCourseMark::where('student_id', $item->student_id)->where('course_id', $course->id)->where('enroll_semister_id', $semester->id)->first();
                $item->marks = $marks;
            });
            $data['courseTeachers'] = $courseTeachers;
            $data['courseEnrolls'] = $courseEnrolls->sortBy('student.roll_no');
            $data['course'] = $course;
            $data['semesterName'] = $semesterName;
            $data['semester'] = $semester;
            $view = view('pdf.students_mark')->with($data)->render();
            $pdf = PDF::loadHTML($view, [
                'format' => 'Legal'
            ]);
            return $pdf->stream(time() . 'students_mark.pdf');
        } else {
            return back()->with('mgsErr', 'No Prev Semester Available');
        }

    }

    public function cancelStudentsMarkReview($course_id)
    {
        $semester = $this->getLastEnrollSemister();
        $course = Course::find($course_id);
        if ($course->type == "Practical") {
            $security = PracticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course_id)
                ->first();
        } else {
            $security = TheoriticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course_id)
                ->first();
        }
        if ($security) {
            if ($security->status == 2) {
                $security->status = 0;
                $security->save();
                $msg = 'Students Marks Cancelled';
            } else {
                $msg = 'Something went Wrong';
            }
            return back()->with('mgs', $msg);

        } else {
            return back()->with('mgsErr', 'You have not permission to access this page');
        }
    }

    public function prevcancelStudentsMarkReview($course_id)
    {
        $semester = $this->getPrevEnrollSemister();
        if ($semester) {
            $course = Course::find($course_id);
            if ($course->type == "Practical") {
                $security = PracticalTeacher::where('enroll_semister_id', $semester->id)
                    ->where('course_id', $course_id)
                    ->first();
            } else {
                $security = TheoriticalTeacher::where('enroll_semister_id', $semester->id)
                    ->where('course_id', $course_id)
                    ->first();
            }
            if ($security) {
                if ($security->status == 2) {
                    $security->status = 0;
                    $security->save();
                    $msg = 'Students Marks Cancelled';
                } else {
                    $msg = 'Something went Wrong';
                }
                return back()->with('mgs', $msg);

            } else {
                return back()->with('mgsErr', 'You have not permission to access this page');
            }
        } else {
            return back()->with('mgsErr', 'No Prev Semester Available');
        }

    }

//    Approve Students marks
    public function approveStudentsMarkReview($course_id)
    {

        $semester = $this->getLastEnrollSemister();
        $course = Course::find($course_id);
        if ($course->type == "Practical") {
            $security = PracticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course_id)
                ->first();
        } else {
            $security = TheoriticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course_id)
                ->first();
        }
        if ($security) {
            if ($security->status == 2) {
                $security->status = 3;
                $security->save();
                $msg = 'Students Marks Approved';
            } else {
                $msg = 'Something Went Wrong';
            }
            return back()->with('mgs', $msg);

        } else {
            return back()->with('mgsErr', 'You have not permission to access this page');
        }
    }

//    Approve Prev Students marks
    public function prevapproveStudentsMarkReview($course_id)
    {

        $semester = $this->getPrevEnrollSemister();
        if ($semester) {
            $course = Course::find($course_id);
            if ($course->type == "Practical") {
                $security = PracticalTeacher::where('enroll_semister_id', $semester->id)
                    ->where('course_id', $course_id)
                    ->first();
            } else {
                $security = TheoriticalTeacher::where('enroll_semister_id', $semester->id)
                    ->where('course_id', $course_id)
                    ->first();
            }
            if ($security) {
                if ($security->status == 2) {
                    $security->status = 3;
                    $security->save();
                    $msg = 'Students Marks Approved';
                } else {
                    $msg = 'Something Went Wrong';
                }
                return back()->with('mgs', $msg);

            } else {
                return back()->with('mgsErr', 'You have not permission to access this page');
            }
        } else {
            return back()->with('mgsErr', 'No Prev Semester Available');
        }

    }

    /*Start Pdf download*/
    public function downloadTheoreticalPdf($semesterId)
    {
        $semisterName = $this->semisterToName($semesterId);

        $semister = $this->getLastEnrollSemister();
        $teachers = TheoriticalTeacher::where('enroll_semister_id', $semister->id)->with(['course' => function ($query) use ($semesterId) {
            $query->where('semister', $semesterId);
        }])->get()->where('course', '!=', null);
        // return view('users.chairman.dashboard.theoreticalPDF', compact('semister', 'teachers'));
        // return view('pdf', compact('enroll','semisterName'));
        $view = view('users.deanOfficer.dashboard.courseTeachers.theoreticalPDF', compact('semister', 'teachers', 'semesterId', 'semisterName'))->render();
        $pdf = PDF::loadHTML($view);
        return $pdf->stream(time() . 'QuestionSetter.pdf');
    }

    /*Start Pdf download*/
    public function prevdownloadTheoreticalPdf($semesterId)
    {
        $semisterName = $this->semisterToName($semesterId);

        $semister = $this->getPrevEnrollSemister();
        if ($semister) {
            $teachers = TheoriticalTeacher::where('enroll_semister_id', $semister->id)->with(['course' => function ($query) use ($semesterId) {
                $query->where('semister', $semesterId);
            }])->get()->where('course', '!=', null);
            $view = view('users.deanOfficer.dashboard.courseTeachers.theoreticalPDF', compact('semister', 'teachers', 'semesterId', 'semisterName'))->render();
            $pdf = PDF::loadHTML($view);
            return $pdf->stream(time() . 'QuestionSetter.pdf');
        } else {
            return back()->with('mgsErr', 'No Prev Semester Available');
        }
    }

    public function downloadInternalPdf($semesterId)
    {
        $semisterName = $this->semisterToName($semesterId);
        $semister = $this->getLastEnrollSemister();
        $teachers = PracticalTeacher::where('enroll_semister_id', $semister->id)->with(['course' => function ($query) use ($semesterId) {
            $query->where('semister', $semesterId);
        }])->get()->where('course', '!=', null);
        $view = view('users.deanOfficer.dashboard.courseTeachers.internalPDF', compact('semister', 'semisterName', 'teachers', 'semesterId'))->render();
        $pdf = PDF::loadHTML($view);
        return $pdf->stream(time() . 'internalTeacher.pdf');
    }

    public function prevdownloadInternalPdf($semesterId)
    {
        $semisterName = $this->semisterToName($semesterId);
        $semister = $this->getPrevEnrollSemister();
        if ($semister) {
            $teachers = PracticalTeacher::where('enroll_semister_id', $semister->id)->with(['course' => function ($query) use ($semesterId) {
                $query->where('semister', $semesterId);
            }])->get()->where('course', '!=', null);
            $view = view('users.deanOfficer.dashboard.courseTeachers.internalPDF', compact('semister', 'semisterName', 'teachers', 'semesterId'))->render();
            $pdf = PDF::loadHTML($view);
            return $pdf->stream(time() . 'internalTeacher.pdf');
        } else {
            return back()->with('mgsErr', 'No Prev Semester Available');
        }
    }

    public function downloadExternalPdf($semesterId)
    {
        $semisterName = $this->semisterToName($semesterId);
        $semister = $this->getLastEnrollSemister();
        $teachers = PracticalTeacher::where('enroll_semister_id', $semister->id)->with(['course' => function ($query) use ($semesterId) {
            $query->where('semister', $semesterId);
        }])->get()->where('course', '!=', null);
        $view = view('users.deanOfficer.dashboard.courseTeachers.externalPDF', compact('semister', 'semisterName', 'teachers', 'semesterId'))->render();
        $pdf = PDF::loadHTML($view);
        return $pdf->stream(time() . 'externalTeacher.pdf');
    }

    public function prevdownloadExternalPdf($semesterId)
    {
        $semisterName = $this->semisterToName($semesterId);
        $semister = $this->getPrevEnrollSemister();
        if ($semister) {
            $teachers = PracticalTeacher::where('enroll_semister_id', $semister->id)->with(['course' => function ($query) use ($semesterId) {
                $query->where('semister', $semesterId);
            }])->get()->where('course', '!=', null);
            $view = view('users.deanOfficer.dashboard.courseTeachers.externalPDF', compact('semister', 'semisterName', 'teachers', 'semesterId'))->render();
            $pdf = PDF::loadHTML($view);
            return $pdf->stream(time() . 'externalTeacher.pdf');
        } else {
            return back()->with('mgsErr', 'No Prev Semester Available');
        }

    }
    /*End Pdf download*/

    /*test Fail*/
    public function current_courses()
    {
        if (date('n') < 7) {
            $courses = Course::whereIn('semister', [1, 3, 5, 7])->get();
        } else {
            $courses = Course::whereIn('semister', [2, 4, 6, 8])->get();
        }
        return view('users.deanOfficer.dashboard.currentCourses', compact('courses'));
    }

    /*Course Fi List*/
    public function courseFiList(Course $course)
    {
        $semister = $this->getLastEnrollSemister();
        if ($semister) {
            $courseEnrolls = CourseEnroll::where('course_id', $course->id)->with(['enroll' => function ($q) use ($semister) {
                $q->where('enroll_semister_id', $semister->id);
                $q->with('student');
            }])->get();
            $courseEnrolls = $courseEnrolls->filter(function ($item, $key) use ($course) {
                return $course->semister < $item->enroll->semister;
            });
            return view('users.deanOfficer.dashboard.courseFiList', compact('course', 'courseEnrolls', 'semister'));

        } else {
            return back()->with('mgsErr', 'Please Start Semister First');
        }
    }

//    Start Teacher Section
    public function teacherCreateForm()
    {
        $data = array();
        $data['departments'] = Department::all();
        return view('users.deanOfficer.dashboard.addTeacher')->with($data);
    }

    public function saveTeacher(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'designation' => 'required',
            'phone_number' => 'required',
            'department_id' => 'required',
            'email' => 'required|unique:teachers',
            'password' => 'required',
        ]);

        $request['password'] = bcrypt($request->password);
        Teacher::create($request->all());
        return redirect(route('dean.teacher.list'))->with('mgs', 'Teacher Created Successfully');
    }

    public function teacherList()
    {
        $data = array();
        $data['teachers'] = Teacher::all();
        return view('users.deanOfficer.dashboard.teacher_list')->with($data);
    }
//    End Teacher Section

//  Start certificate section
    public function manageCertificate()
    {
        $data = array();
        $data['certificates'] = Certificate::all();
        return view('users.deanOfficer.dashboard.certificates.certificate_list')->with($data);
    }

    public function createCertificate()
    {
        return view('users.deanOfficer.dashboard.certificates.addCertificate');

    }

    public function saveCertificate(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'money' => 'required',
            'description' => 'required',
        ]);
        Certificate::create($request->all());
        return back()->with('mgs', 'Certificate Successfully Added!');
    }

    public function showCertificate($id)
    {
        $certificate = Certificate::find($id);
        return view('users.deanOfficer.dashboard.certificates.viewCertificate', compact('certificate'))->render();
    }

    public function editCertificate(Request $request)
    {
        $id = $request->id;
        $certificate = Certificate::find($id);
        return view('users.deanOfficer.dashboard.certificates.editCertificate', compact('certificate'))->render();
    }

    public function updateCertificate(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'money' => 'required',
            'description' => 'required',
        ]);
        if ($validator->fails()) {
            return back()->with('mgsErr', $validator->errors()->first());
        }
        $certificate = Certificate::find($id);
        $certificate->name = $request->name;
        $certificate->money = $request->money;
        $certificate->description = $request->description;
        if ($certificate->save()) {
            return back()->with('mgs', 'Certificate Successfully Updated');
        } else {
            return back()->with('mgsErr', 'Something Went Wrong');
        }
    }

    public function deleteCertificate($id)
    {
        $certificate = Certificate::find($id);
        if ($certificate->delete()) {
            return back()->with('mgs', 'Certificate Successfully Deleted');
        } else {
            return back()->with('mgsErr', 'Something Went Wrong');
        }
    }

//  End certificate section
// certificate requests
    public function certificateRequests()
    {
        $data = array();
        $data['request_list'] = CertificateRequest::all();
        return view('users.deanOfficer.dashboard.requests.request_list')->with($data);
    }

    public function showCertificateRequests($id)
    {
        $certificateRequest = CertificateRequest::find($id);
        return view('users.deanOfficer.dashboard.requests.viewCertificateRequest', compact('certificateRequest'))->render();

    }

    public function approveCertificateRequest($id)
    {
        $requestCertificate = CertificateRequest::find($id);
        if ($requestCertificate->status != 0) {
            if ($requestCertificate->status == 1) {
                $requestCertificate->status = 2;
                $msg = 'Certificate Approved';
            } else {
                $requestCertificate->status = 1;
                $msg = 'Certificate Request Cancel';
            }
            $requestCertificate->save();
            return back()->with('mgs', $msg);
        }
    }

    public function approveCertificateRequestInput(Request $request, $id)
    {
        $request->validate([
            'start_date' => 'required',
            'end_date' => 'required'
        ]);
        $duration = 'from ' . Carbon::parse($request->start_date)->format('d M. y') . ' to ' . Carbon::parse($request->end_date)->format('d M. y');
        $requestCertificate = CertificateRequest::find($id);
        if ($requestCertificate->status != 0) {
            if ($requestCertificate->status == 1) {
                $requestCertificate->status = 2;
                $requestCertificate->duration = $duration;
                $msg = 'Certificate Approved';
            } else {
                $requestCertificate->status = 1;
                $msg = 'Certificate Request Cancel';
            }
            $requestCertificate->save();
            return back()->with('mgs', $msg);
        }
    }

    public function deleteCertificateRequest($id)
    {
        $certificateRequest = CertificateRequest::find($id);
        if ($certificateRequest->delete()) {
            return back()->with('mgs', 'Certificate Request Successfully Deleted');
        } else {
            return back()->with('mgsErr', 'Something Went Wrong');
        }
    }

//    end certificate requests

    public function deanInfo()
    {
        $dean = (new Setting())->get('dean');
        return view('users.deanOfficer.dashboard.settings.dean_info',compact('dean'));
    }

    public function updateDeanInfo(Request $request)
    {
        $request->validate([
            "name" => "required",
            "designation" => "required",
            "phone_number" => "required",
            "email" => "required",
        ]);
        $input = $request->except('_token');

        $setting = new Setting();
        if ($request->has('signature')) {
            $request->validate([
                'signature' => 'required|image'
            ]);
            $path = $this->sigUpload($request);
            $input['signature'] = $path;
        }else{
            $input['signature'] = @$setting->get('dean')['signature'];
        }
        $setting->set('dean', $input);
        return redirect(route('admin.home'))->with('mgs', 'Dean Information Successfully Updated');
    }

    public function examControllerInfo()
    {
        $exam_controller = (new Setting())->get('exam_controller');
        return view('users.deanOfficer.dashboard.settings.exam_controller_info',compact('exam_controller'));
    }

    public function updateExamControllerInfo(Request $request)
    {
        $request->validate([
            "name" => "required",
            "phone_number" => "required",
            "email" => "required",
        ]);
        $input = $request->except('_token');
        $setting = new Setting();
        if ($request->has('signature')) {
            $request->validate([
                'signature' => 'required|image'
            ]);
            $path = $this->sigUpload($request);
            $input['signature'] = $path;
        }else{
            $input['signature'] = @$setting->get('exam_controller')['signature'];
        }
        $setting->set('exam_controller', $input);
        return redirect(route('admin.home'))->with('mgs', 'Exam Controller Information Successfully Updated');

    }

    private function sigUpload($request)
    {
        $image = $request->file('signature');
        $path = "public/users/images/signature/" . time() . "." . $image->getClientOriginalExtension();
        $img = Image::make($image->getRealPath())->resize(100, 60);
        $img->save($path);
        return $path;
    }

}