<?php

namespace App\Http\Controllers;


use App\CourseEnroll;
use App\PracticalAssignedTeacher;
use App\StudentCourseMark;
use App\Teacher;
use App\TheoryAssignedTeacher;
use App\traits\signature;
use Illuminate\Http\Request;
use App\Course;
use App\TheoriticalTeacher;
use App\PracticalTeacher;
use App\traits\getCurrentEnrollSemister;
use Illuminate\Support\Facades\Auth;
use PDF;

class DepartmentChairmanController extends Controller
{
    use getCurrentEnrollSemister, signature;

    public function __construct()
    {
        $this->middleware('departmentChairmanLogin');
        if (!$this->getLastEnrollSemister()) {
            echo 'Dean Officer does not add Current Semister. Please Contact with Dean Officer';
            exit();
        }
    }

    public function index()
    {
        $currentSemister = $this->getLastEnrollSemister();
        $assignedCourses = TheoriticalTeacher::where('enroll_semister_id', $currentSemister->id)->get()->pluck('course_id')->toArray();
        $teachers = $this->departmentTeachers();
        $courses = Course::where('type', 'Theoretical')
            ->where('department_id', $this->department_id())
            ->whereIn('semister', $this->semisterArray())
            ->whereNotIn('id', $assignedCourses)
            ->get();
        return view('users.chairman.dashboard.home', compact('courses', 'currentSemister', 'teachers'));

    }

    //Update Login info form
    public function profileForm()
    {
        return view('users.chairman.dashboard.editProfile');
    }

    //Update  login info
    public function updateProfile(Request $request)
    {
        $this->validate($request, array(
            'email' => 'required|unique:department_chairmen,email,' . auth('dhead')->user()->id,
            'password' => 'required|min:6|confirmed',
        ));
        $chairman = auth('dhead')->user();
        $chairman->email = $request->email;
        $chairman->password = bcrypt($request->password);
        if ($chairman->save()) {
            return redirect(route('chairman.home'))->with('mgs', 'Successfully Updates!');
        }

    }

    //add signature form
    public function showAddSignatureForm()
    {
        return view('users.chairman.dashboard.signature');
    }

    //add signature
    public function addSignature(Request $request)
    {
        $chairman = auth('dhead')->user();
        $this->signature($request, $chairman);
        return redirect(route('chairman.home'))->with('mgs', 'Signature Uploaded!');
    }

    //save a theoritical teacher for course
    public function addTheoriticalTeacher(Request $request)
    {
        $this->validate($request, array(
            'course_id' => 'required',
            'teacher_id' => 'required',
        ));
        $currentSemister = $this->getLastEnrollSemister();
        $teacher = new TheoriticalTeacher();
        $teacher->course_id = $request->course_id;
        $teacher->enroll_semister_id = $currentSemister->id;
        $teacher->department_chairmen_id = Auth::guard('dhead')->user()->id;
        $teacher->save();
        foreach ($request->teacher_id as $eachTeacher) {
            $assignedTeacher = new TheoryAssignedTeacher();
            $assignedTeacher->theoritical_teacher_id = $teacher->id;
            $assignedTeacher->teacher_id = $eachTeacher;
            $assignedTeacher->save();
        }
        return redirect()->back()->with('mgs', 'Successfully Added');
    }

    public function addPracticalTeacherForm()
    {
        $currentSemister = $this->getLastEnrollSemister();
        $assignedCourses = PracticalTeacher::where('enroll_semister_id', $currentSemister->id)->get()->pluck('course_id')->toArray();
        $teachers = $this->departmentTeachers();
        $unsubmittedCourses = Course::where('type', 'Practical')
            ->where('department_id', $this->department_id())
            ->whereIn('semister', $this->semisterArray())
            ->whereNotIn('id', $assignedCourses)
            ->get();

        return view('users.chairman.dashboard.practicalTeacher', compact('unsubmittedCourses', 'currentSemister', 'teachers'));

    }

    public function addPracticalTeacher(Request $request)
    {
        $this->validate($request, array(
            'course_id' => 'required',
            'teacher_id' => 'required',
            'ext_teacher_one' => 'required',
            'ext_teacher_two' => 'required',
            'ext_teacher_three' => 'required',
            'ext_deg_one' => 'required',
            'ext_deg_two' => 'required',
            'ext_deg_three' => 'required',
        ));
        $ext_teachers = array(
            array('name' => $request->ext_teacher_one, 'designation' => $request->ext_deg_one,),
            array('name' => $request->ext_teacher_two, 'designation' => $request->ext_deg_two,),
            array('name' => $request->ext_teacher_three, 'designation' => $request->ext_deg_three)
        );
        $currentSemister = $this->getLastEnrollSemister();
        $teacher = new PracticalTeacher();
        $teacher->course_id = $request->course_id;
        $teacher->enroll_semister_id = $currentSemister->id;
        $teacher->ext_teachers = serialize($ext_teachers);
        $teacher->department_chairmen_id = Auth::guard('dhead')->user()->id;
        $teacher->save();
        foreach ($request->teacher_id as $eachTeacher) {
            $assignedTeacher = new PracticalAssignedTeacher();
            $assignedTeacher->practical_teacher_id = $teacher->id;
            $assignedTeacher->teacher_id = $eachTeacher;
            $assignedTeacher->save();
        }
        return redirect()->back()->with('mgs', 'Successfully Added');
    }

    public function viewIntPracticalTeacher()
    {
        $semister = $this->getLastEnrollSemister();
        $teachers = PracticalTeacher::where('enroll_semister_id', $semister->id)
            ->where('department_chairmen_id', Auth::guard('dhead')->user()->id)
            ->get();
        return view('users.chairman.dashboard.viewIntPracticalTeacher', compact('semister', 'teachers'));
    }

    public function viewExtPracticalTeacher()
    {
        $semister = $this->getLastEnrollSemister();
        $teachers = PracticalTeacher::where('enroll_semister_id', $semister->id)
            ->where('department_chairmen_id', Auth::guard('dhead')->user()->id)
            ->get();
        return view('users.chairman.dashboard.viewExtPracticalTeacher', compact('semister', 'teachers'));
    }

    public function viewTheoreticalTeacher()
    {
        $semister = $this->getLastEnrollSemister();
        $teachers = TheoriticalTeacher::where('enroll_semister_id', $semister->id)
            ->where('department_chairmen_id', Auth::guard('dhead')->user()->id)
            ->get();
        return view('users.chairman.dashboard.viewTheoreticalTeacher', compact('semister', 'teachers'));
    }

    public function downloadTheoreticalPdf()
    {
        $semister = $this->getLastEnrollSemister();
        $teachers = TheoriticalTeacher::where('enroll_semister_id', $semister->id)
            ->where('department_chairmen_id', Auth::guard('dhead')->user()->id)
            ->get();
        // return view('users.chairman.dashboard.theoreticalPDF', compact('semister', 'teachers'));  
        // return view('pdf', compact('enroll','semisterName'));
        $view = view('users.chairman.dashboard.theoreticalPDF', compact('semister', 'teachers'))->render();
        $pdf = PDF::loadHTML($view);
        return $pdf->stream();
    }

    public function downloadInternalPdf()
    {
        $semister = $this->getLastEnrollSemister();
        $teachers = PracticalTeacher::where('enroll_semister_id', $semister->id)
            ->where('department_chairmen_id', Auth::guard('dhead')->user()->id)
            ->get();
        $view = view('users.chairman.dashboard.internalPDF', compact('semister', 'teachers'))->render();
        $pdf = PDF::loadHTML($view);
        return $pdf->stream();
    }

    public function downloadExternalPdf()
    {
        $semister = $this->getLastEnrollSemister();
        $teachers = PracticalTeacher::where('enroll_semister_id', $semister->id)
            ->where('department_chairmen_id', Auth::guard('dhead')->user()->id)
            ->get();
        $view = view('users.chairman.dashboard.externalPDF', compact('semister', 'teachers'))->render();
        $pdf = PDF::loadHTML($view);
        return $pdf->stream();
    }

    //teacher semester wise theory courses
    public function chairmanAssignedTheoryCourses($semester_id)
    {
        $semester = $this->getLastEnrollSemister();
//        dd(auth('dhead')->id());
        $courses=TheoriticalTeacher::where('enroll_semister_id',$semester->id)
//            ->where('approve_status',0)
//            ->where('submit_status',1)
            ->where('department_chairmen_id',auth('dhead')->id())
            ->whereHas('course',function ($query) use($semester_id){
                $query->where('semister', $semester_id);
            })
            ->get();
        return view('users.chairman.dashboard.semesterCoursesTheory', compact('courses', 'semester_id'));
    }
    //teacher prev semester wise theory courses
    public function prevchairmanAssignedTheoryCourses($semester_id)
    {
        $semester = $this->getPrevEnrollSemister();
        $courses=TheoriticalTeacher::where('enroll_semister_id',$semester->id)
            ->where('department_chairmen_id',auth('dhead')->id())
            ->whereHas('course',function ($query) use($semester_id){
                $query->where('semister', $semester_id);
            })
            ->get();
        return view('users.chairman.dashboard.prev.semesterCoursesTheory', compact('courses', 'semester_id'));
    }

    //teacher semester wise practical courses
    public function chairmanAssignedPracticalCourses($semester_id)
    {
        $semester = $this->getLastEnrollSemister();
        $courses = PracticalTeacher::where('enroll_semister_id', $semester->id)
            ->where('department_chairmen_id',auth('dhead')->id())
            ->whereHas('course', function ($query) use ($semester_id) {
                $query->where('semister', $semester_id);
            })->get();
        return view('users.chairman.dashboard.semesterCoursesPractical', compact('courses', 'semester_id'));
    }
    //teacher prev semester wise practical courses
    public function prevchairmanAssignedPracticalCourses($semester_id)
    {
        $semester = $this->getPrevEnrollSemister();
        $courses = PracticalTeacher::where('enroll_semister_id', $semester->id)
            ->where('department_chairmen_id',auth('dhead')->id())
            ->whereHas('course', function ($query) use ($semester_id) {
                $query->where('semister', $semester_id);
            })->get();
        return view('users.chairman.dashboard.prev.semesterCoursesPractical', compact('courses', 'semester_id'));
    }

//    view student marks
    public function viewStudentsMark($course_id)
    {
        $semester = $this->getLastEnrollSemister();
        $course=Course::where('id',$course_id)->where('department_id',auth('dhead')->user()->department->id)->first();

        if ($course) {
            $courseEnrolls = $course->enrolls->where('enroll_semister_id', $semester->id);
            $courseEnrolls->each(function ($item, $key) use ($course, $semester) {
                $marks = StudentCourseMark::where('student_id', $item->student_id)->where('course_id', $course->id)->where('enroll_semister_id', $semester->id)->first();
                $item->marks = $marks;
            });
            $data['courseEnrolls'] = $courseEnrolls;
            $data['course'] = $course;
            return view('users.chairman.dashboard.studentmarksview')->with($data);
        } else {
            return back()->with('mgsErr', 'You have not permission to access this page');
        }
    }
//    view prev student marks
    public function prevviewStudentsMark($course_id)
    {
        $semester = $this->getPrevEnrollSemister();
        $course=Course::where('id',$course_id)->where('department_id',auth('dhead')->user()->department->id)->first();

        if ($course) {
            $courseEnrolls = $course->enrolls->where('enroll_semister_id', $semester->id);
            $courseEnrolls->each(function ($item, $key) use ($course, $semester) {
                $marks = StudentCourseMark::where('student_id', $item->student_id)->where('course_id', $course->id)->where('enroll_semister_id', $semester->id)->first();
                $item->marks = $marks;
            });
            $data['courseEnrolls'] = $courseEnrolls;
            $data['course'] = $course;
            return view('users.chairman.dashboard.studentmarksview')->with($data);
        } else {
            return back()->with('mgsErr', 'You have not permission to access this page');
        }
    }

    public function cancelStudentsMarkReview($course_id){
        $semester = $this->getLastEnrollSemister();
        $course=Course::find($course_id);
        if ($course->type == "Practical") {
            $security = PracticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course_id)
                ->where('department_chairmen_id', auth('dhead')->id())
                ->first();
        } else {
            $security = TheoriticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course_id)
                ->where('department_chairmen_id', auth('dhead')->id())
                ->first();
        }
        if ($security) {
            if ($security->status==1){
                $security->status=0;
                $security->save();
                $msg='Marks Review Cancelled';
            }else{
                $msg='Marks Review Already Cancelled';
            }
            return back()->with('mgs', $msg);

        } else {
            return back()->with('mgsErr', 'You have not permission to access this page');
        }
    }

    public function prevcancelStudentsMarkReview($course_id){
        $semester = $this->getPrevEnrollSemister();
        $course=Course::find($course_id);
        if ($course->type == "Practical") {
            $security = PracticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course_id)
                ->where('department_chairmen_id', auth('dhead')->id())
                ->first();
        } else {
            $security = TheoriticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course_id)
                ->where('department_chairmen_id', auth('dhead')->id())
                ->first();
        }
        if ($security) {
            if ($security->status==1){
                $security->status=0;
                $security->save();
                $msg='Marks Review Cancelled';
            }else{
                $msg='Marks Review Already Cancelled';
            }
            return back()->with('mgs', $msg);

        } else {
            return back()->with('mgsErr', 'You have not permission to access this page');
        }
    }

    public function approveStudentsMarkReview($course_id){
        $semester = $this->getLastEnrollSemister();
        $course=Course::find($course_id);
        if ($course->type == "Practical") {
            $security = PracticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course_id)
                ->where('department_chairmen_id', auth('dhead')->id())
                ->first();
        } else {
            $security = TheoriticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course_id)
                ->where('department_chairmen_id', auth('dhead')->id())
                ->first();
        }
        if ($security) {
            if ($security->status==1){
                $security->status=2;
                $security->save();
                $msg='Marks Review Approved';
            }else{
                $msg='Something Went Wrong';
            }
            return back()->with('mgs', $msg);

        } else {
            return back()->with('mgsErr', 'You have not permission to access this page');
        }
    }
    public function prevapproveStudentsMarkReview($course_id){
        $semester = $this->getPrevEnrollSemister();
        $course=Course::find($course_id);
        if ($course->type == "Practical") {
            $security = PracticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course_id)
                ->where('department_chairmen_id', auth('dhead')->id())
                ->first();
        } else {
            $security = TheoriticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course_id)
                ->where('department_chairmen_id', auth('dhead')->id())
                ->first();
        }
        if ($security) {
            if ($security->status==1){
                $security->status=2;
                $security->save();
                $msg='Marks Review Approved';
            }else{
                $msg='Something Went Wrong';
            }
            return back()->with('mgs', $msg);

        } else {
            return back()->with('mgsErr', 'You have not permission to access this page');
        }
    }
    public function department_id()
    {
        return Auth::guard('dhead')->user()->department_id;
    }

    public function semisterArray()
    {
        $semisterArray = Array();
        if (date('n') < 7) {
            $semisterArray = [1, 3, 5, 7];
        } else {
            $semisterArray = [2, 4, 6, 8];
        }
        return $semisterArray;
    }

    public function departmentTeachers()
    {
        return Teacher::where('department_id', $this->department_id())->get();
    }


}
