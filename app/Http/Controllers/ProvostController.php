<?php

namespace App\Http\Controllers;
use App\Enroll;
use App\Course;
use App\HallOfficer;
use App\Student;
use App\Hall;
use App\AccountOfficer;
use App\EnrollSemister;
use App\HallMoney ;
use App\traits\getCurrentEnrollSemister;
use App\traits\signature;


use Illuminate\Http\Request;

class ProvostController extends Controller
{
    use getCurrentEnrollSemister, signature;
    public function __construct()
    {
        $this->middleware('provostLogin');
        if (!$this->getCurrentEnrollSemister()) {
            echo 'Dean Officer does not add Current Semister. Please Contact with Dean Officer';
            exit();
        }
    }

    //provost homepage
    public function index(){
        // return Hall::where('hall_officer_id',auth('provost')->user()->id)->select('id')->first()->id;
        $semister = $this->getLastEnrollSemister();
        if($semister && auth('provost')->user()->hall){
            // return $students = Student::where('hall_id',Hall::where('hall_officer_id',auth('provost')->user()->id)->first()->id)->has('enrolls')->with(['enrolls' => function($q){
            //     $q->where('enroll_semister_id',$this->getLastEnrollSemister()->id)->first();
            // }])->get();
            $students = auth('provost')->user()->hall->students->pluck('id');
            $enrolls = Enroll::where('enroll_semister_id', $semister->id)->whereIn('student_id', $students)->get();
        }else{
            $enrolls = false;
        }

        return view('users.provost.dashboard.home', compact('enrolls','semister'));
    }
    
    //provost homepage
    public function manageEnroll($semesterId){
        // return Hall::where('hall_officer_id',auth('provost')->user()->id)->select('id')->first()->id;
        $semister = $this->getLastEnrollSemister();
        if($semister && auth('provost')->user()->hall){
            // return $students = Student::where('hall_id',Hall::where('hall_officer_id',auth('provost')->user()->id)->first()->id)->has('enrolls')->with(['enrolls' => function($q){
            //     $q->where('enroll_semister_id',$this->getLastEnrollSemister()->id)->first();
            // }])->get();
            $students = auth('provost')->user()->hall->students->pluck('id');
            $enrolls = Enroll::where('enroll_semister_id', $semister->id)->where('semister',$semesterId)->whereIn('student_id', $students)->get();
        }else{
            $enrolls = false;
        }

        return view('users.provost.dashboard.home', compact('enrolls','semister','semesterId'));
    }


    //view Edit Enroll Form
    public function viewEditEnrollForm(Enroll $enroll){
        $receipt = HallMoney::where('enroll_id', $enroll->id)->first();

        return view('users.provost.dashboard.hallStatus', compact('enroll', 'receipt'));
    }
    //change Enroll Status
    public function changeEnrollStatus(Enroll $enroll){
        if($enroll->hall_status== 1){
            $enroll->hall_status = 0;
            $mgs = "Enroll Disapproved";
        }else{
            $enroll->hall_status = 1;
            $mgs = "Enroll Approved";
        }
        $enroll->save();
        return redirect()->back()->with('mgs', $mgs) ;
    }

    //list of all students
    public function allStudents(){
        $students= null;
        if(auth('provost')->user()->hall){
            
        $students = auth('provost')->user()->hall->students;
        }
        return view('users.provost.dashboard.allStudents', compact('students'));
    }

    //add signature form
    public  function showAddSignatureForm(){
        return view('users.provost.dashboard.signature');
    }

    //add signature
    public function addSignature(Request $request){
        $provost = auth('provost')->user();
        $this->signature($request, $provost);
        return redirect(route('provost.home'))->with('mgs','Signature Uploaded!');
    }

    //Update Login info form
    public function profileForm(){
        return view('users.provost.dashboard.editProfile');
    }

    //Update  login info
    public function updateProfile(Request $request){
        $this->validate($request, array(
            'email' => 'required',
            'password' => 'required|min:6|confirmed',
        ));
        $student = auth('provost')->user();

        $emails = HallOfficer::select('email')->get();
        foreach($emails as $email){
            if($request->email == $email->email and $student->email != $email->email){
                return redirect()->back()->with('mgsErr','Email not Available') ;

            }
        }
        $student->email = $request->email;
        $student->password = bcrypt($request->password);
        if($student->save()){
            return redirect(route('provost.home'))->with('mgs','Successfully Updates!') ;
        }

    }


}
