<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ProvostForgotController extends Controller
{
    //Sends Password Reset emails
    use SendsPasswordResetEmails;


    public function showResetForm()
    {
        return view('users.provost.auth.sendReset');
    }


    public function __construct()
    {
        $this->middleware('guestProvost');
    }
    public function broker()
    {
        return Password::broker('provost');
    }
    protected function sendResetLinkResponse($response)
    {
        return redirect()->back()->with('status', trans($response))->with('mgs','Reset Code Sent!');
    }
}