<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Password;

class ProvostResetController extends Controller
{
    //trait for handling reset Password
    use ResetsPasswords;
    protected $redirectTo = '/provost';

    public function showResetForm(Request $request, $token = null)
    {

        return view('users.provost.auth.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    public function broker()
    {
        return Password::broker('provost');
    }

    protected function guard()
    {
        return Auth::guard('provost');
    }
}
