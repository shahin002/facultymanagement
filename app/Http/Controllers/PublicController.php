<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\traits\getCurrentEnrollSemister;
use App\Course;

class PublicController extends Controller
{
    use getCurrentEnrollSemister;
    //get all course
    public function getCourses(){
        $enrollSemister = $this->getLastEnrollSemister();
        if($enrollSemister->month == 0){
            $courses = Course::whereNotIN('semister',[2,4,6,8])->orderBy('semister','desc')->get();
        }else{
            $courses = Course::whereNotIN('semister',[1,3,5,7])->orderBy('semister','desc')->get();
        }
        return $courses;
    }
}
