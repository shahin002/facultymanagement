<?php

namespace App\Http\Controllers;

use App\Course;
use App\PracticalTeacher;
use App\StudentCourseMark;
use App\TheoriticalTeacher;
use App\traits\getCurrentEnrollSemister;
use App\traits\signature;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeacherController extends Controller
{
    use getCurrentEnrollSemister, signature;

    public function __construct()
    {
        $this->middleware('teacherLogin');
        if (!$this->getLastEnrollSemister()) {
            echo 'Dean Officer does not add Current Semister. Please Contact with Dean Officer';
            exit();
        }
    }

    //show Teacher homepage
    public function index()
    {
        return view('users.teacher.dashboard.home');
    }

    //add signature form
    public function showAddSignatureForm()
    {
        return view('users.teacher.dashboard.signature');
    }

    //save signature
    public function addSignature(Request $request)
    {
        $teacher = auth('teacher')->user();
        $this->signature($request, $teacher);
        return redirect(route('teacher.home'))->with('mgs', 'Signature Uploaded!');
    }

    //Update Login info form
    public function profileForm()
    {
        return view('users.teacher.dashboard.editProfile');
    }

    //Update  login info
    public function updateProfile(Request $request)
    {
        $this->validate($request, array(
            'email' => 'required|unique:teachers,email,' . auth('teacher')->user()->id,
            'password' => 'required|min:6|confirmed',
        ));
        $teacher = auth('teacher')->user();
        $teacher->email = $request->email;
        $teacher->password = bcrypt($request->password);
        if ($teacher->save()) {
            return redirect(route('teacher.home'))->with('mgs', 'Successfully Updates!');
        }

    }

    //teacher semester wise theory courses
    public function teacherAssignedTheoryCourses($semester_id)
    {
        $semester = $this->getLastEnrollSemister();
        $courses = TheoriticalTeacher::where('enroll_semister_id', $semester->id)
            ->with(['course.courseEnrolls.enroll' => function ($query) use ($semester) {
                $query->where('enroll_semister_id', $semester->id);
            }])
            ->whereHas('course', function ($query) use ($semester_id) {
                $query->where('semister', $semester_id);
            })->whereHas('theoryAssignedTeachers', function ($query) {
                $query->where('teacher_id', auth('teacher')->id());
            })->get();
        return view('users.teacher.dashboard.theoriticalCourses', compact('courses', 'semester_id'));
    }

    //teacher prev semester wise theory courses
    public function prevTeacherAssignedTheoryCourses($semester_id)
    {
        $semester = $this->getPrevEnrollSemister();
        if($semester){
            $courses = TheoriticalTeacher::where('enroll_semister_id', $semester->id)
                ->with(['course.courseEnrolls.enroll' => function ($query) use ($semester) {
                    $query->where('enroll_semister_id', $semester->id);
                }])
                ->whereHas('course', function ($query) use ($semester_id) {
                    $query->where('semister', $semester_id);
                })->whereHas('theoryAssignedTeachers', function ($query) {
                    $query->where('teacher_id', auth('teacher')->id());
                })->get();
            return view('users.teacher.dashboard.prev.theoriticalCourses', compact('courses', 'semester_id'));
        }else{
            return 'There is no Previous Semester';
        }

    }

    //teacher semester wise practical courses
    public function teacherAssignedPracticalCourses($semester_id)
    {
        $semester = $this->getLastEnrollSemister();
        $courses = PracticalTeacher::where('enroll_semister_id', $semester->id)
            ->with(['course.courseEnrolls.enroll' => function ($query) use ($semester) {
                $query->where('enroll_semister_id', $semester->id);
            }])
            ->whereHas('course', function ($query) use ($semester_id) {
                $query->where('semister', $semester_id);
            })->whereHas('practicalAssignedTeacher', function ($query) {
                $query->where('teacher_id', auth('teacher')->id());
            })->get();
        return view('users.teacher.dashboard.practicalCourses', compact('courses', 'semester_id'));
    }

    //teacher prev semester wise practical courses
    public function prevTeacherAssignedPracticalCourses ($semester_id)
    {
        $semester = $this->getPrevEnrollSemister();
        if($semester){
            $courses = PracticalTeacher::where('enroll_semister_id', $semester->id)
                ->with(['course.courseEnrolls.enroll' => function ($query) use ($semester) {
                    $query->where('enroll_semister_id', $semester->id);
                }])
                ->whereHas('course', function ($query) use ($semester_id) {
                    $query->where('semister', $semester_id);
                })->whereHas('practicalAssignedTeacher', function ($query) {
                    $query->where('teacher_id', auth('teacher')->id());
                })->get();
            return view('users.teacher.dashboard.prev.practicalCourses', compact('courses', 'semester_id'));
        }else{
            return 'There is no Previous Semester';
        }

    }

//    mark students
    public function teacherMarkStudent($id)
    {

        $course = Course::where('id', $id)->with('enrolls.student')->first();

        $semester = $this->getLastEnrollSemister();
        if ($course->type == "Practical") {
            $security = PracticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course->id)
                ->where('status', '<', 2)
                ->whereHas('practicalAssignedTeacher', function ($query) {
                    $query->where('teacher_id', auth('teacher')->id());
                })->first();
        } else {
            $security = TheoriticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course->id)
                ->where('status', '<', 2)
                ->whereHas('theoryAssignedTeachers', function ($query) {
                    $query->where('teacher_id', auth('teacher')->id());
                })->first();
        }
        if ($security) {
            $courseEnrolls = $course->enrolls->where('enroll_semister_id', $semester->id);
            $courseEnrolls->each(function ($item, $key) use ($course, $semester) {
                $marks = StudentCourseMark::where('student_id', $item->student_id)->where('course_id', $course->id)->where('enroll_semister_id', $semester->id)->first();
                $item->marks = $marks;
            });

            $data['courseEnrolls'] = $courseEnrolls->sortBy('student.roll_no');
            $data['course'] = $course;
            return view('users.teacher.dashboard.studentmarks')->with($data);
        } else {
            return back()->with('mgsErr', 'You have not permission to access this page');
        }
    }
//    Prev mark students
    public function prevTeacherMarkStudent($id)
    {

        $course = Course::where('id', $id)->with('enrolls.student')->first();

        $semester = $this->getPrevEnrollSemister();

        if ($semester){
            if ($course->type == "Practical") {
                $security = PracticalTeacher::where('enroll_semister_id', $semester->id)
                    ->where('course_id', $course->id)
                    ->where('status', '<', 2)
                    ->whereHas('practicalAssignedTeacher', function ($query) {
                        $query->where('teacher_id', auth('teacher')->id());
                    })->first();
            } else {
                $security = TheoriticalTeacher::where('enroll_semister_id', $semester->id)
                    ->where('course_id', $course->id)
                    ->where('status', '<', 2)
                    ->whereHas('theoryAssignedTeachers', function ($query) {
                        $query->where('teacher_id', auth('teacher')->id());
                    })->first();
            }
            if ($security) {
                $courseEnrolls = $course->enrolls->where('enroll_semister_id', $semester->id);
                $courseEnrolls->each(function ($item, $key) use ($course, $semester) {
                    $marks = StudentCourseMark::where('student_id', $item->student_id)->where('course_id', $course->id)->where('enroll_semister_id', $semester->id)->first();
                    $item->marks = $marks;
                });

                $data['courseEnrolls'] = $courseEnrolls->sortBy('student.roll_no');
                $data['course'] = $course;
                return view('users.teacher.dashboard.prev.studentmarks')->with($data);
            } else {
                return back()->with('mgsErr', 'You have not permission to access this page');
            }
        }else{
            return 'There is no Previous Semester';
        }

    }

    public function teacherMarkStudentView($id)
    {

        $course = Course::where('id', $id)->with('enrolls.student')->first();

        $semester = $this->getLastEnrollSemister();
        if ($course->type == "Practical") {
            $security = PracticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course->id)
                ->whereHas('practicalAssignedTeacher', function ($query) {
                    $query->where('teacher_id', auth('teacher')->id());
                })->first();
        } else {
            $security = TheoriticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course->id)
                ->whereHas('theoryAssignedTeachers', function ($query) {
                    $query->where('teacher_id', auth('teacher')->id());
                })->first();
        }
        if ($security) {
            $courseEnrolls = $course->enrolls->where('enroll_semister_id', $semester->id);
            $courseEnrolls->each(function ($item, $key) use ($course, $semester) {
                $marks = StudentCourseMark::where('student_id', $item->student_id)->where('course_id', $course->id)->where('enroll_semister_id', $semester->id)->first();
                $item->marks = $marks;
            });

            $data['courseEnrolls'] = $courseEnrolls->sortBy('student.roll_no');
            $data['course'] = $course;
            return view('users.teacher.dashboard.studentmarksview')->with($data);
        } else {
            return back()->with('mgsErr', 'You have not permission to access this page');
        }
    }

    public function prevTeacherMarkStudentView($id)
    {

        $course = Course::where('id', $id)->with('enrolls.student')->first();

        $semester = $this->getPrevEnrollSemister();
        if ($course->type == "Practical") {
            $security = PracticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course->id)
                ->whereHas('practicalAssignedTeacher', function ($query) {
                    $query->where('teacher_id', auth('teacher')->id());
                })->first();
        } else {
            $security = TheoriticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course->id)
                ->whereHas('theoryAssignedTeachers', function ($query) {
                    $query->where('teacher_id', auth('teacher')->id());
                })->first();
        }
        if ($security) {
            $courseEnrolls = $course->enrolls->where('enroll_semister_id', $semester->id);
            $courseEnrolls->each(function ($item, $key) use ($course, $semester) {
                $marks = StudentCourseMark::where('student_id', $item->student_id)->where('course_id', $course->id)->where('enroll_semister_id', $semester->id)->first();
                $item->marks = $marks;
            });

            $data['courseEnrolls'] = $courseEnrolls->sortBy('student.roll_no');
            $data['course'] = $course;
            return view('users.teacher.dashboard.prev.studentmarksview')->with($data);
        } else {
            return back()->with('mgsErr', 'You have not permission to access this page');
        }
    }

    public function saveTeacherMarkStudent(Request $request, $id)
    {
        $semester = $this->getLastEnrollSemister();
        $studentIds = Course::where('id', $id)->with(['enrolls' => function ($query) use ($semester) {
            $query->where('enroll_semister_id', $semester->id);
        }])->first()->enrolls->pluck('student_id');
//        $request['stuedentsId']=$studentIds->toArray();
//        dd($request->all());

        $course = Course::find($id);

        if ($course->type == "Practical") {
            $security = PracticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course->id)
                ->where('status', 0)
                ->whereHas('practicalAssignedTeacher', function ($query) {
                    $query->where('teacher_id', auth('teacher')->id());
                })->first();
        } else {
            $security = TheoriticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course->id)
                ->where('status', 0)
                ->whereHas('theoryAssignedTeachers', function ($query) {
                    $query->where('teacher_id', auth('teacher')->id());
                })->first();
        }
        if ($security) {
            $data = array();
            foreach ($studentIds as $key => $student) {
                $data[$key]['course_id'] = $id;
                $data[$key]['enroll_semister_id'] = $semester->id;
                $data[$key]['student_id'] = $student;
                $data[$key]['assignment'] = $request->assignment[$student];
                $data[$key]['mid'] = $request->mid[$student];
                $data[$key]['final'] = $request->final[$student];
            }
            StudentCourseMark::marksUpdateOrCreate($data);
            return redirect(route('teacher.home'))->with('mgs', 'Mark Entry Successfull');

        } else {
            return back()->with('mgsErr', 'You have not permission to access this page');
        }
    }

    public function prevSaveTeacherMarkStudent(Request $request, $id)
    {
        $semester = $this->getPrevEnrollSemister();
        $studentIds = Course::where('id', $id)->with(['enrolls' => function ($query) use ($semester) {
            $query->where('enroll_semister_id', $semester->id);
        }])->first()->enrolls->pluck('student_id');
//        $request['stuedentsId']=$studentIds->toArray();
//        dd($request->all());

        $course = Course::find($id);

        if ($course->type == "Practical") {
            $security = PracticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course->id)
                ->where('status', 0)
                ->whereHas('practicalAssignedTeacher', function ($query) {
                    $query->where('teacher_id', auth('teacher')->id());
                })->first();
        } else {
            $security = TheoriticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course->id)
                ->where('status', 0)
                ->whereHas('theoryAssignedTeachers', function ($query) {
                    $query->where('teacher_id', auth('teacher')->id());
                })->first();
        }
        if ($security) {
            $data = array();
            foreach ($studentIds as $key => $student) {
                $data[$key]['course_id'] = $id;
                $data[$key]['enroll_semister_id'] = $semester->id;
                $data[$key]['student_id'] = $student;
                $data[$key]['assignment'] = $request->assignment[$student];
                $data[$key]['mid'] = $request->mid[$student];
                $data[$key]['final'] = $request->final[$student];
            }
            StudentCourseMark::marksUpdateOrCreate($data);
            return redirect(route('teacher.home'))->with('mgs', 'Mark Entry Successfull');

        } else {
            return back()->with('mgsErr', 'You have not permission to access this page');
        }
    }

    public function submitMarksForReview($course_id)
    {
        $semester = $this->getLastEnrollSemister();
        $course = Course::find($course_id);
        if ($course->type == "Practical") {
            $security = PracticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course_id)
                ->whereHas('practicalAssignedTeacher', function ($query) {
                    $query->where('teacher_id', auth('teacher')->id());
                })->first();
        } else {
            $security = TheoriticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course_id)
                ->whereHas('theoryAssignedTeachers', function ($query) {
                    $query->where('teacher_id', auth('teacher')->id());
                })->first();
        }
        if ($security) {
            if ($security->status == 0) {
                $security->status = 1;
                $security->save();
                $msg = 'Marks Submitted for Review';
            } else {
                $msg = 'Marks Already Submitted for Review';
            }
            return back()->with('mgs', $msg);

        } else {
            return back()->with('mgsErr', 'You have not permission to access this page');
        }
    }

    public function prevSubmitMarksForReview($course_id)
    {
        $semester = $this->getPrevEnrollSemister();
        $course = Course::find($course_id);
        if ($course->type == "Practical") {
            $security = PracticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course_id)
                ->whereHas('practicalAssignedTeacher', function ($query) {
                    $query->where('teacher_id', auth('teacher')->id());
                })->first();
        } else {
            $security = TheoriticalTeacher::where('enroll_semister_id', $semester->id)
                ->where('course_id', $course_id)
                ->whereHas('theoryAssignedTeachers', function ($query) {
                    $query->where('teacher_id', auth('teacher')->id());
                })->first();
        }
        if ($security) {
            if ($security->status == 0) {
                $security->status = 1;
                $security->save();
                $msg = 'Marks Submitted for Review';
            } else {
                $msg = 'Marks Already Submitted for Review';
            }
            return back()->with('mgs', $msg);

        } else {
            return back()->with('mgsErr', 'You have not permission to access this page');
        }
    }
}
