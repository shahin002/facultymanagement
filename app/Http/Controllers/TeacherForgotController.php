<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class TeacherForgotController extends Controller
{
    //Sends Password Reset emails
    use SendsPasswordResetEmails;


    public function showResetForm()
    {
        return view('users.teacher.auth.sendReset');
    }


    public function __construct()
    {
        $this->middleware('guestTeacher');
    }
    public function broker()
    {
        return Password::broker('teacher');
    }
    protected function sendResetLinkResponse($response)
    {
        return redirect()->back()->with('status', trans($response))->with('mgs','Reset Code Sent!');
    }
}