<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Auth;

class TeacherResetController extends Controller
{
    //trait for handling reset Password
    use ResetsPasswords;
    protected $redirectTo = '/teacher';

    public function showResetForm(Request $request, $token = null)
    {

        return view('users.teacher.auth.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    public function broker()
    {
        return Password::broker('teacher');
    }

    protected function guard()
    {
        return Auth::guard('teacher');
    }
}
