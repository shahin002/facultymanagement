<?php

namespace App\Http\Controllers;

use PDF;
use Illuminate\Http\Request;
use App\traits\semisterToName;
use App\Enroll;
use App\DeanOfficer;

class getPdfController extends Controller
{
    use semisterToName;

    public function __construct()
    {
        $this->middleware('adminLogin');
    }

    public function downloadEnrollPdfByAdmin(Enroll $enroll)
    {
        $semisterName = $this->semisterToName($enroll->semister);
        $adminSignature = DeanOfficer::find(1)->signature;
        $data['enroll'] = $enroll;
        $data['semisterName'] = $semisterName;
        $data['adminSignature'] = $adminSignature;
        $pdf = PDF::loadView('pdf.enrollmentPdf', $data,[],[
            'format' => 'Legal'
        ]);
        return $pdf->stream(time().'enrollment.pdf');
    }

}
