<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\traits\getEnrolledFormData;
use App\traits\semisterToName;
use PDF;
use App\DeanOfficer;
use App\traits\getCurrentEnrollSemister;


class pdfController extends Controller
{
    use getCurrentEnrollSemister;
     public function __construct()
    {
        $this->middleware('studentLogin');
    }
    use getEnrolledFormData, semisterToName;
    // //genarate a pdf
    // public function downloadEnrollPdf(){

    // $enroll = $this->getEnrolledFormData();
    // $semisterName = $this->semisterToName($enroll->semister);
    //   $pdf = PDF::loadView('pdf', compact('enroll','semisterName'));
    //   return $pdf->download('invoice.pdf');
    // } 

    public function downloadEnrollPdf()
    {
        $currentSemister = $this->getLastEnrollSemister();
        $enroll = $this->getEnrolledFormData($currentSemister->id);
        $semisterName = $this->semisterToName($enroll->semister);
        $adminSignature = DeanOfficer::find(1)->signature;
        // return view('pdf', compact('enroll','semisterName'));
        $view = view('pdf.enrollmentPdf', compact('enroll','semisterName','adminSignature'))->render();
        // $pdf = PDF::loadHTML($view);
        // return $pdf->stream();
//        $pdf = PDF::loadView('pdf.enrollmentPdf', $data,[],[
//            'format' => 'Legal'
//        ]);
//        return $pdf->stream(time().'enrollment.pdf');
        $pdf = PDF::loadHTML($view,[
            'format' => 'Legal'
        ]);
        return $pdf->stream(time().'enrollment.pdf');
    }

}
   

