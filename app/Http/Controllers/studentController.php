<?php

namespace App\Http\Controllers;

use App\Certificate;
use App\CertificateRequest;
use App\DeanOfficer;
use App\Hall;
use App\Setting;
use Illuminate\Http\Request;
use App\EnrollSemister;
use App\Course;
use App\Enroll;
use App\Student;
use App\traits\semisterToName;
use App\traits\getEnrolledFormData;
use App\traits\getCurrentEnrollSemister;
use App\traits\signature;
use App\StudentInfo;
use App\CourseMoney;
use App\HallMoney;
use PDF;
use Image;

use Illuminate\Support\Facades\Auth;

class studentController extends Controller
{
    use semisterToName, getEnrolledFormData, getCurrentEnrollSemister, signature;

    public function __construct()
    {
        $this->middleware('studentLogin');
    }

    //show student homepage
    public function index()
    {
        return view('users.student.dashboard.home');
    }

    //add Student info form
    public function addInfo()
    {
        $student = auth('student')->user();
        $halls = Hall::all();
        return view('users.student.dashboard.profileInfo',compact('student','halls'));
    }

    //save intudent info
    public function saveInfo(Request $request)
    {
        $student=auth('student')->user();
        $this->validate($request, array(
            "name" => "required",
            "gender" => "required",
            "session" => "required",
            "email" => "required|unique:students,id,".$student->id,

            "father_name" => "required",
            "mother_name" => "required",
            "phone_number" => "required",
            'ssc_roll' => 'required',
            'ssc_reg' => 'required',
            'ssc_gpa' => 'required',
            'ssc_institute' => 'required',
            'ssc_board' => 'required',
            'ssc_year' => 'required',
            'hsc_roll' => 'required',
            'hsc_reg' => 'required',
            'hsc_gpa' => 'required',
            'hsc_institute' => 'required',
            'hsc_board' => 'required',
            'hsc_year' => 'required',
        ));
        $student->name=$request->name;
        $student->gender=$request->gender;
        $student->session=$request['session'];
        $student->email=$request->email;
        if ($student->save()){
            if ($student->studentInfo){
                $info = $student->studentInfo;
            }else{
                $info = new StudentInfo();
            }
            $info->student_id = auth('student')->user()->id;
            $info->father_name = $request->father_name;
            $info->mother_name = $request->mother_name;
            $info->phone_number = $request->phone_number;
            $info->ssc_roll = $request->ssc_roll;
            $info->ssc_reg = $request->ssc_reg;
            $info->ssc_gpa = $request->ssc_gpa;
            $info->ssc_institute = $request->ssc_institute;
            $info->ssc_board = $request->ssc_board;
            $info->ssc_year = $request->ssc_year;
            $info->hsc_roll = $request->hsc_roll;
            $info->hsc_reg = $request->hsc_reg;
            $info->hsc_gpa = $request->hsc_gpa;
            $info->hsc_institute = $request->hsc_institute;
            $info->hsc_board = $request->hsc_board;
            $info->hsc_year = $request->hsc_year;
            $info->save();

            return redirect(route('student.home'))->with('mgs', 'Your Information Successfully Saved!');
        }else{
            return back()->withInput()->with('mgsErr', 'Your Information can not saved');
        }




    }

    //Show Course Enroll form
    public function showEnrollForm()
    {
        //checking if any enrollment in database
        if (!$this->getCurrentEnrollSemister()) {
            return redirect(route('student.home'))->with('mgsErr', 'No Enrollment Running right now!');
        }
        $enrollSemister = $this->getCurrentEnrollSemister();

        //checking if the user already enrolled in current semister
        if ($enroll = Enroll::where('student_id', auth('student')->user()->id)->orderBy('created_at', 'desc')->first()) {
            if ($enroll->enroll_semister_id == $enrollSemister->id) {
                return redirect(route('student.enrolled'))->with('mgsErr', 'You Already Enrolled in this Semester');
            }
        }

        $courses = $this->authUserCourses();
        return view('users.student.dashboard.enrollCourse', compact('courses', 'enrollSemister'));
    }

    //save course Enroll Form
    public function saveEnrollForm(Request $request)
    {

        //validate form data
        $this->validate($request, array(
            'semister' => 'required',
            'previous_gpa' => 'required|numeric|min:0|max:4',
            'cgpa' => 'required|numeric|min:0|max:4',
            'student_status' => 'required',
            'courses' => 'required',
        ));

        //saving data to database
        $enroll = new Enroll();
        $enroll->enroll_semister_id = $request->enroll_semister_id;
        $enroll->student_id = auth('student')->user()->id;
        $enroll->semister = $request->semister;
        $enroll->previous_gpa = $request->previous_gpa;
        $enroll->cgpa = $request->cgpa;
        $enroll->account_status = "0";
        $enroll->hall_status = "0";
        $enroll->student_status = $request->student_status;
        $enroll->has_retake = 0;
        $enroll->save();

        $enroll->courses()->sync($request->courses, true);
        $enroll->has_retake = 0;
        foreach ($enroll->courses as $course) {
            if ($course->semister < $enroll->semister) {
                $enroll->has_retake = 1;
                break;
            }
        }
        $enroll->save();
        return redirect(route('student.enrolled'))->with('mgs', "Successfully Enrolled!");
    }

    //show enrolled Course student copy
    public function showEnrolledForm()
    {

        $semister = $this->getLastEnrollSemister();
        if ($semister) {
            if ($this->getEnrolledFormData($semister->id)) {
                $enroll = $this->getEnrolledFormData($semister->id);
                $semisterName = $this->semisterToName($enroll->semister);
                $adminSignature = DeanOfficer::find(1)->signature;
                return view('users.student.dashboard.enrolledForm', compact('enroll', 'semisterName', 'adminSignature'));
            }
            return redirect(route('student.enrollForm'))->with('mgsErr', "You haven't Enroll in this Semester");
        }
        return redirect()->back()->with('mgsErr', 'Enrollment is not Starting Yet!');


    }

    //respond ajax request get courses
    public function getCourses()
    {
        return $courses = $this->authUserCourses();
    }

    //get courses for current semester
    private function authUserCourses()
    {
        $enrollSemister = EnrollSemister::orderBy('id', 'desc')->where('status', '1')->first();
        if ($enrollSemister->month == 0) {
            $courses = Course::whereNotIN('semister', [2, 4, 6, 8])->orderBy('semister', 'desc')->get();
        } else {
            $courses = Course::whereNotIN('semister', [1, 3, 5, 7])->orderBy('semister', 'desc')->get();
        }
        return $courses;
    }

    //Update Login info form
    public function profileForm()
    {
        return view('users.student.dashboard.editProfile');
    }

    //Update  login info
    public function updateProfile(Request $request)
    {
        $this->validate($request, array(
            'email' => 'required',
            'password' => 'required|min:6|confirmed',
        ));
        $student = auth('student')->user();
        $student->password = bcrypt($request->password);
        if ($student->save()) {
            return redirect(route('student.home'))->with('mgs', 'Successfully Updates!');
        }

    }

    public function addCourseReceipt()
    {
        $semister = $this->getLastEnrollSemister();
        $retakeSemister = array();
        $receiptSemister = array();
        if ($semister) {
            $enroll = $this->getEnrolledFormData($semister->id);
            if ($enroll) {
                if (CourseMoney::where('enroll_id', $enroll->id)->get()) {
                    $receipts = CourseMoney::where('enroll_id', $enroll->id)->get();
                    foreach ($receipts as $receipt) {
                        $receiptSemister[] = $receipt->semister;
                    }
                }

                foreach ($enroll->courses as $course) {
                    if (in_array($course->semister, $receiptSemister)) {
                        continue;
                    }
//                        if ($enroll->has_retake == 1) {
                    if (!in_array($course->semister, $retakeSemister)) {
                        $retakeSemister[] = $course->semister;
                    }

//                    }
                    // return $retakeSemister;
                }
                return view('users.student.dashboard.addCourseReceipt', compact('enroll', 'retakeSemister'));

            } else {

                return redirect(route('student.enrollForm'))->with('mgsErr', "You haven't Enroll in this Semester");

            }

        } else {
            return redirect(route('student.home'))->with('mgsErr', 'No Enrollment Running right now!');

        }

    }

    public function saveCourseReceipt(Request $request)
    {
        $this->validate($request, array(
            'receipt' => 'required',
            'amount' => 'required'
        ));

        $courseMoney = new CourseMoney();
        $courseMoney->enroll_id = $request->enroll_id;
        $courseMoney->receipt = $request->receipt;
        $courseMoney->amount = $request->amount;
        $courseMoney->semister = $request->semister;
        $courseMoney->save();
        return redirect(route('add.courseReceipt'))->with('mgs', 'Money Receipt submitted!');
    }

    //add hall money receipt form
    public function addHallReceipt()
    {
        $receipt = null;
        $semister = $this->getLastEnrollSemister();
        if ($semister) {
            $enroll = $this->getEnrolledFormData($semister->id);
            if ($enroll) {
                if (HallMoney::where('enroll_id', $enroll->id)->first()) {
                    $receipt = HallMoney::where('enroll_id', $enroll->id)->first();
                }
                return view('users.student.dashboard.addHallReceipt', compact('enroll', 'receipt'));

            }
            return redirect(route('student.enrollForm'))->with('mgsErr', "You haven't Enroll in this Semester");
        }
        return redirect(route('student.home'))->with('mgsErr', 'No Enrollment Running right now!');

    }

    //save hall money receipt
    public function saveHallReceipt(Request $request)
    {
        $this->validate($request, array(
            'semister' => 'required',
            'enroll_id' => 'required',
            'receipt' => 'required',
            'amount' => 'required'
        ));
        $hallmoney = new HallMoney();
        $hallmoney->enroll_id = $request->enroll_id;
        $hallmoney->receipt = $request->receipt;
        $hallmoney->amount = $request->amount;
        $hallmoney->semister = $request->semister;
        $hallmoney->save();

        return redirect()->back()->with('mgs', 'Hall Money Receipt Successfully Submitted');
    }


    //add signature form
    public function showAddSignatureForm()
    {
        return view('users.student.dashboard.signature');
    }

    //add signature
    public function addSignature(Request $request)
    {
        $student = auth('student')->user();
        $this->signature($request, $student);
        return redirect(route('student.home'))->with('mgs', 'Signature Uploaded!');
    }

    // View Admit Card if Admit card download system is running
    public function viewAdmitCard()
    {
        $semister = $this->getLastEnrollSemister();
        if ($semister) {
            if ($semister->admit_status == 0) {
                return redirect(route('student.home'))->with('mgsErr', 'Admit Card Not Available yet!');
            }
            $enroll = $this->getEnrolledFormData($semister->id);
            if ($enroll) {
                if ($enroll->account_status == 1 && $enroll->hall_status == 1) {
                    $admitSemister = ($enroll->courses->unique('semister'))->pluck('semister');
                    return view('users.student.dashboard.admit', compact('enroll', 'admitSemister'));
                } else {
                    return back()->with('mgsErr', "You haven't Paid Your Hall Or Enroll Money.");
                }

            } else {

                return redirect(route('student.enrollForm'))->with('mgsErr', "You haven't Enroll in this Semester");

            }

        } else {
            return redirect(route('student.home'))->with('mgsErr', 'No Enrollment Running right now!');

        }


    }

    public function downloadAdmit()
    {
        $semister = $this->getLastEnrollSemister();
        if ($semister) {
            if ($semister->admit_status == 0) {
                return redirect(route('student.home'))->with('mgsErr', 'Admit Card Not Available yet!');
            }
            $enroll = $this->getEnrolledFormData($semister->id);
            if ($enroll) {
                if ($enroll->account_status == 1 && $enroll->hall_status == 1) {
                    $admitSemister = ($enroll->courses->unique('semister'))->pluck('semister');
                    $semisterName = array();
                    foreach ($admitSemister as $item) {
                        $semisterName[$item] = $this->semisterToName($item);
                    }
                    $exam_cntrl_sig = (new Setting())->get('exam_controller_signature');
                    $view = view('users.student.dashboard.admitPdf', compact('enroll', 'admitSemister', 'semisterName','exam_cntrl_sig'))->render();
                    // $pdf = PDF::loadHTML($view);
                    // return $pdf->stream();
                    $pdf = PDF::loadHTML($view, [
                        'mode' => 'utf-8'
                    ]);
                    $pdf->autoScriptToLang = true;
                    $pdf->autoLangToFont = true;
                    return $pdf->stream(time() . 'admit.pdf');
                } else {
                    return back()->with('mgsErr', "You haven't Paid Your Hall and Enroll Money.");
                }


            } else {

                return redirect(route('student.enrollForm'))->with('mgsErr', "You haven't Enroll in this Semester");

            }

        } else {
            return redirect(route('student.home'))->with('mgsErr', 'No Enrollment Running right now!');

        }
    }

//    certificate request
    public function certificateRequests()
    {
        $data = array();
        $data['request_list'] = CertificateRequest::where('student_id', auth('student')->id())->get();
        return view('users.student.dashboard.requests.request_list')->with($data);
    }

    public function createCertificateRequests()
    {

        $data = array();
        $data['certificates'] = Certificate::all();
//        dd($data['certificates']);
        return view('users.student.dashboard.requests.addCertificateRequest')->with($data);
    }

    public function saveCertificateRequests(Request $request)
    {
        $request->validate([
            'certificate_id' => 'required',
            'money_amount' => 'required',
            'received_number' => 'required',
            'comment' => 'required'
        ]);
        $certificate = new CertificateRequest();
        $certificate->certificate_id = $request->certificate_id;
        $certificate->student_id = auth('student')->id();
        $certificate->money_amount = $request->money_amount;
        $certificate->received_number = $request->received_number;
        $certificate->comment = $request->comment;
        if ($certificate->save()) {
            return redirect(route('student.manage.certificat.requests'))->with('mgs', 'Your Request Successfully Saved!');
        } else {
            return back()->with('mgsErr', 'Your Request Can not Saved');
        }
    }

    public function imageUpload()
    {
        return view('users.student.dashboard.imageupload');
    }

    public function saveImage(Request $request)
    {
        $student = auth('student')->user();
        $image = $request->file('image');
        $path = "public/users/images/photo/" . $student->name . time() . "." . $image->getClientOriginalExtension();
        $img = Image::make($image->getRealPath())->resize(120, 150);
        $img->save($path);
        $student->image = $path;
        if ($student->save()) {
            return redirect(route('student.home'))->with('mgs', 'Profile Image Uploaded!');
        } else {
            return back()->with('mgsErr', 'Can not Updated');
        }
    }

//    end certificate request

//download certificate
    public function downloadCertificateRequest($id)
    {
        $certificate = CertificateRequest::find($id);

//        $admin = DeanOfficer::where('id', 1)->select('name', 'signature')->first();
        $admin = @(new Setting())->get('dean');
        if ($certificate->certificate_id == 5) {
            $view = view('pdf.certificates.computer', compact('certificate', 'admin'))->render();
        } elseif ($certificate->certificate_id == 6) {
            $view = view('pdf.certificates.eng_pro', compact('certificate', 'admin'))->render();
        } elseif ($certificate->certificate_id == 7) {
            $view = view('pdf.certificates.incountry', compact('certificate', 'admin'))->render();
        } elseif ($certificate->certificate_id == 8) {
            $view = view('pdf.certificates.medium', compact('certificate', 'admin'))->render();
        } elseif ($certificate->certificate_id == 9) {
            $view = view('pdf.certificates.saarc', compact('certificate', 'admin'))->render();
        }
        $certificate->status = 3;
        $certificate->save();
        $pdf = PDF::loadHTML($view);
        return $pdf->stream(time() . 'certificate.pdf');

    }

}
