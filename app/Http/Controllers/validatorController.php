<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\traits\check;

class validatorController extends Controller
{
    use check;
    //check student email validity when editings
    public function studentEmail(Request $request){
         $emails = Student::select('email')->get();
        foreach($emails as $email){
            if($email->email == $request->email && $email->email != $request->currentEmail){
                return 'false';
            }
        }
        return 'true';
    }
    public function roll(Request $request){
        $rolls = Student::select('roll_no')->get();
        foreach($rolls as $roll){
            if($roll->roll_no == $request->roll && $roll->roll_no != $request->currentRoll){
                return 'false';
            }
        }
        return 'true';
    }

    public function reg(Request $request){
        $all_reg = Student::select('reg_no')->get();
        foreach($all_reg as $reg){
            if($reg->reg_no == $request->reg_no && $reg->reg_no != $request->currentReg){
                return 'false';
            }
        }
        return 'true';
    }


}
