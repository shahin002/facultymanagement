<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PracticalTeacher extends Model
{
    //

    public function course(){
        return $this->belongsTo('App\Course');
    }
    public function practicalAssignedTeacher(){
        return $this->hasMany('App\PracticalAssignedTeacher');
    }
}
