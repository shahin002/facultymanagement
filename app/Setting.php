<?php

namespace App;


use Illuminate\Support\Facades\Storage;

class Setting
{

    protected $filename = 'settings.json';

    protected $settings;

    public function __construct()
    {
        // Load the file and store the contents in $this->settings
        $this->load();
    }

    public function get($key = null)
    {
        $value = array_get($this->settings, $key);
        return $value;
    }

    public function set($key, $value)
    {
        array_set($this->settings, $key, $value);
        $this->save();
    }

    public function forget($deleteKey)
    {
        array_forget($this->settings, $deleteKey);
        $this->save();
    }

    public function has($searchKey)
    {
        if (array_has($this->settings, $searchKey)) {
            return true;
        } else {
            return false;
        }
    }

    public function load()
    {
        if (Storage::exists($this->filename)) {
            $this->settings = json_decode(Storage::get($this->filename), true);
        } else {
            $this->settings = array();
        }
    }

    public function save()
    {
        $content = json_encode($this->settings);
        Storage::put($this->filename, $content);
    }

    public function clear()
    {
        $this->settings = array();
        $this->save();
    }

    public function setArray(array $data)
    {
        foreach ($data as $key => $value) {
            array_set($this->settings, $key, $value);
        }
        $this->save();
    }
}
