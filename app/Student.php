<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\StudentResetPasswordNotification;

class Student extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'roll_no', 'reg_no', 'hall_id', 'session', 'gender', 'email',  'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function studentInfo(){
        return $this->hasOne('App\StudentInfo');
    }

    public function enrolls(){
        return $this->hasMany('App\Enroll');
    }
    public function hall(){
        return $this->belongsTo('App\Hall');
    }

  public function sendPasswordResetNotification($token)
  {
      $this->notify(new StudentResetPasswordNotification($token));
  }

}
