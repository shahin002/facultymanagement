<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentCourseMark extends Model
{
    protected $fillable = [
        'course_id',
        'enroll_semister_id',
        'student_id',
        'assignment',
        'mid',
        'final'
    ];

    public static function marksUpdateOrCreate(array $marks)
    {
        foreach ($marks as $mark) {
            static::updateOrCreate([
                'course_id' => $mark['course_id'],
                'enroll_semister_id' => $mark['enroll_semister_id'],
                'student_id' => $mark['student_id'],
            ], $mark);
        }
    }
}
