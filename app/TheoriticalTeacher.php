<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TheoriticalTeacher extends Model
{
    //
    public function course(){
        return $this->belongsTo('App\Course');
    }
    public function theoryAssignedTeachers(){
        return $this->hasMany('App\TheoryAssignedTeacher');
    }
}
