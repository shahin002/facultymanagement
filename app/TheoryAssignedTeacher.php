<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TheoryAssignedTeacher extends Model
{
    public function theoriticalTeacher(){
        return $this->belongsTo('App\TheoriticalTeacher');
    }
    public function teacher(){
        return $this->belongsTo('App\Teacher');
    }
}
