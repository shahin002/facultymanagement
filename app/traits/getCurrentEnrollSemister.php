<?php

namespace App\traits;

use App\EnrollSemister;

trait getCurrentEnrollSemister
{
    // get current  running enroll semester
    public function getCurrentEnrollSemister()
    {
        $currentSemester = EnrollSemister::where('year', date('Y'))->where('month', date('m') < 7 ? 0 : 1)->where('status', '1')->first();
        if ($currentSemester) {
            return $currentSemester;
        } else {
            return false;
        }
    }

    // get current  enroll semester
    public function getLastEnrollSemister()
    {
        $lastEnrollSemester = EnrollSemister::where('year', date('Y'))->where('month', date('m') < 7 ? 0 : 1)->first();
        if ($lastEnrollSemester) {
            return $lastEnrollSemester;
        } else {
            return false;
        }
    }

    // get prev  enroll semester
    public function getPrevEnrollSemister()
    {
        $date = date('m') < 7 ? 0 : 1;
        if ($date == 0) {
            $year = date('Y') - 1;
            $month = 1;

        } else {
            $year = date('Y');
            $month = 0;
        }
        $lastEnrollSemester = EnrollSemister::where('year', $year)->where('month', $month)->first();
        if ($lastEnrollSemester) {
            return $lastEnrollSemester;
        } else {
            return false;
        }
    }
}