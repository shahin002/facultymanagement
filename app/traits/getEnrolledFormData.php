<?php

namespace App\traits;

use App\Enroll;

trait getEnrolledFormData
{
    public function getEnrolledFormData($semister)
    {
        if ($enroll = Enroll::where('student_id', auth('student')->user()->id)->where('enroll_semister_id', $semister)
            ->orderBy('created_at', 'desc')
            ->with('student')
            ->with('courses')
            ->first()) {

            return $enroll;
        } else {
            return false;
        }
    }

}