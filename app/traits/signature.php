<?php
namespace App\traits;
use Image;

trait signature{
    public function signature($request, $user){
        $image = $request->file('signature');
        $path = "public/users/images/signature/".str_replace(' ', '', $user->name).time().".".$image->getClientOriginalExtension();
        $img = Image::make($image->getRealPath())->resize(100,60);
        $img->save($path);
        $user->signature = $path;
        $user->save();
    }
}