<?php

return [
    'font_path' => base_path('public/pdf/fonts/'),
    'font_data' => [
        "shahin" => [
            'R' => "MTCORSVA.TTF"
        ],
    ],
	'mode'                  => 'utf-8',
	'format'                => 'A4',
	'author'                => '',
	'subject'               => '',
	'keywords'              => '',
	'creator'               => 'Laravel Pdf',
	'display_mode'          => 'fullpage',
	'tempDir'               => base_path('../temp/')
];
