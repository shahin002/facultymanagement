-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 26, 2018 at 04:07 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ag_faculty`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_officers`
--

CREATE TABLE `account_officers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `signature` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `account_password_reset`
--

CREATE TABLE `account_password_reset` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `certificates`
--

CREATE TABLE `certificates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `money` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `certificates`
--

INSERT INTO `certificates` (`id`, `name`, `money`, `description`, `created_at`, `updated_at`) VALUES
(5, 'COMPUTER  LITERACY  CERTIFICATE', '200', 'dummy Text', '2018-04-25 02:48:18', '2018-04-25 02:48:18'),
(6, 'ENGLISH PROFICIENCY CERTIFICATE', '200', 'dummy text', '2018-04-25 02:49:01', '2018-04-25 02:49:01'),
(7, 'IN-COUNTRY STUDY TOUR TESTIMONIAL', '200', 'dummy text', '2018-04-25 02:49:49', '2018-04-25 02:49:49'),
(8, 'THE MEDIUM OF INSTRUCTION', '200', 'dummy text', '2018-04-25 02:51:33', '2018-04-25 02:51:33'),
(9, 'SAARC CERTIFICATE', '200', 'Dummy Text', '2018-04-25 02:52:18', '2018-04-25 02:52:18');

-- --------------------------------------------------------

--
-- Table structure for table `certificate_requests`
--

CREATE TABLE `certificate_requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `certificate_id` int(10) UNSIGNED NOT NULL,
  `student_id` int(10) UNSIGNED NOT NULL,
  `money_amount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `received_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `chairman_password_reset`
--

CREATE TABLE `chairman_password_reset` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `department_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `credit` int(11) NOT NULL,
  `semister` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `course_enroll`
--

CREATE TABLE `course_enroll` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `enroll_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `course_moneys`
--

CREATE TABLE `course_moneys` (
  `id` int(10) UNSIGNED NOT NULL,
  `enroll_id` int(10) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL,
  `receipt` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `semister` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dean_officers`
--

CREATE TABLE `dean_officers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `signature` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dean_officers`
--

INSERT INTO `dean_officers` (`id`, `name`, `designation`, `phone_number`, `signature`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Md. Jashim Uddin', 'Assistant Registrar', '+8801721923647', NULL, 'jashimpstu3rd@gmail.com', '$2y$12$i7nLdPODm.pzZWGtFu0rzerwJ.EnABfa4q920hgPSZ6lrN4kcZVbG', 'GmPr5k96GbBQhXCvVBJ9bEksPAL91odGWjOgwTHkcWhAhoDIFkr9NDxux1L8', '2018-05-25 06:36:07', '2018-05-25 06:36:30');

-- --------------------------------------------------------

--
-- Table structure for table `dean_officer_password_reset`
--

CREATE TABLE `dean_officer_password_reset` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `department_chairmen`
--

CREATE TABLE `department_chairmen` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `enrolls`
--

CREATE TABLE `enrolls` (
  `id` int(10) UNSIGNED NOT NULL,
  `enroll_semister_id` int(10) UNSIGNED NOT NULL,
  `student_id` int(10) UNSIGNED NOT NULL,
  `semister` int(11) NOT NULL,
  `previous_gpa` double NOT NULL,
  `cgpa` double NOT NULL,
  `account_status` tinyint(4) NOT NULL,
  `account_officer_id` int(11) DEFAULT NULL,
  `hall_status` tinyint(4) NOT NULL,
  `student_status` tinyint(4) NOT NULL,
  `has_retake` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `enroll_semisters`
--

CREATE TABLE `enroll_semisters` (
  `id` int(10) UNSIGNED NOT NULL,
  `year` int(11) NOT NULL,
  `month` tinyint(4) NOT NULL,
  `session` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `admit_status` tinyint(3) NOT NULL,
  `exam_date` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `halls`
--

CREATE TABLE `halls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hall_officer_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hall_moneys`
--

CREATE TABLE `hall_moneys` (
  `id` int(10) UNSIGNED NOT NULL,
  `enroll_id` int(10) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL,
  `receipt` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `semister` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hall_officers`
--

CREATE TABLE `hall_officers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `signature` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hall_officer_password_reset`
--

CREATE TABLE `hall_officer_password_reset` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(61, '2017_08_02_110620_add_column_studentStatus_to_enrolls', 2),
(100, '2014_10_12_000000_create_users_table', 3),
(101, '2014_10_12_100000_create_password_resets_table', 3),
(105, '2017_07_31_181111_create_hall_officers_table', 3),
(107, '2017_07_31_181328_create_enroll_semisters_table', 3),
(109, '2017_07_31_181550_create_dean_officers_table', 3),
(110, '2017_07_31_181618_create_account_officers_table', 3),
(112, '2017_08_19_090610_create_dean_officers_password_reset_table', 3),
(113, '2017_08_20_090610_create_student_password_reset_table', 4),
(114, '2017_08_21_090610_create_account_password_reset_table', 5),
(121, '2017_09_23_150538_create_theoritical_teachers_table', 8),
(122, '2017_09_23_150921_create_practical_teachers_table', 8),
(123, '2017_09_23_145211_create_departments_table', 9),
(126, '2017_07_31_181131_create_courses_table', 11),
(127, '2017_09_23_160720_create_department_chairmen_table', 12),
(128, '2017_07_31_180855_create_students_table', 13),
(129, '2017_07_31_181011_create_student_infos_table', 14),
(130, '2017_07_31_181041_create_halls_table', 15),
(132, '2017_07_31_181440_create_enrolls_table', 16),
(133, '2017_07_31_181618_create_course_enroll_table', 17),
(134, '2017_09_04_073359_create_course_moneys_table', 18),
(135, '2017_09_04_073452_create_hall_moneys_table', 19),
(136, '2018_03_03_190653_create_teachers_table', 20),
(138, '2018_03_06_024717_create_theory_assigned_teachers_table', 21),
(139, '2018_03_06_194019_create_practical_assigned_teachers_table', 22),
(140, '2018_03_19_153625_create_certificates_table', 23),
(141, '2018_03_19_153558_create_certificate_requests_table', 24),
(142, '2018_03_25_052714_create_student_course_marks_table', 25),
(143, '2017_08_21_0906101_create_hall_officer_password_reset_table', 26),
(144, '2017_08_21_0906102_create_teacher_password_reset_table', 27),
(145, '2017_08_21_0906103_create_chairman_password_reset_table', 28);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `practical_assigned_teachers`
--

CREATE TABLE `practical_assigned_teachers` (
  `id` int(10) UNSIGNED NOT NULL,
  `practical_teacher_id` int(10) UNSIGNED NOT NULL,
  `teacher_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `practical_teachers`
--

CREATE TABLE `practical_teachers` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_id` int(11) NOT NULL,
  `enroll_semister_id` int(11) NOT NULL,
  `ext_teachers` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department_chairmen_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roll_no` int(11) NOT NULL,
  `reg_no` int(11) NOT NULL,
  `hall_id` int(10) UNSIGNED NOT NULL,
  `session` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` tinyint(4) NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student_course_marks`
--

CREATE TABLE `student_course_marks` (
  `id` int(10) UNSIGNED NOT NULL,
  `student_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `enroll_semister_id` int(11) NOT NULL,
  `assignment` double NOT NULL,
  `mid` double NOT NULL,
  `final` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student_infos`
--

CREATE TABLE `student_infos` (
  `id` int(10) UNSIGNED NOT NULL,
  `student_id` int(10) UNSIGNED NOT NULL,
  `father_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mother_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ssc_roll` int(11) NOT NULL,
  `ssc_reg` int(11) NOT NULL,
  `ssc_gpa` double NOT NULL,
  `ssc_institute` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ssc_board` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ssc_year` int(11) NOT NULL,
  `hsc_roll` int(11) NOT NULL,
  `hsc_reg` int(11) NOT NULL,
  `hsc_gpa` double NOT NULL,
  `hsc_institute` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hsc_board` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hsc_year` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `student_password_reset`
--

CREATE TABLE `student_password_reset` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(10) UNSIGNED NOT NULL,
  `department_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `teacher_password_reset`
--

CREATE TABLE `teacher_password_reset` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `theoritical_teachers`
--

CREATE TABLE `theoritical_teachers` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_id` int(11) NOT NULL,
  `enroll_semister_id` int(11) NOT NULL,
  `department_chairmen_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `theory_assigned_teachers`
--

CREATE TABLE `theory_assigned_teachers` (
  `id` int(10) UNSIGNED NOT NULL,
  `theoritical_teacher_id` int(10) UNSIGNED NOT NULL,
  `teacher_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_officers`
--
ALTER TABLE `account_officers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_officers_email_unique` (`email`);

--
-- Indexes for table `account_password_reset`
--
ALTER TABLE `account_password_reset`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_password_reset_email_index` (`email`),
  ADD KEY `account_password_reset_token_index` (`token`);

--
-- Indexes for table `certificates`
--
ALTER TABLE `certificates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `certificate_requests`
--
ALTER TABLE `certificate_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `certificate_requests_certificate_id_foreign` (`certificate_id`),
  ADD KEY `certificate_requests_student_id_foreign` (`student_id`);

--
-- Indexes for table `chairman_password_reset`
--
ALTER TABLE `chairman_password_reset`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chairman_password_reset_email_index` (`email`),
  ADD KEY `chairman_password_reset_token_index` (`token`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `courses_department_id_foreign` (`department_id`);

--
-- Indexes for table `course_enroll`
--
ALTER TABLE `course_enroll`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_enroll_course_id_foreign` (`course_id`),
  ADD KEY `course_enroll_enroll_id_foreign` (`enroll_id`);

--
-- Indexes for table `course_moneys`
--
ALTER TABLE `course_moneys`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_moneys_enroll_id_foreign` (`enroll_id`);

--
-- Indexes for table `dean_officers`
--
ALTER TABLE `dean_officers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dean_officers_email_unique` (`email`);

--
-- Indexes for table `dean_officer_password_reset`
--
ALTER TABLE `dean_officer_password_reset`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dean_officer_password_reset_email_index` (`email`),
  ADD KEY `dean_officer_password_reset_token_index` (`token`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department_chairmen`
--
ALTER TABLE `department_chairmen`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `department_chairmen_email_unique` (`email`),
  ADD KEY `department_chairmen_department_id_foreign` (`department_id`);

--
-- Indexes for table `enrolls`
--
ALTER TABLE `enrolls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `enrolls_enroll_semister_id_foreign` (`enroll_semister_id`),
  ADD KEY `enrolls_student_id_foreign` (`student_id`);

--
-- Indexes for table `enroll_semisters`
--
ALTER TABLE `enroll_semisters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `halls`
--
ALTER TABLE `halls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `halls_hall_officer_id_foreign` (`hall_officer_id`);

--
-- Indexes for table `hall_moneys`
--
ALTER TABLE `hall_moneys`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hall_moneys_enroll_id_foreign` (`enroll_id`);

--
-- Indexes for table `hall_officers`
--
ALTER TABLE `hall_officers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `hall_officers_email_unique` (`email`);

--
-- Indexes for table `hall_officer_password_reset`
--
ALTER TABLE `hall_officer_password_reset`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hall_officer_password_reset_email_index` (`email`),
  ADD KEY `hall_officer_password_reset_token_index` (`token`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `practical_assigned_teachers`
--
ALTER TABLE `practical_assigned_teachers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `practical_assigned_teachers_practical_teacher_id_foreign` (`practical_teacher_id`),
  ADD KEY `practical_assigned_teachers_teacher_id_foreign` (`teacher_id`);

--
-- Indexes for table `practical_teachers`
--
ALTER TABLE `practical_teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `students_roll_no_unique` (`roll_no`),
  ADD UNIQUE KEY `students_email_unique` (`email`),
  ADD KEY `students_hall_id_foreign` (`hall_id`);

--
-- Indexes for table `student_course_marks`
--
ALTER TABLE `student_course_marks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_infos`
--
ALTER TABLE `student_infos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_infos_student_id_foreign` (`student_id`);

--
-- Indexes for table `student_password_reset`
--
ALTER TABLE `student_password_reset`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_password_reset_email_index` (`email`),
  ADD KEY `student_password_reset_token_index` (`token`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `teachers_email_unique` (`email`),
  ADD KEY `teachers_department_id_foreign` (`department_id`);

--
-- Indexes for table `teacher_password_reset`
--
ALTER TABLE `teacher_password_reset`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teacher_password_reset_email_index` (`email`),
  ADD KEY `teacher_password_reset_token_index` (`token`);

--
-- Indexes for table `theoritical_teachers`
--
ALTER TABLE `theoritical_teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `theory_assigned_teachers`
--
ALTER TABLE `theory_assigned_teachers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `theory_assigned_teachers_theoritical_teacher_id_foreign` (`theoritical_teacher_id`),
  ADD KEY `theory_assigned_teachers_teacher_id_foreign` (`teacher_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_officers`
--
ALTER TABLE `account_officers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `account_password_reset`
--
ALTER TABLE `account_password_reset`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `certificates`
--
ALTER TABLE `certificates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `certificate_requests`
--
ALTER TABLE `certificate_requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chairman_password_reset`
--
ALTER TABLE `chairman_password_reset`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `course_enroll`
--
ALTER TABLE `course_enroll`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `course_moneys`
--
ALTER TABLE `course_moneys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dean_officers`
--
ALTER TABLE `dean_officers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `dean_officer_password_reset`
--
ALTER TABLE `dean_officer_password_reset`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `department_chairmen`
--
ALTER TABLE `department_chairmen`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `enrolls`
--
ALTER TABLE `enrolls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `enroll_semisters`
--
ALTER TABLE `enroll_semisters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `halls`
--
ALTER TABLE `halls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hall_moneys`
--
ALTER TABLE `hall_moneys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hall_officers`
--
ALTER TABLE `hall_officers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hall_officer_password_reset`
--
ALTER TABLE `hall_officer_password_reset`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- AUTO_INCREMENT for table `practical_assigned_teachers`
--
ALTER TABLE `practical_assigned_teachers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `practical_teachers`
--
ALTER TABLE `practical_teachers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_course_marks`
--
ALTER TABLE `student_course_marks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_infos`
--
ALTER TABLE `student_infos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_password_reset`
--
ALTER TABLE `student_password_reset`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teacher_password_reset`
--
ALTER TABLE `teacher_password_reset`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `theoritical_teachers`
--
ALTER TABLE `theoritical_teachers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `theory_assigned_teachers`
--
ALTER TABLE `theory_assigned_teachers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `certificate_requests`
--
ALTER TABLE `certificate_requests`
  ADD CONSTRAINT `certificate_requests_certificate_id_foreign` FOREIGN KEY (`certificate_id`) REFERENCES `certificates` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `certificate_requests_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `courses_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `course_enroll`
--
ALTER TABLE `course_enroll`
  ADD CONSTRAINT `course_enroll_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `course_enroll_enroll_id_foreign` FOREIGN KEY (`enroll_id`) REFERENCES `enrolls` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `course_moneys`
--
ALTER TABLE `course_moneys`
  ADD CONSTRAINT `course_moneys_enroll_id_foreign` FOREIGN KEY (`enroll_id`) REFERENCES `enrolls` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `department_chairmen`
--
ALTER TABLE `department_chairmen`
  ADD CONSTRAINT `department_chairmen_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `enrolls`
--
ALTER TABLE `enrolls`
  ADD CONSTRAINT `enrolls_enroll_semister_id_foreign` FOREIGN KEY (`enroll_semister_id`) REFERENCES `enroll_semisters` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `enrolls_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `halls`
--
ALTER TABLE `halls`
  ADD CONSTRAINT `halls_hall_officer_id_foreign` FOREIGN KEY (`hall_officer_id`) REFERENCES `hall_officers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `hall_moneys`
--
ALTER TABLE `hall_moneys`
  ADD CONSTRAINT `hall_moneys_enroll_id_foreign` FOREIGN KEY (`enroll_id`) REFERENCES `enrolls` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `practical_assigned_teachers`
--
ALTER TABLE `practical_assigned_teachers`
  ADD CONSTRAINT `practical_assigned_teachers_practical_teacher_id_foreign` FOREIGN KEY (`practical_teacher_id`) REFERENCES `practical_teachers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `practical_assigned_teachers_teacher_id_foreign` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_hall_id_foreign` FOREIGN KEY (`hall_id`) REFERENCES `halls` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `student_infos`
--
ALTER TABLE `student_infos`
  ADD CONSTRAINT `student_infos_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `teachers`
--
ALTER TABLE `teachers`
  ADD CONSTRAINT `teachers_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `theory_assigned_teachers`
--
ALTER TABLE `theory_assigned_teachers`
  ADD CONSTRAINT `theory_assigned_teachers_teacher_id_foreign` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `theory_assigned_teachers_theoritical_teacher_id_foreign` FOREIGN KEY (`theoritical_teacher_id`) REFERENCES `theoritical_teachers` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
