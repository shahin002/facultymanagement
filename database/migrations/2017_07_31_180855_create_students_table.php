<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email',100)->unique();
            $table->integer('roll_no')->unique();
            $table->integer('reg_no');
            $table->integer('hall_id')->unsigned();
            $table->string('session');
            $table->string('signature')->nullable();
            $table->string('image')->nullable();
            $table->tinyInteger('gender');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('hall_id')
                ->references('id')->on('halls')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
