<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id')->unsigned();

            $table->string('father_name');
            $table->string('mother_name');
            $table->string('phone_number');

            $table->integer('ssc_roll');
            $table->integer('ssc_reg');
            $table->double('ssc_gpa');
            $table->string('ssc_institute');
            $table->string('ssc_board');
            $table->integer('ssc_year');
            $table->integer('hsc_roll');
            $table->integer('hsc_reg');
            $table->double('hsc_gpa');
            $table->string('hsc_institute');
            $table->string('hsc_board');
            $table->integer('hsc_year');
            $table->timestamps();
            $table->foreign('student_id')
                ->references('id')->on('students')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_infos');
    }
}
