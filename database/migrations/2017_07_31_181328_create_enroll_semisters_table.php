<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnrollSemistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enroll_semisters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('year');
            $table->tinyInteger('month');
            $table->string('session');
            $table->tinyInteger('status');
            $table->tinyInteger('admit_status');
            $table->string('exam_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enroll_semisters');
    }
}
