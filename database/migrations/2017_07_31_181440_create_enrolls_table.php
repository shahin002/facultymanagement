<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrolls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enroll_semister_id')->unsigned();
            $table->integer('student_id')->unsigned();
            $table->integer('semister');
            $table->double('previous_gpa');
            $table->double('cgpa');
            $table->tinyInteger('account_status');
            $table->integer('account_officer_id')->nullable();
            $table->tinyInteger('hall_status');
            $table->tinyInteger('student_status');
            $table->tinyInteger('has_retake');
            $table->timestamps();
            $table->foreign('enroll_semister_id')
                ->references('id')->on('enroll_semisters')
                ->onDelete('cascade');
            $table->foreign('student_id')
                ->references('id')->on('students')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enrolls');
    }
}
