<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHallMoneysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hall_moneys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enroll_id')->unsigned();
            $table->integer('amount');
            $table->string('receipt');
            $table->integer('semister');
            $table->timestamps();
            $table->foreign('enroll_id')
                ->references('id')->on('enrolls')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hall_moneys');
    }
}
