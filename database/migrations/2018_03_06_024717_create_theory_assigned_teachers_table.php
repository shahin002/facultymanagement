<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTheoryAssignedTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theory_assigned_teachers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('theoritical_teacher_id')->unsigned();
            $table->integer('teacher_id')->unsigned();
            $table->timestamps();
            $table->foreign('theoritical_teacher_id')
                ->references('id')->on('theoritical_teachers')
                ->onDelete('cascade');
            $table->foreign('teacher_id')
                ->references('id')->on('teachers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('theory_assigned_teachers');
    }
}
