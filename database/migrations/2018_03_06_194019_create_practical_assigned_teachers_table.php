<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePracticalAssignedTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('practical_assigned_teachers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('practical_teacher_id')->unsigned();
            $table->integer('teacher_id')->unsigned();
            $table->timestamps();
            $table->foreign('practical_teacher_id')
                ->references('id')->on('practical_teachers')
                ->onDelete('cascade');
            $table->foreign('teacher_id')
                ->references('id')->on('teachers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('practical_assigned_teachers');
    }
}
