var CurrentSemester = function () {

    var changeEnrollmentStatus = function () {
        jQuery('.change-enrollment-status').on('click', function (e) {
            var href = $(this).attr('href');
            var title = $(this).data('title');
            e.preventDefault();
            swal({
                title: 'Are you sure?',
                text: 'To Enrollment ' + title,
                showCancelButton: true,
                confirmButtonText: 'Yes, ' + title + '!',
                closeOnConfirm: false,
                html: false
            }, function () {
                window.location.href = href;
            });

        });
    };
    var changeAdmitCardDownloadStatus = function () {
        jQuery('.change-admit-card-download-status').on('click', function (e) {
            var href = $(this).attr('href');
            var title = $(this).data('title');
            e.preventDefault();
            swal({
                title: 'Are you sure?',
                text: 'To Admit Card Download ' + title,
                showCancelButton: true,
                confirmButtonText: 'Yes, ' + title + '!',
                closeOnConfirm: false,
                html: false
            }, function () {
                window.location.href = href;
            });

        });
    };

    return {
        init: function () {
            // Init Register Form Validation
            changeEnrollmentStatus();
            changeAdmitCardDownloadStatus();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    CurrentSemester.init();
});