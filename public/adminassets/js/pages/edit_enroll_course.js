jQuery(function () {
    var coursess = null;
    getCourseList();

    function getCourseList() {
        $.ajax({
            type: 'get',
            url: getCoursekUrl,
            async: false,
            success: function (courses) {
                coursess = courses;
            }
        });
    }
    $("#editCourse").click(function () {
        $("#courseTable").hide();
        $("#courseSelect").css('visibility', 'visible');
        $("#semister").select2("val", "");

    });
    jQuery('.js-select2').select2();
    $('#semister').change(function () {
        filter();
    });
    function filter() {
        var option = "";
        var courses = coursess;
        var semister = $('#semister').val();
        for (var i = 0; i < courses.length; i += 1) {
            if (semister >= courses[i].semister) {
                option += "<option value='" + courses[i].id + "'>" + courses[i].name + " - Semister " + courses[i].semister + "</option>"
            }
        }
        $('#course').html(option);

    }
});

