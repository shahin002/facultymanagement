/*
 *  Document   : base_pages_register.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Register Page
 */

var SemesterCreate = function () {
    // Init Register Form Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation
    var semesterCreateForm = function () {
        jQuery('.add-semester-form').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function (error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },
            highlight: function (e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            success: function (e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
        });
    };

    var select2Init=function () {
        jQuery('.js-select2').select2();
        jQuery('.js-select2').on('change', function () {
            jQuery(this).valid();
        });
    };


    return {
        init: function () {
            // Init Register Form Validation
            semesterCreateForm();
            select2Init();
        }
    };
}();

// Initialize when page loads
jQuery(function () {
    SemesterCreate.init();
});