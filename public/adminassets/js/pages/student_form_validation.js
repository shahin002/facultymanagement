jQuery(function () {
    'use strict';
    jQuery('.js-select2').select2();
    jQuery('.js-select2').on('change', function () {
        jQuery(this).valid();
    });
    $('#studentForm').validate({
        errorElement: 'span',
        errorClass: 'text-danger',
        rules: {
            email: {
                required: true,
                email: true,
                remote: {
                    url: emailUrl,
                    type: "post",
                    data: {
                        email: function () {
                            return $("#email").val();
                        },
                        _token: function () {
                            return token;
                        },
                        currentEmail: function () {
                            return currentEmail;
                        }
                    }

                }
            },
            roll_no: {
                required: true,
                number: true,
                remote: {
                    url: rollUrl,
                    type: "post",
                    data: {
                        roll: function () {
                            return $("#roll").val();
                        },
                        _token: function () {
                            return token;
                        },
                        currentRoll: function () {
                            return currentRoll;
                        }
                    }

                }
            },
            reg_no: {
                required: true,
                number: true,
                remote: {
                    url: regUrl,
                    type: "post",
                    data: {
                        reg: function () {
                            return $("#reg").val();
                        },
                        _token: function () {
                            return token;
                        },
                        currentReg: function () {
                            return currentReg;
                        }
                    }

                }
            }
        },
        messages: {
            email: {
                required: "Email is required",
                email: "Please enter a valid email address",
                remote: "This email is already registered"
            },
            roll_no: {
                required: "Roll is required",
                number: "Please enter a valid Roll",
                remote: "This Roll is already registered"
            },
            reg_no: {
                required: "Reg No is required",
                number: "Please enter a valid Roll",
                remote: "This Reg No is already registered"
            }
        }
    });
});