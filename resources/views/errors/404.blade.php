@extends('errors.layout')
@section('title','Page Not Found')
@section('maincontent')
    <!-- Error Titles -->
    <h1 class="font-s128 font-w300 text-city animated flipInX">404</h1>
    <h2 class="h3 font-w300 push-50 animated fadeInUp">Sorry, the page you are looking for could not be found.</h2>
    <!-- END Error Titles -->
@endsection