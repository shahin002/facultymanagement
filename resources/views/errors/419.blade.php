@extends('errors.layout')
@section('title','Page Expired')
@section('maincontent')
    <!-- Error Titles -->
    <h1 class="font-s128 font-w300 text-city animated flipInX">419</h1>
    <h2 class="h3 font-w300 push-50 animated fadeInUp">The page has expired due to inactivity.<br/><br/>
        Please refresh and try again.</h2>
    <!-- END Error Titles -->
@endsection