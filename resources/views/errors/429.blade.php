@extends('errors.layout')
@section('title','Error')
@section('maincontent')
    <!-- Error Titles -->
    <h1 class="font-s128 font-w300 text-city animated flipInX">429</h1>
    <h2 class="h3 font-w300 push-50 animated fadeInUp">Too many requests.</h2>
    <!-- END Error Titles -->
@endsection