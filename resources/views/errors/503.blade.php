@extends('errors.layout')
@section('title','Service Unavailable')
@section('maincontent')
    <!-- Error Titles -->
    <h1 class="font-s128 font-w300 text-city animated flipInX">503</h1>
    <h2 class="h3 font-w300 push-50 animated fadeInUp">Be right back.</h2>
    <!-- END Error Titles -->
@endsection