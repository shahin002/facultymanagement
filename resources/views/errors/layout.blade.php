<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-focus"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <title>@yield('title') - Faculty of Agriculture</title>


    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="{{asset('public/adminassets/img/favicons/favicon.png')}}">

    <!-- Stylesheets -->
    <!-- Web fonts -->
    <link rel="stylesheet"
          href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

    <!-- Bootstrap and OneUI CSS framework -->
    <link rel="stylesheet" href="{{asset('public/adminassets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" id="css-main" href="{{asset('public/adminassets/css/oneui.css')}}">
</head>
<body>
<!-- Error Content -->
<div class="content bg-white text-center pulldown overflow-hidden">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            @yield('maincontent')
        </div>
    </div>
</div>
<!-- END Error Content -->

<!-- Error Footer -->
<div class="content pulldown text-muted text-center">
    Would you like to let us know about it?<br>
    <a class="link-effect" href="{{url('/')}}">Home</a> or <a class="link-effect" href="{{url()->previous()}}">Go Back</a>
</div>
<!-- END Error Footer -->

<!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
<script src="{{asset('public/adminassets/js/core/jquery.min.js')}}"></script>
<script src="{{asset('public/adminassets/js/core/bootstrap.min.js')}}"></script>
<script src="{{asset('public/adminassets/js/core/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('public/adminassets/js/core/jquery.scrollLock.min.js')}}"></script>
<script src="{{asset('public/adminassets/js/core/jquery.appear.min.js')}}"></script>
<script src="{{asset('public/adminassets/js/core/jquery.countTo.min.js')}}"></script>
<script src="{{asset('public/adminassets/js/core/jquery.placeholder.min.js')}}"></script>
<script src="{{asset('public/adminassets/js/core/js.cookie.min.js')}}"></script>
<script src="{{asset('public/adminassets/js/app.js')}}"></script>
</body>
</html>