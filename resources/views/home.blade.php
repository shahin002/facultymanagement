<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{asset('public/frontend/favicon.png')}}" type="image/png" sizes="16x16">

    <title>Faculty Of Agriculture</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('public/frontend/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{asset('public/frontend/css/small-business.css')}}" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">Faculty Of Agriculture</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item ">
                    <a class="nav-link" href="{{route('student.home')}}">Student
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <div style="color:aliceblue; margin-top:7px; ">
                    |
                </div>

                <li class="nav-item">
                    <a class="nav-link" href="{{route('provost.home')}}">Hall Provost</a>
                </li>
                <div style="color:aliceblue; margin-top:7px; ">
                    |
                </div>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('account.home')}}">Account Officer</a>
                </li>
                <div style="color:aliceblue; margin-top:7px;">
                    |
                </div>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('teacher.home')}}">Teacher</a>
                </li>
                <div style="color:aliceblue; margin-top:7px;">
                    |
                </div>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('chairman.home')}}">Dept Chairman</a>
                </li>
                <div style="color:aliceblue; margin-top:7px;">
                    |
                </div>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('admin.home')}}">Dean Officer</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">

    <div class="row my-4">
        <div class="col-lg-8">
            <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img style="height:280px; width:900px" class="d-block img-fluid" src="{{asset('public/frontend/03.jpg')}}"
                             alt="Second slide">
                        <div class="carousel-caption">
                            <p>Faculty of Agriculture</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img style="height:280px; width:900px" class="d-block img-fluid" src="{{asset('public/frontend/0.jpg')}}" alt="Second slide">
                        <div class="carousel-caption">
                            <p>Faculty of Agriculture</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img style="height:280px; width:900px" class="d-block img-fluid" src="{{asset('public/frontend/02.jpg')}}"
                             alt="Second slide">
                        <div class="carousel-caption">
                            <p>Faculty of Agriculture</p>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="col-lg-4">
            <div style="margin-top:20px;">
                <img src="{{asset('public/frontend/cube.jpg')}}" alt="cube picture" style="width:100px; height:100px;">
            </div>

            <div style="float:right">
                <p>Welcome you to the Faculty Of Agriculture. It is one of the fast growing faculty of Patuakhali
                    Science and Technology University. Faculty of Agriculture is the mother faculty of Patuakhali
                    Science and Technology University.</p>
            </div>


        </div>
    </div>

    <div class="card text-white bg-secondary my-4 text-center">
        <div class="card-body">
            <p class="text-white m-0">This application is only for the Faculty Of Agriculture to dynamic the faculty
                activity.</p>
        </div>
    </div>
</div>


<!-- Footer -->
<footer class="py-3 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Faculty Of Agriculture 2017</p>
    </div>
    <!-- /.container -->
</footer>

<!-- Bootstrap core JavaScript -->
<script src="{{asset('public/frontend/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('public/frontend/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

</body>

</html>
