<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('public/pdf/enrollment.css') }}" media="all"/>
</head>
<body>
<div class="extra-div"></div>
<header class="padding-10 clearfix">
    <div class="col-md-12">
        <div class="col-md-3 bg-red">
            <img class="logo" src="{{asset('public/logo.png')}}" alt="#">
        </div>
        <div class="header-content text-center col-md-6">
            <h1>Faculty of Agriculture</h1>
            <p class="lh-0">Patuakhali Science and Technology University</p>
            <p class="lh-0">Dumki, Patuakhali-8602, Bangladesh</p>
            <p class="lh-0">Telephone : 880-4427-56003</p>
            <p class="lh-0">E-mail : deanagriculture@pstu.ac.bd</p>
        </div>
        <div class="bg-red col-md-3">
            <img class="avatar"
                 src="{{asset('public/ppphoto.PNG')}}"
                 alt="#">
        </div>
    </div>


</header>

<section class="sec-1 col-md-12 padding-10">
    <h1>Enrollment Form</h1>
    <p class="sub-title">7 Semester (Level-3, Semseter-1), Januaary-June ;2018,</p>
    <p style="line-height: 0px"><strong>Name of the student(Capital Letter) :</strong> MD. SHAHIN HOWLADER</p>
    <p><strong>Registration No:</strong> 03627 , &nbsp;&nbsp;&nbsp;&nbsp; <strong>ID No (Class Roll):</strong>
        1202002 ,&nbsp;&nbsp;&nbsp;&nbsp; <strong>Session :</strong> 2012-2013</p>
    <p><strong>Status of the student :</strong> Regular. &nbsp;&nbsp;&nbsp;&nbsp; <strong>Information about previous
            semester: GPA :</strong> 3.27, <strong>CGPA :</strong> 3.15</p>
    <p><strong>Name of the Hall :</strong> M. K. ALI HALL</p>
</section>

<section class="sec-2 col-md-12 padding-10">
    <p>Name of the courses (Must be mentioned) :</p>
    <div class="sec-2-table">
        <div class="col-md-6">
            <table>
                <tr>
                    <th colspan="3">Theoretical</th>
                </tr>
                <tr>
                    <td>Course Code</td>
                    <td>Course Title</td>
                    <td>Cr. Hr.</td>
                </tr>
                <tr>
                    <td>CCE-311</td>
                    <td>This is Course Title</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>CCE-311</td>
                    <td>This is Course Title</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>CCE-311</td>
                    <td>This is Course Title</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>CCE-311</td>
                    <td>This is Course Title</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>CCE-311</td>
                    <td>This is Course Title</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: right; padding-right: 15px;" colspan="2">Total Cr.Hr. (Theoretical)=</td>
                    <td>10</td>
                </tr>
            </table>
        </div>
        <div class="col-md-6">
            <table>
                <tr>
                    <th colspan="3">Theoretical</th>
                </tr>
                <tr>
                    <td>Course Code</td>
                    <td>Course Title</td>
                    <td>Cr. Hr.</td>
                </tr>
                <tr>
                    <td>CCE-311</td>
                    <td>This is Course Title</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>CCE-311</td>
                    <td>This is Course Title</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>CCE-311</td>
                    <td>This is Course Title</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>CCE-311</td>
                    <td>This is Course Title</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>CCE-311</td>
                    <td>This is Course Title</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: right; padding-right: 15px;" colspan="2">Total Cr.Hr. (Theoretical)=</td>
                    <td>10</td>
                </tr>
            </table>
        </div>
    </div>
</section>


<section class="sec-3 col-md-12 padding-10">
    <p>Name of the courses for F/Course Repeat intended to be enrolled :</p>
    <div class="sec-3-table">
        <div class="col-md-6">
            <table>
                <tr>
                    <th colspan="3">Theoretical</th>
                </tr>
                <tr>
                    <td>Course Code</td>
                    <td>Course Title</td>
                    <td>Cr. Hr.</td>
                </tr>
                <tr>
                    <td>CCE-311</td>
                    <td>This is Course Title</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>CCE-311</td>
                    <td>This is Course Title</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>CCE-311</td>
                    <td>This is Course Title</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>CCE-311</td>
                    <td>This is Course Title</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>CCE-311</td>
                    <td>This is Course Title</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: right; padding-right: 15px;" colspan="2">Total Cr.Hr. (Theoretical)=</td>
                    <td>10</td>
                </tr>
            </table>
        </div>
        <div class="col-md-6">
            <table>
                <tr>
                    <th colspan="3">Theoretical</th>
                </tr>
                <tr>
                    <td>Course Code</td>
                    <td>Course Title</td>
                    <td>Cr. Hr.</td>
                </tr>
                <tr>
                    <td>CCE-311</td>
                    <td>This is Course Title</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>CCE-311</td>
                    <td>This is Course Title</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>CCE-311</td>
                    <td>This is Course Title</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>CCE-311</td>
                    <td>This is Course Title</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>CCE-311</td>
                    <td>This is Course Title</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: right; padding-right: 15px;" colspan="2">Total Cr.Hr. (Theoretical)=</td>
                    <td>10</td>
                </tr>
            </table>
        </div>
    </div>
</section>

<section class="signature col-md-12 padding-10 lh-0">
    <div class="col-md-12">
        <img src="{{asset('public/signature.PNG')}}" alt="">
        <p>Signature of the student:</p>
        <p>Date : 2018-03-20</p>
        <p>Mobile No : 01748668096</p>
    </div>


    <div class="bottom-signature col-md-12 text-center">
        <div class="col-md-4">
            <img src="{{asset('public/signature.PNG')}}" alt="">
            <p>Accounts Officer</p>
        </div>
        <div class="col-md-4">
            <img src="{{asset('public/signature.PNG')}}" alt="">
            <p>Hall Provost</p>
        </div>
        <div class="col-md-4">
            <img src="{{asset('public/signature.PNG')}}" alt="">
            <p>Dean</p>
            <p>Faculty of Agriculture</p>
        </div>
    </div>
</section>
</body>
</html>