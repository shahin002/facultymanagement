<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('public/pdf/enrollment.css') }}" media="all"/>
</head>
<body>
<div class="extra-div"></div>
<header class="padding-10 clearfix">
    <div class="col-md-12">
        <div class="col-md-3 bg-red">
            <img class="logo" src="{{asset('public/pdf/logo.png')}}" alt="#">
        </div>
        <div class="header-content text-center col-md-6">
            <h1>Faculty of Agriculture</h1>
            <p class="lh-0">Patuakhali Science and Technology University</p>
            <p class="lh-0">Dumki, Patuakhali-8602, Bangladesh</p>
            <p class="lh-0">Telephone : 880-4427-56003</p>
            <p class="lh-0">E-mail : deanagriculture@pstu.ac.bd</p>
        </div>
        <div class="bg-red col-md-3">
            <img class="avatar"
                 src="{{asset($enroll->student->image)}}"
                 alt="#">
        </div>
    </div>


</header>

<section class="sec-1 col-md-12 padding-10">
    <h1>Enrollment Form</h1>
    <p class="sub-title">{{$enroll->semister}} Semester ({{$semisterName}}
        ), {{$enroll->enrollSemister->month==0?'January-June':'July-December'}} ;{{$enroll->enrollSemister->year}}</p>
    <p style="line-height: 0px"><strong>Name of the student(Capital Letter)
            :</strong> {{ucwords($enroll->student->name)}}</p>
    <p><strong>Registration No:</strong> {{$enroll->student->reg_no}} , &nbsp;&nbsp;&nbsp;&nbsp; <strong>ID No (Class
            Roll):</strong>
        {{$enroll->student->roll_no}} ,&nbsp;&nbsp;&nbsp;&nbsp; <strong>Session :</strong> {{$enroll->student->session}}
    </p>
    <p><strong>Status of the student :</strong> {{$enroll->student_status==1?'Regular':'Irregular'}}. &nbsp;&nbsp;&nbsp;&nbsp;
        <strong>Information about previous
            semester: GPA :</strong> {{$enroll->previous_gpa}}, <strong>CGPA :</strong> {{$enroll->cgpa}}</p>
    <p><strong>Name of the Hall :</strong> {{$enroll->student->hall->name}}</p>
</section>

<section class="sec-2 col-md-12 padding-10" style="padding-top: 0px">
    <p>Name of the courses (Must be mentioned) :</p>
    <div class="sec-2-table">
        <div class="col-md-6">
            <table>
                <tr>
                    <th colspan="3">Theoretical</th>
                </tr>
                <tr>
                    <th>Course Code</th>
                    <th>Course Title</th>
                    <th width="10">Cr. Hr.</th>
                </tr>
                @php ($credit = 0 )
                @php ($count = 0 )
                @foreach($enroll->courses as $course)
                    @if($enroll->semister != $course->semister || $course->type=='Practical')
                        @continue
                    @endif
                    <tr>

                        <td>{{$course->code}}</td>
                        <td>{{$course->name}}</td>
                        <td width="">{{$course->credit}}</td>
                    </tr>
                    @php($credit += $course->credit)
                    @php($count++)
                @endforeach
                @for($i=0;$i<(8-$count);$i++)
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                @endfor
                <tr>
                    <td style="text-align: right; padding-right: 15px;" colspan="2">Total Cr.Hr. (Theoretical)=</td>
                    <td>{{$credit}}</td>
                </tr>
            </table>
        </div>
        <div class="col-md-6">
            <table>
                <tr>
                    <th colspan="3">Practical</th>
                </tr>
                <tr>
                    <th>Course Code</th>
                    <th>Course Title</th>
                    <th width="10">Cr. Hr.</th>
                </tr>
                @php ($credit = 0 )
                @php ($count = 0 )
                @foreach($enroll->courses as $course)
                    @if($enroll->semister != $course->semister || $course->type=='Theoretical')
                        @continue
                    @endif
                    <tr>
                        <td>{{$course->code}}</td>
                        <td>{{$course->name}}</td>
                        <td>{{$course->credit}}</td>
                    </tr>
                    @php($credit += $course->credit)
                    @php($count++)
                @endforeach
                @for($i=0;$i<(8-$count);$i++)
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                @endfor
                <tr>
                    <td style="text-align: right; padding-right: 15px;" colspan="2">Total Cr.Hr. (Practical)=</td>
                    <td>{{$credit}}</td>
                </tr>
            </table>
        </div>
    </div>
</section>


<section class="sec-3 col-md-12 padding-10">
    <p>Name of the courses for F/Course Repeat intended to be enrolled :</p>
    <div class="sec-3-table">
        <div class="col-md-6">
            <table>
                <tr>
                    <th colspan="3">Theoretical</th>
                </tr>
                <tr>
                    <th>Course Code</th>
                    <th>Course Title</th>
                    <th width="10">Cr. Hr.</th>
                </tr>
                @php ($credit = 0 )
                @php ($count = 0 )
                @foreach($enroll->courses as $course)
                    @if($enroll->semister == $course->semister || $course->type=='Practical')
                        @continue
                    @endif
                    <tr>

                        <td>{{$course->code}}</td>
                        <td>{{$course->name}}</td>
                        <td width="">{{$course->credit}}</td>
                    </tr>
                    @php($credit += $course->credit)
                    @php($count++)
                @endforeach
                @for($i=0;$i<(8-$count);$i++)
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                @endfor
                <tr>
                    <td style="text-align: right; padding-right: 15px;" colspan="2">Total Cr.Hr. (Theoretical)=</td>
                    <td>{{$credit}}</td>
                </tr>
            </table>
        </div>
        <div class="col-md-6">
            <table>
                <tr>
                    <th colspan="3">Practical</th>
                </tr>
                <tr>
                    <th>Course Code</th>
                    <th>Course Title</th>
                    <th width="10">Cr. Hr.</th>
                </tr>
                @php ($credit = 0 )
                @php ($count = 0 )
                @foreach($enroll->courses as $course)
                    @if($enroll->semister == $course->semister || $course->type=='Theoretical')
                        @continue
                    @endif
                    <tr>
                        <td>{{$course->code}}</td>
                        <td>{{$course->name}}</td>
                        <td>{{$course->credit}}</td>
                    </tr>
                    @php($credit += $course->credit)
                    @php($count++)
                @endforeach
                @for($i=0;$i<(8-$count);$i++)
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                @endfor
                <tr>
                    <td style="text-align: right; padding-right: 15px;" colspan="2">Total Cr.Hr. (Practical)=</td>
                    <td>{{$credit}}</td>
                </tr>
            </table>
        </div>
    </div>
</section>

<section class="signature col-md-12 padding-10 lh-0">
    <div class="col-md-12">
        <img src="{{asset($enroll->student->signature)}}" height="50px" alt="">
        <p>Signature of the student:</p>
        <p>Date : {{date('Y-m-d')}}</p>
        <p>Mobile No : {{$enroll->student->phone_number}}</p>
    </div>


    <div class="bottom-signature col-md-12 text-center">
        <div class="col-md-4">
            @if($enroll->account_status == 1 )
                <img src="{{asset($enroll->accountOfficer->signature)}}">
            @endif
            <p>Accounts Officer</p>
        </div>
        <div class="col-md-4">
            @if($enroll->hall_status == 1 )
                <img src="{{asset($enroll->student->hall->hallOfficer->signature)}}">
            @endif
            <p>Hall Provost</p>
        </div>
        <div class="col-md-4">
            @if($enroll->account_status == 1 && $enroll->hall_status==1)
                <img src="{{asset($adminSignature)}}">
            @endif
            <p>Dean</p>
            <p>Faculty of Agriculture</p>
        </div>
    </div>
</section>
</body>
</html>