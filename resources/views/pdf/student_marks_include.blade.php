<?php
$sum = 0;
$attendence = @$enroll->marks->assignment ? $enroll->marks->assignment : 0;
$mid = @$enroll->marks->assignment ? $enroll->marks->mid : 0;
$final = @$enroll->marks->assignment ? $enroll->marks->final : 0;
$sum = $attendence + $mid + $final;
$grade = '';
$point = 0.00;
if ($sum > 79) {
    $grade = 'A+';
    $point = 4.00;
} elseif ($sum > 74) {
    $grade = 'A';
    $point = 3.75;
} elseif ($sum > 69) {
    $grade = 'A-';
    $point = 3.50;
} elseif ($sum > 64) {
    $grade = 'B+';
    $point = 3.25;
} elseif ($sum > 59) {
    $grade = 'B';
    $point = 3.00;
} elseif ($sum > 54) {
    $grade = 'B-';
    $point = 2.75;
} elseif ($sum > 49) {
    $grade = 'C+';
    $point = 2.50;
} elseif ($sum > 44) {
    $grade = 'C';
    $point = 2.25;
} elseif ($sum > 39) {
    $grade = 'D';
    $point = 2.00;
} else {
    $grade = 'F';
    $point = 0.00;
}
?>
<tr>
    <td width="72" nowrap="" style="border:1px solid black;" align="center">
        {{$enroll->student->roll_no}}
    </td>
    <td width="189" nowrap="" style="border:1px solid black;" align="center">
        {{$enroll->student->name}}
    </td>
    <td width="60" style="border:1px solid black;" align="center">
        {{$enroll->student->roll_no}}
    </td>
    <td width="54" valign="bottom" style="border:1px solid black;" align="center">
        {{$attendence}}</td>
    <td width="54" valign="top" style="border:1px solid black;" align="center">
        {{$mid}}
    </td>
    <td width="51" valign="bottom" style="border:1px solid black;" align="center">
        {{$final}}
    </td>
    <td width="60" valign="bottom" style="border:1px solid black;" align="center">
        {{$sum}}
    </td>
    <td width="54" valign="bottom" style="border:1px solid black;" align="center">
        {{$sum}}
    </td>
    <td width="54" valign="bottom" style="border:1px solid black;" align="center">
        {{$grade}}
    </td>
    <td width="68" valign="bottom" style="border:1px solid black;" align="center">
        {{$point}}
    </td>
</tr>