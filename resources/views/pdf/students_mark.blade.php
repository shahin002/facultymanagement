<style>
    @page {
        margin: 0 auto;
    }
</style>
<div style="height: 30px;display: block">&nbsp;</div>
<div style="width: 90%;margin: 0 auto">
    <div style="text-align: center;line-height: 1px;">
        <p><span style="font-size:17.0pt">Patuakhali Science and Technology University</span></p>

        <p>Dumki, Patuakhlia-8602</p>
        <br>

        <p style="text-align:center"><u><span style="font-size:17.0pt">Grade Sheet </span></u></p>
    </div>


    <table border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; width: 100%">
        <tr>
            <td style="width: 45%">
                <table cellspacing="0" cellpadding="0" style="border-collapse:collapse; width: 100%;">

                    <tr>
                        <th colspan="3" align="center" style="border: 1px solid black">
                            Grading System
                        </th>
                    </tr>

                    <tr>
                        <th style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">Score (%)</span>
                        </th>
                        <th align="center" style="border:1px solid black;">
                            <span style="font-size:10.0pt">Letter Grade</span>
                        </th>
                        <th style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">Grade Point</span>
                        </th>
                    </tr>
                    <tr>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">80%and Above</span>
                        </td>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">A+</span>
                        </td>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">4.00</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">75% Less than 80%</span>
                        </td>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">A</span>
                        </td>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">3.75</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">70% Less than 75%</span>
                        </td>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">A-</span>
                        </td>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">3.50</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">65% Less than 70%</span>
                        </td>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">B+</span>
                        </td>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">3.25</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">60% Less than 65%</span>
                        </td>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">B</span>
                        </td>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">3.00</span>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 10%;"></td>
            <td style="width: 45%">
                <table border="1" cellspacing="0" cellpadding="0" style="border-collapse:collapse; width: 100%;">
                    <tr>
                        <th colspan="3" align="center" style="border:1px solid black;">
                            Grading System
                        </th>
                    </tr>
                    <tr>
                        <th style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">Score (%)</span>
                        </th>
                        <th align="center" style="border:1px solid black;">
                            <span style="font-size:10.0pt">Letter Grade</span>
                        </th>
                        <th style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">Grade Point</span>
                        </th>
                    </tr>
                    <tr>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">55% Less than 60%</span>
                        </td>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">B-</span>
                        </td>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">2.75</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">50% Less than 55%</span>
                        </td>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">C+</span>
                        </td>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">2.50</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">45% Less than 50%</span>
                        </td>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">C</span>
                        </td>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">2.25</span>
                        </td>
                    </tr>

                    <tr>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">40% Less than 45%</span>
                        </td>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">D</span>
                        </td>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">2.00</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">Less than 40%</span>
                        </td>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">F</span>
                        </td>
                        <td style="border:1px solid black;" align="center">
                            <span style="font-size:10.0pt">0.00</span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <div style="text-align: center; line-height: .5px">
        <p><b>Name of Examination:</b> Semester {{$course->semister}}({{$semesterName}}) final examination of B. Sc. Ag.
            (Hons), </p>

        <p>{{$semester->month==0?'January-June':'July-December'}}, {{$semester->year}}</p>
    </div>


    <p><span style="width: 50%;display: block"><b>Department:</b> {{$course->department->name}}</span>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span style="width: 23%;display: block;float: left"><b>Course Code:</b> {{$course->code}}</span>&nbsp;&nbsp;&nbsp;
    </p>

    <p><span
                style="width: 30%;display: block;float: left"><b>Credit Hour:</b> {{$course->credit}}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;<span
                style="width: 55%;display: block;float: left"><b>Course title:</b> {{$course->name}}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span

                style="width: 55%;display: block;float: left"><b>Session:</b> {{$courseEnrolls->last()->student->session}}</span>
    </p>
    <br>
    <div>

        <table border="1" cellspacing="0" cellpadding="0" width="715"
               style="width:100%;border-collapse:collapse;border:none;table-layout: fixed;">
            <thead repeat_header="1">
            <tr>
                <th  nowrap="" style="border:1px solid black;" align="center">
                    ID No.
                </th>
                <th  nowrap="" style="border:1px solid black;" align="center">
                    Name&nbsp; of&nbsp; the&nbsp;
                    Student
                </th>
                <th  style="border:1px solid black;" align="center">
                    Reg. No.
                </th>
                <th  colspan="3" style="border:1px solid black;" align="center">
                    <table style="border-collapse:collapse;width: 100%">
                        <tr>
                            <th colspan="3">Marks Obtained</th>
                        </tr>

                        <tr>
                            <th width="34%" style="border:1px solid black;" align="center">
                                Attendance
                            </th>
                            <th width="33%" valign="top" style="border:1px solid black;" align="center">
                                Class test
                            </th>
                            <th width="33%" valign="top" style="border:1px solid black;">
                                Final
                            </th>
                        </tr>
                    </table>
                </th>
                <th  valign="top" style="border:1px solid black;" align="center">
                    Total Marks
                </th>
                <th  valign="top" style="border:1px solid black;" align="center">
                    Score (%)
                </th>
                <th  valign="top" style="border:1px solid black;" align="center">
                    Letter Grade
                </th>
                <th  valign="top" style="border:1px solid black;" align="center">
                    Grade Point
                </th>
            </tr>

            </thead>
            <tbody>

            @php($fi=array())
            @php($repeat=array())
            @foreach($courseEnrolls as $enroll)
                @if($enroll->semister !=$course->semister)
                    @php($fi[]=$enroll)
                    @continue
                @elseif($enroll->student_status !=1)
                    @php($repeat[]=$enroll)
                    @continue
                @endif
                @include('pdf.student_marks_include')
            @endforeach
            @foreach($repeat as $enroll)

                @include('pdf.student_marks_include')
            @endforeach
            @foreach($fi as $enroll)
                @include('pdf.student_marks_include')
            @endforeach
            </tbody>

        </table>

    </div>


    <div style="font-size: 15px;  text-align: justify;">
        <table>
            <tr>
                <td align="center" width="35%">
                    @foreach($courseTeachers as $signature)
                    <img align="right" height="50px" width="280px" src="{{asset($signature)}}"/><br>
                    @endforeach
                    <br>
                    <span style="float: right;">Course Teacher
                    </span>
                </td>
                <td width="20%" >
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
                <td align="left" width="45%">
                    <img align="right" style="max-height: 50px; max-width: 280px" src="{{asset($course->department->chairman->signature)}}"/>
                    <br>
                    <br>
                    <span style="float: right;">{{$course->department->chairman->name}}<br>
Chairman<br>
Dept of {{$course->department->name}}<br>
Patuakhali Science and Technology University<br>
Dumki, Patuakhali-8602
                    </span>
                </td>
            </tr>
        </table>


    </div>

</div>