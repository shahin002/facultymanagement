@extends('users.account.master')

@section('title')
    Login Information
@endsection

@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Update Login Information</strong>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{route('update.account')}}" method="post">
                            {{csrf_field()}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" id="email" name="email" placeholder="Enter a New Email"
                                           value="{{auth('account')->user()->email}}" class="form-control" required
                                           readonly>
                                    @if ($errors->has('email'))
                                        <span class="help-block text-danger">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="password" id="password" name="password" placeholder="" value=""
                                           class="form-control" required autofocus>
                                    @if ($errors->has('password'))
                                        <span class="help-block text-danger">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" id="confirmPassword" name="password_confirmation"
                                           placeholder="" value="" class="form-control" required autofocus>
                                </div>
                                <input type="submit" class="btn btn-lg btn-info" value="Update">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection