<!-- Sidebar -->
<nav id="sidebar">
    <!-- Sidebar Scroll Container -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
        <div class="sidebar-content">
            <!-- Side Header -->
            <div class="side-header side-content bg-white-op">
                <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button"
                        data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times"></i>
                </button>
                <!-- Themes functionality initialized in App() -> uiHandleTheme() -->
                <a class="h5 text-white" href="{{route('account.home')}}">
                    <span class="h5 text-primary font-w600">Account</span> <span
                            class="h5 font-w600 sidebar-mini-hide">Officer</span>
                </a>
            </div>
            <!-- END Side Header -->

            <!-- Side Content -->
            <div class="side-content">
                <ul class="nav-main">
                    <li>
                        <a href="{{route('account.home')}}"><i class="si si-speedometer"></i><span
                                    class="sidebar-mini-hide">Dashboard</span></a>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-rocket"></i><span
                                    class="sidebar-mini-hide">Manage Enrolls</span></a>
                        <ul>
                            @if(date('n')<7)
                                <li><a href="{{route('account.manage.enroll',1)}}">Semester 1</a></li>
                                <li><a href="{{route('account.manage.enroll',3)}}">Semester 3</a></li>
                                <li><a href="{{route('account.manage.enroll',5)}}">Semester 5</a></li>
                                <li><a href="{{route('account.manage.enroll',7)}}">Semester 7</a></li>
                            @else
                                <li><a href="{{route('account.manage.enroll',2)}}">Semester 2</a></li>
                                <li><a href="{{route('account.manage.enroll',4)}}">Semester 4</a></li>
                                <li><a href="{{route('account.manage.enroll',6)}}">Semester 6</a></li>
                                <li><a href="{{route('account.manage.enroll',8)}}">Semester 8</a></li>
                            @endif
                        </ul>
                    </li>

                </ul>
            </div>
            <!-- END Side Content -->
        </div>
        <!-- Sidebar Content -->
    </div>
    <!-- END Sidebar Scroll Container -->
</nav>
<!-- END Sidebar -->