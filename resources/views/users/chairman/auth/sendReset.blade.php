<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-focus"> <!--<![endif]-->
<head>
    @section('title')
        Dept Chairman Reset Password
    @endsection

    @include('users.chairman.includes.headerfiles')
</head>
<body>
<!-- Login Content -->
<div class="content overflow-hidden">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">

            <div class="block block-themed animated fadeIn">
                <div class="block-header bg-primary">
                    <ul class="block-options">
                        <li>
                            <a href="{{route('chairman.login')}}" data-toggle="tooltip" data-placement="left" title=""
                               data-original-title="Log In"><i class="si si-login"></i></a>
                        </li>
                    </ul>
                    <h3 class="block-title">Dept Chairman Password Reminder</h3>
                </div>
                <div class="block-content block-content-full block-content-narrow">

                    <!-- Reminder Form -->
                    <!-- jQuery Validation (.js-validation-reminder class is initialized in js/pages/base_pages_reminder.js) -->
                    <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
                    <form class="js-validation-login form-horizontal push-30-t push-50"
                          action="{{ url('chairman/email') }}" method="post">
                        @csrf
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-xs-12">
                                <div class="form-material form-material-primary floating">
                                    <input class="form-control" type="email" id="login-email" name="email"
                                           value="{{ old('email') }}"
                                           autocomplete="false" required>
                                    <label for="login-email">Email</label>
                                </div>
                                @if ($errors->has('email'))
                                    <div id="login-email-error"
                                         class="help-block text-right animated fadeInDown">{{ $errors->first('email') }}
                                    </div>
                                @endif


                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12 col-sm-6 col-md-5">
                                <button class="btn btn-block btn-primary" type="submit"><i
                                            class="si si-envelope-open pull-right"></i> Send Mail
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- END Reminder Form -->
                </div>
            </div>

        </div>
    </div>
</div>
<!-- END Login Content -->

<!-- Login Footer -->
<div class="push-10-t text-center animated fadeInUp">
    <small class="text-muted font-w600"><span class="js-year-copy"></span> &copy; Faculty of Agriculture</small>
</div>
<!-- END Login Footer -->

<!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
@include('users.chairman.includes.footerfiles')
@include('users.chairman.includes.notifications')
<!-- Page JS Plugins -->
<script src="{{asset('public/adminassets/js/plugins/jquery-validation/jquery.validate.min.js')}}"></script>

<!-- Page JS Code -->
<script src="{{asset('public/adminassets/js/pages/login_page.js')}}"></script>

</body>
</html>