<style>
    @page {
        margin: 0;
        padding: 0;
    }
</style>
<div style="display: block; height: 20px;"></div>
<table width="90%" align="center">
    <tr>

        <td align="right" width="300">
            <img src="{{asset('public/pdf/logo.png')}}" height="120" style="align:top"/>
        </td>
        <td>
            <b style="font-size: 23px">Faculty of Agriculture</b>
            <div style="font-size: 20px"> Patuakhali Science And Technology University <br> Dumki,Patuakhali-8602<br>Bangladesh
            </div>

        </td>

    </tr>

</table>
<div style="border: 1px solid"></div>
<div style="width: 90%; margin:0 auto; text-align:center;font-size: 16px">
    <b>Nomination of the External Examiner Of Practical Courses: Final Examination</b>
    <p>B.Sc.Ag.(Hons.) Dept of {{auth('dhead')->user()->department->sort_name}}
        ,@if($semister) {{$semister->month==0?'January-June':'July-December'}} {{$semister->year}}@endif</p>

</div>
<table border="1" width="90%" align="center" style="border-collapse:collapse;" cellpadding="5">
    <thead>
    <tr>
        <th>Course Code</th>
        <th>Course Title</th>
        <th>CR. HR.</th>
        <th>Teachers</th>
    </tr>
    </thead>
    <tbody>
    @if($teachers)
        @foreach($teachers as $teacher)
            <tr>
                <td>{{$teacher->course->code}}</td>
                <td>{{$teacher->course->name}}</td>
                <td>{{$teacher->course->credit}} </td>
                <td style="font-size:13px">
                    @php($teacherNames = unserialize($teacher->ext_teachers) )
                    @foreach($teacherNames as $key=>$teacherName)
                        <p>{{$key+1}}. <strong>{{$teacherName['name']}}</strong></p>
                        <p>{{$teacherName['designation']}}</p>
                    @endforeach
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
