@extends('users.chairman.master')

@section('title')
    Dashboard
@endsection
@section('maincontent')
    <!-- Page Header -->
    <div class="content bg-image overflow-hidden"
         style="background-image: url('{{asset('public/adminassets/img/photos/photo3@2x.jpg')}}');">
        <div class="push-50-t push-15">
            <h1 class="h2 text-white animated zoomIn">Dashboard</h1>
            <h2 class="h5 text-white-op animated zoomIn">Welcome {{auth('dhead')->user()->name}}</h2>
        </div>
    </div>
    <!-- END Page Header -->


    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="row">
            <div class="col-lg-12">
                @if(!auth('dhead')->user()->signature)
                    <div class="content">
                        <div class="block">
                            <div class="block-content">
                                <div class="alert alert-danger">
                                    <h3>You haven't Upload your <strong>Signature</strong> yet!<span class="pull-right"><a
                                                    href="{{route('signatureForm.chairman')}}" class="btn btn-primary ">Upload It</a></span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="content bg-gray-lighter hidden-print">
                    <div class="row items-push">
                        <div class="col-sm-12">
                            <h1 class="page-heading text-center font-w600">
                                <strong>Add Question Setter and Script Evaluator (Theoretical)</strong>
                            </h1>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="well">
                        <h4 class="text-center"><strong> Semester
                                :</strong>@if($currentSemister) {{$currentSemister->month==0?'January-June, ':'July-December, '}}{{$currentSemister->year}}@endif
                        </h4></div>
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{route('add.theoriticalTeacher')}}" method="post">
                                {{csrf_field()}}
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Select Course</label>
                                        <select required class="form-control" name="course_id" id="course_id"
                                                data-placeholder="Select Course">
                                            <option></option>
                                            @foreach($courses as $course)
                                                <option value="{{$course->id}}">[{{$course->code}}
                                                    ] {{$course->name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('course_id'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('course_id') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Select Teacher</label>
                                        <select class="form-control" name="teacher_id[]" id="teacher_id"
                                                data-placeholder="Select Teacher"
                                                multiple="multiple" required>
                                            <option></option>
                                            @foreach($teachers as $teacher)
                                                <option value="{{$teacher->id}}">{{$teacher->name}}
                                                    ( {{$teacher->designation}} )
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button class="btn btn-primary" type="submit">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-styles')

@endsection
@section('custom-js')
    <script src="{{asset('public/adminassets/js/plugins/select2/select2.full.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $("#course_id").select2();
            $('#teacher_id').select2();
        });
    </script>
@endsection