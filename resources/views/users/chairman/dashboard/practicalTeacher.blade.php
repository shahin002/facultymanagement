@extends('users.chairman.master')

@section('title')
    Add Question Setter and Script Evaluator Practical
@endsection
@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12 text-center">
                <h1 class="page-heading font-w600">
                    <strong>Add Question Setter and Script Evaluator Practical</strong>
                </h1>
                <h4>@if($currentSemister) {{$currentSemister->month==0?'January-June, ':'July-December, '}}{{$currentSemister->year}}@endif
                </h4>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{route('add.practicalTeacher')}}" method="post">
                            {{csrf_field()}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Select Course</label>
                                    <select required class="form-control" name="course_id" id="course_id"
                                            data-placeholder="Select Course">
                                        <option></option>
                                        @foreach($unsubmittedCourses as $course)
                                            <option value="{{$course->id}}">[{{$course->code}}
                                                ] {{$course->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('course_id'))
                                        <span class="help-block text-danger">
                        <strong>{{ $errors->first('course_id') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <h4>Internal Teacher</h4>
                                <select class="form-control" name="teacher_id[]" id="teacher_id" multiple="multiple"
                                        required data-placeholder="Select Internal Teacher">
                                    @foreach($teachers as $teacher)
                                        <option value="{{$teacher->id}}">{{$teacher->name}} ( {{$teacher->designation}})
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-12">
                                <h4>External Teacher</h4>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Name Of Teacher</label>
                                        <input type="text" id="ext_teacher_one" name="ext_teacher_one" required
                                               placeholder="Example: Mr John Doe" class="form-control"/>
                                        @if ($errors->has('ext_teacher_one'))
                                            <span class="help-block text-danger">
                        <strong>{{ $errors->first('ext_teacher_one') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Teacher's Designation</label>
                                        <input type="text" name="ext_deg_one" required
                                               placeholder="Example: Professor, dept. of CSE" class="form-control"/>

                                        @if ($errors->has('ext_deg_one'))
                                            <span class="help-block text-danger">
                        <strong>{{ $errors->first('ext_deg_one') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Name Of Teacher</label>
                                        <input type="text" id="ext_teacher_two" name="ext_teacher_two" required
                                               placeholder="Example: Mr John Doe" class="form-control"/>

                                        @if ($errors->has('ext_teacher_two'))
                                            <span class="help-block text-danger">
                        <strong>{{ $errors->first('ext_teacher_two') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Teacher's Designation</label>
                                        <input type="text" id="ext_deg_two" name="ext_deg_two" required
                                               placeholder="Example: Professor, dept. of CSE" class="form-control"/>

                                        @if ($errors->has('ext_deg_two'))
                                            <span class="help-block text-danger">
                        <strong>{{ $errors->first('ext_deg_two') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Name Of Teacher</label>
                                        <input type="text" id="ext_teacher_three" name="ext_teacher_three" required
                                               placeholder="Example: Mr John Doe" class="form-control"/>

                                        @if ($errors->has('ext_teacher_three'))
                                            <span class="help-block text-danger">
                        <strong>{{ $errors->first('ext_teacher_three') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Teacher's Designation</label>
                                        <input type="text" id="ext_deg_three" name="ext_deg_three" required
                                               placeholder="Example: Professor, dept. of CSE" class="form-control"/>

                                        @if ($errors->has('ext_deg_three'))
                                            <span class="help-block text-danger">
                        <strong>{{ $errors->first('ext_deg_three') }}</strong>
                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="form-group" style="float:right">
                                    <button class="btn btn-info" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

@section('custom-js')
    <script src="{{asset('public/adminassets/js/plugins/select2/select2.full.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $("#course_id").select2();
            $('#teacher_id').select2();
        });
    </script>
@endsection