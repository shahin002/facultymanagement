@extends('users.chairman.master')

@section('title')
    Semester {{$semester_id}} Theoritical Courses
@endsection
@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12 text-center">
                <h1 class="page-heading font-w600">
                    <strong>Practical Courses of Semester {{$semester_id}}</strong>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Course Name</th>
                                <th>Course Code</th>
                                <th>Credit</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($courses)
                                @foreach($courses as $temp)
                                    @php($course=$temp->course)
                                    <tr>
                                        <td>{{$course->name}}</td>
                                        <td>{{$course->code}}</td>
                                        <td>{{$course->credit}}</td>
                                        <td>
                                            @if($temp->status>0)
                                                <a href="{{route('chairman.mark.student.view',$course->id)}}"
                                                   class="btn btn-success">View
                                                    Srudent Marks</a>
                                            @endif
                                            @if($temp->status==1)
                                                <a href="{{route('chairman.cancel.marks.review',$course->id)}}"
                                                   class="btn btn-success js-warning" data-title="Cancel">Cancel</a>
                                                <a href="{{route('chairman.approve.marks.review',$course->id)}}"
                                                   class="btn btn-success js-warning" data-title="Approve">Appprove</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->

@endsection

@section('custom-js')
    <script>
        jQuery('.js-warning').click(function (e) {
            var href = $(this).attr('href');
            var title = $(this).data('title');
            e.preventDefault();
            swal({
                title: 'Are you sure?',
                text: 'To ' + title + ' Request',
                showCancelButton: true,
                confirmButtonText: 'Yes ' + title + '!',
                closeOnConfirm: false,
                html: false
            }, function () {
                window.location.href = href;
            });

        })
    </script>
@endsection