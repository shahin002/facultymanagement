@extends('users.chairman.master')

@section('title')
    Question Setter and Script Evaluator Theoretical
@endsection
@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12 text-center">
                <h1 class="page-heading font-w600">
                    <strong>Internal Examiner Of Practical Courses</strong> <span class="pull-right"><a
                                href="{{route('download.internalTeacher')}}" class="btn btn-primary btn-rounded">Download Full List</a></span>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Course Code</th>
                                <th>Course Title</th>
                                <th>CR. HR.</th>
                                <th>Teachers</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($teachers)
                                @foreach($teachers as $teacher)
                                    <tr>
                                        <td>{{$teacher->course->code}}</td>
                                        <td>{{$teacher->course->name}}</td>
                                        <td>{{$teacher->course->credit}} </td>
                                        <td>
                                            @foreach($teacher->practicalAssignedTeacher as $key=>$teacherDetails)
                                                <p style="line-height: 0">{{$key+1}}.
                                                    <strong>{{$teacherDetails->teacher->name}}</strong></p>
                                                <p style="line-height: 0">{{$teacherDetails->teacher->designation}}</p>
                                            @endforeach
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection