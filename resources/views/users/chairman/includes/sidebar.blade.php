<!-- Sidebar -->
<nav id="sidebar">
    <!-- Sidebar Scroll Container -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
        <div class="sidebar-content">
            <!-- Side Header -->
            <div class="side-header side-content bg-white-op">
                <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button"
                        data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times"></i>
                </button>
                <!-- Themes functionality initialized in App() -> uiHandleTheme() -->
                <a class="h5 text-white" href="{{route('chairman.home')}}">
                    <span class="h5 text-primary font-w600">{{auth('dhead')->user()->department->sort_name}}</span> <span
                            class="h5 font-w600 sidebar-mini-hide">Chairman</span>
                </a>
            </div>
            <!-- END Side Header -->

            <!-- Side Content -->
            <div class="side-content">
                <ul class="nav-main">
                    <li>
                        <a href="{{route('chairman.home')}}"><i class="si si-speedometer"></i><span
                                    class="sidebar-mini-hide">Dashboard</span></a>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-rocket"></i><span
                                    class="sidebar-mini-hide">Add Teachers</span></a>
                        <ul>
                            <li><a href="{{route('chairman.home')}}">Theoritical </a> </li>
                            <li><a href="{{route('form.practicalTeacher')}}">Practical</a> </li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-rocket"></i><span
                                    class="sidebar-mini-hide">View Teachers</span></a>
                        <ul>
                            <li><a href="{{route('view.theoreticalTeacher')}}">Theoritical </a> </li>
                            <li><a href="{{route('view.intPracticalTeacher')}}">Practical Internal</a> </li>
                            <li><a href="{{route('view.extPracticalTeacher')}}">Practical External</a> </li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-rocket"></i><span
                                    class="sidebar-mini-hide">Student Marks</span></a>
                        <ul>
                            @if(date('n')<7)
                                <li><a href="{{route('chairman.assigned.theory.courses',1)}}">Semester 1 Theory</a></li>
                                <li><a href="{{route('chairman.assigned.practical.courses',1)}}">Semester 1 Practical</a></li>
                                <li><a href="{{route('chairman.assigned.theory.courses',3)}}">Semester 3 Theory</a></li>
                                <li><a href="{{route('chairman.assigned.practical.courses',3)}}">Semester 3 Practical</a></li>
                                <li><a href="{{route('chairman.assigned.theory.courses',5)}}">Semester 5 Theory</a></li>
                                <li><a href="{{route('chairman.assigned.practical.courses',5)}}">Semester 5 Practical</a></li>
                                <li><a href="{{route('chairman.assigned.theory.courses',7)}}">Semester 7 Theory</a></li>
                                <li><a href="{{route('chairman.assigned.practical.courses',7)}}">Semester 7 Practical</a></li>
                            @else
                                <li><a href="{{route('chairman.assigned.theory.courses',2)}}">Semester 2 Theory</a></li>
                                <li><a href="{{route('chairman.assigned.practical.courses',2)}}">Semester 2 Practical</a></li>
                                <li><a href="{{route('chairman.assigned.theory.courses',4)}}">Semester 4 Theory</a></li>
                                <li><a href="{{route('chairman.assigned.practical.courses',4)}}">Semester 4 Practical</a></li>
                                <li><a href="{{route('chairman.assigned.theory.courses',6)}}">Semester 6 Theory</a></li>
                                <li><a href="{{route('chairman.assigned.practical.courses',6)}}">Semester 6 Practical</a></li>
                                <li><a href="{{route('chairman.assigned.theory.courses',8)}}">Semester 8 Theory</a></li>
                                <li><a href="{{route('chairman.assigned.practical.courses',8)}}">Semester 8 Practical</a></li>
                            @endif
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-rocket"></i><span
                                    class="sidebar-mini-hide">Prev. Student Marks</span></a>
                        <ul>
                            @if(date('n')>6)
                                <li><a href="{{route('prev.chairman.assigned.theory.courses',1)}}">Semester 1 Theory</a></li>
                                <li><a href="{{route('prev.chairman.assigned.practical.courses',1)}}">Semester 1 Practical</a></li>
                                <li><a href="{{route('prev.chairman.assigned.theory.courses',3)}}">Semester 3 Theory</a></li>
                                <li><a href="{{route('prev.chairman.assigned.practical.courses',3)}}">Semester 3 Practical</a></li>
                                <li><a href="{{route('prev.chairman.assigned.theory.courses',5)}}">Semester 5 Theory</a></li>
                                <li><a href="{{route('prev.chairman.assigned.practical.courses',5)}}">Semester 5 Practical</a></li>
                                <li><a href="{{route('prev.chairman.assigned.theory.courses',7)}}">Semester 7 Theory</a></li>
                                <li><a href="{{route('prev.chairman.assigned.practical.courses',7)}}">Semester 7 Practical</a></li>
                            @else
                                <li><a href="{{route('prev.chairman.assigned.theory.courses',2)}}">Semester 2 Theory</a></li>
                                <li><a href="{{route('prev.chairman.assigned.practical.courses',2)}}">Semester 2 Practical</a></li>
                                <li><a href="{{route('prev.chairman.assigned.theory.courses',4)}}">Semester 4 Theory</a></li>
                                <li><a href="{{route('prev.chairman.assigned.practical.courses',4)}}">Semester 4 Practical</a></li>
                                <li><a href="{{route('prev.chairman.assigned.theory.courses',6)}}">Semester 6 Theory</a></li>
                                <li><a href="{{route('prev.chairman.assigned.practical.courses',6)}}">Semester 6 Practical</a></li>
                                <li><a href="{{route('prev.chairman.assigned.theory.courses',8)}}">Semester 8 Theory</a></li>
                                <li><a href="{{route('prev.chairman.assigned.practical.courses',8)}}">Semester 8 Practical</a></li>
                            @endif
                        </ul>
                    </li>

                </ul>
            </div>
            <!-- END Side Content -->
        </div>
        <!-- Sidebar Content -->
    </div>
    <!-- END Sidebar Scroll Container -->
</nav>
<!-- END Sidebar -->