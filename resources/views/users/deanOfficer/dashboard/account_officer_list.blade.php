@extends('users.deanOfficer.master')

@section('title')
    Account Officer List
@endsection

@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Account Officer List</strong><a href="{{route('form.account')}}"
                                                            class="btn btn-primary btn-rounded pull-right"> Add Account
                        Officer</a>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Striped Table -->
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Phone Number</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($accountOfficers as $accountOfficer)
                                <tr>
                                    <td>{{$accountOfficer->name}}</td>
                                    <td>{{$accountOfficer->phone_number}}</td>
                                    <td>{{$accountOfficer->email}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- END Striped Table -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection