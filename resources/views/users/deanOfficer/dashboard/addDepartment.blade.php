@extends('users.deanOfficer.master')

@section('title')
    Add Department Chairman
@endsection

@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12 text-center">
                <h1 class="page-heading  font-w600">
                    <strong>Add a New Department Chairman</strong>
                </h1>
            </div>
        </div>
    </div>

    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{route('create.department')}}" method="post">
                            {{csrf_field()}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Name of the Department</label>
                                    <input type="text" id="dept_name" name="dept_name"
                                           placeholder="Example: Computer Science and Engineering " value=""
                                           class="form-control" required>
                                    @if ($errors->has('dept_name'))
                                        <span class="help-block text-danger">
                                    <strong>{{ $errors->first('dept_name') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Sort Form Of Department name</label>
                                    <input type="text" id="sort_name" name="sort_name" placeholder="Example: CSE"
                                           value=""
                                           class="form-control" required>
                                    @if ($errors->has('sort_name'))
                                        <span class="help-block text-danger">
                                    <strong>{{ $errors->first('sort_name') }}</strong>
                                </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Name of Departmment Chairman</label>
                                    <input type="text" id="chairman_name" name="chairman_name"
                                           placeholder="Enter a Name"
                                           value="" class="form-control" required>
                                    @if ($errors->has('chairman_name'))
                                        <span class="help-block text-danger">
                                    <strong>{{ $errors->first('chairman_name') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type="text" id="phone_number" name="phone_number"
                                           placeholder="Enter Phone Number" value="" class="form-control" required>
                                    @if ($errors->has('phone_number'))
                                        <span class="help-block text-danger">
                                    <strong>{{ $errors->first('phone_number') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" id="email" name="email" placeholder="Enter Email" value=""
                                           class="form-control" required>
                                    @if ($errors->has('email'))
                                        <span class="help-block text-danger">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" id="password" name="password"
                                           placeholder="Enter a new Password "
                                           value="" class="form-control" required>
                                    @if ($errors->has('password'))
                                        <span class="help-block text-danger">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <input type="submit" class="btn btn-lg btn-primary" value="Save">


                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection