@extends('users.deanOfficer.master')

@section('title')
    Add Semester
@endsection
@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    Add a Semester for Enrollment
                </h1>
            </div>
        </div>
    </div>


    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Static Labels -->
                        <div class="block">
                            <div class="block-content block-content-narrow">
                                <form class="form-horizontal push-10-t add-semester-form"
                                      action="{{route('add.semister')}}"
                                      method="post">
                                    @csrf

                                    <div class="form-group{{$errors->has('year')?' has-error':''}}">
                                        <div class="col-sm-12">
                                            <div class="form-material form-material-primary">
                                                <input class="form-control" type="text" id="year"
                                                       name="year" placeholder="Semester Year"
                                                       value="{{date("Y")}}" required readonly>
                                                <label for="year">Year</label>
                                            </div>
                                            @if ($errors->has('year'))
                                                <div id="year-error"
                                                     class="help-block animated fadeInDown">{{ $errors->first('year') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('month') ? ' has-error' : '' }}">
                                        <div class="col-sm-12">
                                            <div class="form-material">
                                                <select class="js-select2 form-control" id="month" name="month"
                                                        data-placeholder="Please Select Month" required>
                                                    <option></option>
                                                    <!-- Required for data-placeholder attribute to work with Select2 plugin -->

                                                    @if(date('n')<7)
                                                        <option selected value="0">January - June</option>
                                                    @else
                                                        <option selected value="1">July - December</option>
                                                    @endif
                                                </select>

                                                <label for="category_id">Month</label>
                                            </div>
                                            @if ($errors->has('month'))
                                                <div id="month-error"
                                                     class="help-block animated fadeInDown">{{ $errors->first('month') }}</div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-9">
                                            <button class="btn btn-sm btn-primary" type="submit">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- END Static Labels -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->

@endsection

@section('custom-js')
    <script src="{{asset('public/adminassets/js/pages/semester_create.js')}}"></script>
@endsection