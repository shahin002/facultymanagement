@extends('users.deanOfficer.master')

@section('title')
    Add a New Teacher
@endsection

@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12 text-center">
                <h1 class="page-heading  font-w600">
                    <strong>Add a New Teacher</strong>
                </h1>
            </div>
        </div>
    </div>

    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{route('dean.create.teacher')}}" method="post">
                            {{csrf_field()}}
                            <div class="col-md-12">

                                <div class="form-group col-sm-6">
                                    <label>Name</label>
                                    <input type="text" id="name" name="name" placeholder="Enter a Name" value=""
                                           class="form-control" required>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="form-group col-sm-6">
                                    <label>Designation</label>
                                    <select name="designation" class="form-control" required="">
                                        <option selected value="" disabled>Select Designation</option>
                                        <option value="Professor">Professor</option>
                                        <option value="Associate Professor">Associate Professor</option>
                                        <option value="Assistant Professor">Assistant Professor</option>
                                        <option value="Lecturer">Lecturer</option>
                                    </select>
                                    @if ($errors->has('designation'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('designation') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="form-group col-sm-6">
                                    <label>Phone Number</label>
                                    <input type="text" id="phone" name="phone_number" placeholder="Enter Phone Number"
                                           value="" class="form-control" required>
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="form-group col-sm-6">
                                    <label>Select Department</label>
                                    <select name="department_id" class="form-control" required="">
                                        <option selected value="" disabled>Select Department</option>
                                        @foreach($departments as $department)
                                            <option value="{{$department->id}}">{{$department->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                                <div class="form-group col-sm-6">
                                    <label>Email</label>
                                    <input type="text" id="email" name="email" placeholder="Enter Email" value=""
                                           class="form-control" required>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="form-group col-sm-6">
                                    <label>Password</label>
                                    <input type="password" id="password" name="password" placeholder="Enter Email"
                                           value=""
                                           class="form-control" required>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <input type="submit" class="btn btn-lg btn-primary" value="Save">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->


@endsection