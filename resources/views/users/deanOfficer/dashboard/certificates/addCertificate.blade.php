@extends('users.deanOfficer.master')
@section('title')
    Add Certificate
@endsection

@section('mainContent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Add New Certificate</strong>
                </h1>
            </div>
        </div>
    </div>

    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="col-lg-12 well">
                    <div class="row">
                        <form action="{{route('dean.create.certificate')}}" method="post">
                            {{csrf_field()}}
                            <div class="col-md-12">

                                <div class="form-group col-sm-6">
                                    <label>Name</label>
                                    <input type="text" id="name" name="name" placeholder="Enter a Name" value=""
                                           class="form-control" required>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="form-group col-sm-6">
                                    <label>Money Amount</label>
                                    <input type="text" name="money" placeholder="Enter Money Amount" value=""
                                           class="form-control" required>
                                    @if ($errors->has('money'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('money') }}</strong>
                                </span>
                                    @endif
                                </div>

                                <div class="form-group col-sm-12">
                                    <label>Description</label>
                                    <textarea type="text" name="description" placeholder="Enter Certificate Description"
                                              value="" class="form-control" required></textarea>
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            <input type="submit" class="btn btn-lg btn-info" value="Save">
                        </form>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <!-- END Page Content -->


@endsection