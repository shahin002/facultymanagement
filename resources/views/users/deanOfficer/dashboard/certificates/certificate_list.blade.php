@extends('users.deanOfficer.master')

@section('title')
    Certificate List
@endsection

@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Certificate List</strong>
                    {{--<button
                            class="btn btn-primary btn-rounded pull-right" data-toggle="modal"
                            data-target="#createCertificate"> Add
                        Certificate
                    </button>--}}
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Striped Table -->
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Money(tk)</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($certificates as $certificate)
                                <tr>
                                    <td>{{$certificate->name}}</td>
                                    <td>{{$certificate->money}}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="javascript://"
                                               data-link="{{route('dean.view.certificate',$certificate->id)}}"
                                               class="certificateAjaxView btn btn-xs btn-default" data-toggle="tooltip"
                                               title="View"><i class="fa fa-eye"></i></a>
                                            <a href="javascript://"
                                               data-link="{{route('dean.edit.certificate',$certificate->id)}}"
                                               class="certificateAjaxView btn btn-xs btn-default" data-toggle="tooltip"
                                               title="Edit"><i class="fa fa-pencil"></i></a>
                                            <a href="{{route('dean.delete.certificate',$certificate->id)}}"
                                               class="delete-certificate btn btn-xs btn-default" data-toggle="tooltip"
                                               title="Delete"><i class="fa fa-times"></i></a>

                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- END Striped Table -->
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- END Page Content -->
    <div class="modal fade" id="createCertificate" tabindex="-1" role="dialog" aria-labelledby="createCertificateLabel">
        <div class="modal-dialog" role="document">
            <form class="modal-content" action="{{route('dean.create.certificate')}}" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="createCertificateLabel">Create Certificate</h4>
                </div>
                <div class="modal-body">
                    <div class="row">

                        {{csrf_field()}}
                        <div class="col-md-12">

                            <div class="form-group col-sm-6">
                                <label>Name</label>
                                <input type="text" id="name" name="name" placeholder="Enter a Name" value=""
                                       class="form-control" required>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-6">
                                <label>Money Amount</label>
                                <input type="text" name="money" placeholder="Enter Money Amount" value=""
                                       class="form-control" required>
                                @if ($errors->has('money'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('money') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group col-sm-12">
                                <label>Description</label>
                                <textarea type="text" name="description" placeholder="Enter Certificate Description"
                                          class="form-control" required></textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="modalContent" tabindex="-1" role="dialog" aria-labelledby="modalContentLabel">
        <div id="loadHtml" class="modal-dialog" role="document">

        </div>
    </div>
@endsection
@section('custom-js')
    <script>
        $(document).on("click", ".certificateAjaxView", function (e) {
            e.preventDefault();
            var link = $(this).data("link");
            $.ajax({
                type: 'get',
                url: link,
                success: function (data) {
                    $('#loadHtml').html(data);
                    $('#modalContent').modal('show');
                }
            });
        });
        jQuery('.delete-certificate').on('click', function (e) {
            var href = $(this).attr('href');
            e.preventDefault();
            swal({
                title: 'Are you sure?',
                text: 'To Delete This Certificate?',
                showCancelButton: true,
                confirmButtonText: 'Yes !',
                closeOnConfirm: false,
                html: false
            }, function () {
                window.location.href = href;
            });

        });
    </script>
@endsection