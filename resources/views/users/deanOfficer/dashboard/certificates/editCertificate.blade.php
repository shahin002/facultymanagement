<form class="modal-content" action="{{route('dean.edit.certificate',$certificate->id)}}" method="post">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Certificate</h4>
    </div>
    <div class="modal-body">
        <div class="row">

            {{csrf_field()}}
            <div class="col-md-12">

                <div class="form-group col-sm-6">
                    <label>Name</label>
                    <input type="text" name="name" placeholder="Enter a Name" value="{{$certificate->name}}"
                           class="form-control" required>
                    @if ($errors->has('name'))
                        <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                    @endif
                </div>
                <div class="form-group col-sm-6">
                    <label>Money Amount</label>
                    <input type="text" name="money" placeholder="Enter Money Amount" value="{{$certificate->money}}"
                           class="form-control" required>
                    @if ($errors->has('money'))
                        <span class="help-block">
                                    <strong>{{ $errors->first('money') }}</strong>
                                </span>
                    @endif
                </div>

                <div class="form-group col-sm-12">
                    <label>Description</label>
                    <textarea type="text" name="description" placeholder="Enter Certificate Description"
                               class="form-control" required>{{$certificate->description}}</textarea>
                    @if ($errors->has('description'))
                        <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
</form>