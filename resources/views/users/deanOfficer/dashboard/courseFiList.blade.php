@extends('users.deanOfficer.master')

@section('title')
    Course FI List
@endsection

@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12 text-center">
                <h1 class="page-heading  font-w600">
                    {{$course->name}}-<strong> {{$course->code}}</strong>
                </h1>
                <h4><strong>Semester, {{$course->semister}}</strong></h4>
                <h4>{{$semister->month==0?'January-June, ':'July-December, '}}{{$semister->year}}</h4>

            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Striped Table -->
                        <div class="block">
                            <div class="block-content">
                                <table id="course-fi-list" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Roll No</th>
                                        <th>Current Semester</th>
                                        <th>Account Status</th>
                                        <th>Hall Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($courseEnrolls as $enroll)
                                        <tr>
                                            <td>{{$enroll->enroll->student->name}}</td>
                                            <td>{{$enroll->enroll->student->roll_no}}</td>
                                            <td>{{$enroll->enroll->semister}}</td>
                                            <td>{!! $enroll->enroll->account_status=="0"?"<span class='btn-danger'>Disapproved</span>":"<span class='btn-success'>Approved</span>" !!}</td>
                                            <td>{!! $enroll->enroll->hall_status=="0"?"<span class='btn-danger'>Disapproved</span>":"<span class='btn-success'>Approved</span>" !!}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END Striped Table -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->


@endsection
@section('custom-js')
    <script src="{{asset('public/adminassets/js/pages/courseFiList.js')}}"></script>
@endsection