<style>
    @page {
        margin: 0;
        padding: 0;
    }
</style>
<div style="display: block; height: 20px;"></div>
<table width="90%" align="center">
    <tr>

        <td align="right" width="300">
            <img src="{{asset('public/pdf/logo.png')}}" height="120" style="align:top"/>
        </td>
        <td>
            <b style="font-size: 23px">Faculty of Agriculture</b>
            <div style="font-size: 20px"> Patuakhali Science And Technology University <br> Dumki,Patuakhali-8602<br>Bangladesh
            </div>

        </td>

    </tr>

</table>
<div style="border: 1px solid"></div>
<div style="width: 90%; margin:0 auto; text-align:center;font-size: 16px">
    <b style="font-size: 25px">Internal Examiner Of Practical Courses</b>
    <p>B.Sc.Ag.(Hons.) Semester {{$semesterId}}({{$semisterName}}
        ),@if($semister) {{$semister->month==0?'January-June':'July-December'}} {{$semister->year}}@endif</p>

</div>
<table border="1" width="90%" align="center" style="border-collapse:collapse;" cellpadding="5">
    <tr>
        <th>Course Code</th>
        <th>Course Title</th>
        <th>CR.HR.</th>
        <th>Teachers</th>
    </tr>
    @if($teachers)
        @foreach($teachers as $teacher)
            <tr>
                <td align="center">{{$teacher->course->code}}</td>
                <td>{{$teacher->course->name}}</td>
                <td align="center">{{$teacher->course->credit}}</td>
                <td style="font-size:13px">
                    @foreach($teacher->practicalAssignedTeacher as $key=>$teacherDetails)
                        <p>{{$key+1}}. <strong>{{$teacherDetails->teacher->name}}</strong></p>
                        <p>Dept of {{$teacherDetails->teacher->department->sort_name}}, PSTU</p>
                    @endforeach
                </td>
            </tr>
        @endforeach
    @endif

</table>
