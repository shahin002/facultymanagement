@extends('users.deanOfficer.master')

@section('mainContent')
    <div class="container manageStudent" style="margin-bottom:25px;">
        <div class="page-header text-center">
            <h2>External Examiner Of Practical Courses <span style="float:right"><a
                            href="{{route('download.externalTeacher')}}" class="btn btn-success">Download Full List</a></span>
            </h2>
            <h4><strong> Semister
                    :</strong>@if($semister) {{$semister->month==0?'January-June, ':'July-December, '}}{{$semister->year}}@endif
            </h4>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Striped Table -->
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Course Code</th>
                                <th>Course Title</th>
                                <th>CR. HR.</th>
                                <th>Teachers</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($teachers)
                                @foreach($teachers as $teacher)
                                    <tr>
                                        <td>{{$teacher->course->code}}</td>
                                        <td>{{$teacher->course->name}}</td>
                                        <td>{{$teacher->course->credit}} </td>
                                        <td>
                                            @php($teacherNames = unserialize($teacher->ext_teachers) )
                                            @foreach($teacherNames as $teacherName)
                                                <p><strong>{{$teacherName['name']}}</strong></p>
                                                <p>{{$teacherName['designation']}}</p>
                                            @endforeach
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        <!-- END Striped Table -->
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection