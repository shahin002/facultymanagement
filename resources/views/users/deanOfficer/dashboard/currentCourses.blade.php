@extends('users.deanOfficer.master')

@section('title')
    Current Courses
@endsection

@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong> Current Courses</strong>
                </h1>

            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Striped Table -->
                        <div class="block">
                            <div class="block-content">
                                <table id="current-courses" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Semester</th>
                                        <th>Course Name</th>
                                        <th>Course Code</th>
                                        <th>Type</th>
                                        <th>Credit</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($courses as $course)

                                        <tr>
                                            <td>{{$course->semister}}</td>
                                            <td>{{$course->name}}</td>
                                            <td>{{$course->code}}</td>
                                            <td>{{$course->type}}</td>
                                            <td>{{$course->credit}}</td>
                                            <td class="text-center">
                                                <a href="{{route('officer.course.fi',$course->id)}}"
                                                   class="btn btn-xs btn-default"
                                                   data-toggle="tooltip" title="View Fi List"><i class="fa fa-eye"></i></a>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END Striped Table -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->


@endsection
@section('custom-js')
    <script src="{{asset('public/adminassets/js/pages/current_courses.js')}}"></script>
@endsection
