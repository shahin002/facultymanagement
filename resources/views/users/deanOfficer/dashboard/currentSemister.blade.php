@extends('users.deanOfficer.master')

@section('title')
    Current Semester
@endsection
@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Current
                        Semester:</strong> {{$enrollSemister->month==0?'January-June, ':'July-December, '}}{{$enrollSemister->year}}
                </h1>
            </div>
        </div>
    </div>


    <div class="content">
        <!-- Alerts -->
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-sm-12 col-lg-6">
                        <div class="block block-themed">
                            <div class="block-header bg-primary">
                                <h3 class="block-title text-center">Enrollment Status</h3>
                            </div>
                            <div class="block-content block-content-full text-center">
                                <div>
                                    <h1>{!!$enrollSemister->status==1?"<span class='text-success'>Open</span>":"<span class='text-danger'>Closed</span>"!!}</h1>
                                </div>
                                <div class="text-muted push-15-t"><a
                                            data-title="{{$enrollSemister->status==1?'End':'Start'}}"
                                            href="{{url('officer/'.$enrollSemister->id.'/semister')}}"
                                            class="btn btn-info change-enrollment-status">{{$enrollSemister->status==1?"End Enrollment":"Start Enrollment"}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($enrollSemister->exam_date)
                        <div class="col-sm-12 col-lg-6">
                            <div class="block block-themed">
                                <div class="block-header bg-primary">
                                    <h3 class="block-title text-center">Admit Card Download Status</h3>
                                </div>
                                <div class="block-content block-content-full text-center">
                                    <div>
                                        <h1>{!!$enrollSemister->admit_status==1?"<span class='text-success'>running</span>":"<span class='text-danger'>Closed</span>"!!}</h1>
                                    </div>
                                    <div class="text-muted push-15-t"><a
                                                data-title="{{$enrollSemister->admit_status==1?'End':'Start'}}"
                                                href="{{url('officer/'.$enrollSemister->id.'/admit')}}"
                                                class="btn btn-info change-admit-card-download-status">{{$enrollSemister->admit_status==1?"End Admit Card Download":"Start Admit Card Download"}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-sm-12 col-lg-6">
                            <div class="block block-themed">
                                <div class="block-header bg-primary">
                                    <h3 class="block-title text-center">Add Final Exam Date</h3>
                                </div>
                                <div class="block-content block-content-full text-center">
                                    <form action="{{url('officer/'.$enrollSemister->id.'/exam')}}" method="post">
                                        @csrf
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="date" name="exam_date"
                                                   placeholder="Enter final examination Date" required>
                                        </div>
                                        @if ($errors->has('exam_date'))
                                            <span class="help-block text-danger">
                                        <strong class="text-danger">{{ $errors->first('exam_date') }}</strong>
                                    </span>
                                        @endif
                                        <button type="submit" class="btn btn-default">Save</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <!-- END Alerts -->

    </div>

@endsection

@section('custom-js')

    <script src="{{asset('public/adminassets/js/pages/current_semester.js')}}"></script>
@endsection
@section('custom-styles')

@endsection

