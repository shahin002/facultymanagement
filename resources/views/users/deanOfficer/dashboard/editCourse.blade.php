@extends('users.deanOfficer.master')

@section('title')
    Department List
@endsection

@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Update Courses</strong>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">

                        <form action="{{url('update/'.$course->id.'/course')}}" method="post">
                            {{csrf_field()}}
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <label>Course Name</label>
                                        <input type="text" id="name" name="name" placeholder="Enter Course Name"
                                               class="form-control" value="{{$course->name}}" required>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                        @endif
                                    </div>

                                    <div class="col-sm-6 form-group">
                                        <label>Course Code</label>
                                        <input type="text" id="code" name="code" placeholder="Enter Course Code"
                                               class="form-control" value="{{$course->code}}" required>
                                        @if ($errors->has('code'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('code') }}</strong>
                                </span>
                                        @endif
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-4 form-group">
                                        <label>Course Semester</label>
                                        <select id="semister" name="semister" class="form-control js-select2"
                                                data-placeholder="Course Semester" required>
                                            <option></option>
                                            <option value="1" {{$course->semister==1?'selected':''}}>Semester 1</option>
                                            <option value="2" {{$course->semister==2?'selected':''}}>Semester 2</option>
                                            <option value="3" {{$course->semister==3?'selected':''}}>Semester 3</option>
                                            <option value="4" {{$course->semister==4?'selected':''}}>Semester 4</option>
                                            <option value="5" {{$course->semister==5?'selected':''}}>Semester 5</option>
                                            <option value="6" {{$course->semister==6?'selected':''}}>Semester 6</option>
                                            <option value="7" {{$course->semister==7?'selected':''}}>Semester 7</option>
                                            <option value="8" {{$course->semister==8?'selected':''}}>Semester 8</option>
                                        </select>
                                        @if ($errors->has('semister'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('semister') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>Course Type</label>
                                        <select id="type" name="type" class="form-control js-select2"
                                                data-placeholder="Course Type" required>
                                            <option></option>
                                            <option value="Theoretical" {{$course->type=='Theoretical'?'selected':''}}>
                                                Theoretical
                                            </option>
                                            <option value="Practical" {{$course->type=='Practical'?'selected':''}}>
                                                Practical
                                            </option>
                                        </select>
                                        @if ($errors->has('type'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('type') }}</strong>
                                </span>
                                        @endif
                                    </div>

                                    <div class="col-sm-4 form-group">
                                        <label>Course Credit</label>
                                        <input type="number" id="credit" name="credit" placeholder="Enter Course Credit"
                                               class="form-control" value="{{$course->credit}}" required>
                                        @if ($errors->has('credit'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('credit') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-12 form-group">
                                        <input type="submit" class="btn btn-lg btn-primary" value="Update">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->

@endsection
@section('custom-js')
    <script>
        $(document).ready(function () {
            $('.js-select2').select2();
        });
    </script>
@endsection

