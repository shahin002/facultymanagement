@extends('users.deanOfficer.master')

@section('title')
    Edit Enroll Course
@endsection
@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong> Student Form</strong>
                </h1>

            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <form name="editEnrollForm" action="{{url('update/'.$enroll->id.'/enroll')}}" method="post">
                            {{csrf_field()}}
                            <div class="col-md-12">

                                <div class="form-group">
                                    <label>Current Semister</label>
                                    <input type="text" name="enroll_semister" class="form-control"
                                           value="{{$enroll->enrollSemister->month==0?'January-June, ':'July-December, '}}{{$enroll->enrollSemister->year}}"
                                           disabled>
                                    <input type="hidden" id="eSem" name="enroll_semister_id"
                                           value="{{$enroll->enrollSemister->id}}">
                                </div>

                                <div class=" form-group">
                                    <label>Student Name</label>
                                    <input type="text" id="name" name="name" value="{{$enroll->student->name}}"
                                           class="form-control" disabled>
                                </div>

                                <div class=" form-group">
                                    <label>Student Roll</label>
                                    <input type="text" id="name" name="name" value="{{$enroll->student->roll_no}}"
                                           class="form-control" disabled>
                                </div>

                                <div class="row">

                                    <div class="col-sm-6 form-group">
                                        <label>Previous Semister GPA</label>
                                        <input type="number" step="0.01" id="pgpa" name="previous_gpa"
                                               value="{{$enroll->previous_gpa}}" class="form-control" required>
                                        @if ($errors->has('pgpa'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('pgpa') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label>CGPA</label>
                                        <input type="number" step="0.01" id="cgpa" name="cgpa" value="{{$enroll->cgpa}}"
                                               class="form-control" required>
                                        @if ($errors->has('cgpa'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('cgpa') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <label>Student Semister</label>
                                        <select class="form-control js-select2" name="semister"
                                                data-placeholder="Select Semester"
                                                id="semister" required>
                                            <option></option>
                                            @if($enroll->enrollSemister->month==0)
                                                <option value="1" {{$enroll->semister==1?'selected':''}}>Semister 1
                                                </option>
                                                <option value="3" {{$enroll->semister==3?'selected':''}}>Semister 3
                                                </option>
                                                <option value="5" {{$enroll->semister==5?'selected':''}}>Semister 5
                                                </option>
                                                <option value="7" {{$enroll->semister==7?'selected':''}}>Semister 7
                                                </option>
                                            @else
                                                <option value="2" {{$enroll->semister==2?'selected':''}}>Semister 2
                                                </option>
                                                <option value="4" {{$enroll->semister==4?'selected':''}}>Semister 4
                                                </option>
                                                <option value="6" {{$enroll->semister==6?'selected':''}}>Semister 6
                                                </option>
                                                <option value="8" {{$enroll->semister==8?'selected':''}}>Semister 8
                                                </option>
                                            @endif
                                        </select>
                                        @if ($errors->has('semister'))
                                            <span class="help-block">
										<strong>{{ $errors->first('semister') }}</strong>
									</span>
                                        @endif
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label>Student Status </label>
                                        <select id="studentStatus" class="form-control js-select2"
                                                data-placeholder="Select Status" name="student_status"
                                                required>
                                            <option></option>
                                            <option value="1" {{$enroll->student_status==1?'selected':''}}>
                                                Regular Student
                                            </option>
                                            <option value="0" {{$enroll->student_status==0?'selected':''}}>
                                                Irregular Student
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <table id="courseTable" class="table table-bordered table-hover">
                                    <caption class="text-center"><strong>Courses</strong><span style="float:right"><div
                                                    id="editCourse" class="btn btn-info btn-sm">Edit Course</div></span>
                                    </caption>

                                    <tr>
                                        <th>Course Code</th>
                                        <th>Course Title</th>
                                        <th>Course Type</th>
                                        <th>Course Credit</th>
                                        <th>Course Semester</th>
                                    </tr>
                                    @foreach($enroll->courses as $course)

                                        <tr>
                                            <td>{{$course->code}}</td>
                                            <td>{{$course->name}}</td>
                                            <td>{{$course->type}}</td>
                                            <td>{{$course->credit}}</td>
                                            <td>{{$course->semister}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                                <div id="courseSelect" class="form-group" style="visibility: hidden">
                                    <select class="form-control js-select2" data-placeholder="Select your Course"
                                            name="courses[]" id="course" multiple="multiple">

                                    </select>
                                    <input type="submit" class="btn btn-lg btn-success" value="Update">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->

@endsection
@section('custom-js')
    <script type="text/javascript">
        var getCoursekUrl = "{{route('get.course')}}";
        var tok = "{{csrf_token()}}";
    </script>
    <script src="{{asset('public/adminassets/js/pages/edit_enroll_course.js')}}"></script>
@endsection