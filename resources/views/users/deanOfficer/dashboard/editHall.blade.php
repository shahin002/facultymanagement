@extends('users.deanOfficer.master')

@section('title')
    Edit Hall
@endsection

@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12 text-center">
                <h1 class="page-heading  font-w600">
                    <strong>Edit Hall Information</strong>
                </h1>
            </div>
        </div>
    </div>

    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{url('hall/'.$hall->id.'/update')}}" method="post">
                            {{csrf_field()}}
                            <div class="col-md-12">

                                <div class="form-group">
                                    <label>Hall Name</label>
                                    <input type="text" id="name" name="name" placeholder="Enter a Name"
                                           value="{{$hall->name}}"
                                           class="form-control" required>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Provost</label>
                                    <select id="hallOffier" name="hall_officer_id" class="form-control"
                                            data-placeholder="Select a Hall Provost" required>
                                        <option></option>
                                        @foreach($hallOfficers as $hallOfficer)
                                            <option value="{{$hallOfficer->id}}" {{$hallOfficer->id==$hall->hallOfficer->id?'selected':''}}>{{$hallOfficer->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('hallOffier'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('hallOffier') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-lg btn-info" value="Save">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
@section('custom-js')
    <script>
        jQuery('#hallOffier').select2();
    </script>
@endsection