@extends('users.deanOfficer.master')

@section('title')
    Edit Profile
@endsection

@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Edit Your Profile Information</strong>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">

                        <form action="{{route('edit.profile.officer')}}" method="post">
                            {{csrf_field()}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" id="name" name="name" placeholder="Enter a New Name"
                                           value="{{auth('admin')->user()->name}}" class="form-control" required
                                           autofocus>
                                    @if ($errors->has('name'))
                                        <span class="help-block text-danger">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" id="email" name="email" placeholder="Enter a New Email"
                                           value="{{auth('admin')->user()->email}}" class="form-control" required
                                           readonly>
                                    @if ($errors->has('email'))
                                        <span class="help-block text-danger">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Designation</label>
                                    <input type="text" id="designation" name="designation"
                                           placeholder="Example: Professor"
                                           value="{{auth('admin')->user()->designation}}" class="form-control" required>
                                    @if ($errors->has('designation'))
                                        <span class="help-block text-danger">
                                    <strong>{{ $errors->first('designation') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type="text" name="phone_number" placeholder="Enter your Phone Number"
                                           value="{{auth('admin')->user()->phone_number}}" class="form-control"
                                           required>
                                    @if ($errors->has('phone_number'))
                                        <span class="help-block text-danger">
                                    <strong>{{ $errors->first('phone_number') }}</strong>
                                </span>
                                    @endif
                                </div>

                                <input type="submit" class="btn btn-lg btn-primary" value="Update">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection