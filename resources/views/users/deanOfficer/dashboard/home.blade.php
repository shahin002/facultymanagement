@extends('users.deanOfficer.master')

@section('title')
    Dashboard
@endsection
@section('maincontent')
    <!-- Page Header -->
    <div class="content bg-image overflow-hidden"
         style="background-image: url('{{asset('public/adminassets/img/photos/photo3@2x.jpg')}}');">
        <div class="push-50-t push-15">
            <h1 class="h2 text-white animated zoomIn">Dashboard</h1>
            <h2 class="h5 text-white-op animated zoomIn">Welcome Administrator</h2>
        </div>
    </div>
    <!-- END Page Header -->



    @if(!auth('admin')->user()->signature)
        <div class="content">
            <div class="block">
                <div class="block-content">
                    <div class="alert alert-danger">
                        <h3>You haven't Upload your <strong>Signature</strong> yet!<span class="pull-right"><a
                                        href="{{route('signatureForm.admin')}}"
                                        class="btn btn-success ">Upload It</a></span></h3>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if(empty($dean))
        <div class="content">
            <div class="block">
                <div class="block-content">
                    <div class="alert alert-danger">
                        <h3>You haven't Upload Dean <strong>Info</strong> yet!<span class="pull-right"><a
                                        href="{{route('officer.dean.info')}}"
                                        class="btn btn-success ">Update</a></span></h3>
                    </div>
                </div>
            </div>
        </div>
    @if(empty($exam_cntroller))
        <div class="content">
            <div class="block">
                <div class="block-content">
                    <div class="alert alert-danger">
                        <h3>You haven't Upload Exam Controller's <strong>Info</strong> yet!<span class="pull-right"><a
                                        href="{{route('officer.exam_controller.info')}}"
                                        class="btn btn-success ">Update</a></span></h3>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @endif
@endsection

@section('custom-styles')

@endsection
@section('custom-js')
@endsection