@extends('users.deanOfficer.master')

@section('title')
    Manage Courses
@endsection
@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong> Course List</strong>
                </h1>

            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{route('add.course')}}" method="post">
                            {{csrf_field()}}
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-4 form-group">
                                        <label>Course Name</label>
                                        <input type="text" id="name" name="name" placeholder="Enter Course Name"
                                               class="form-control" required>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                        @endif
                                    </div>

                                    <div class="col-sm-4 form-group">
                                        <label>Course Code</label>
                                        <input type="text" id="code" name="code" placeholder="Enter Course Code"
                                               class="form-control" required>
                                        @if ($errors->has('code'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('code') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>Select Department</label>
                                        <select name="department_id" class="form-control js-select2"
                                                data-placeholder="Select Department" required>
                                            <option></option>
                                            @foreach($depts as $dept)
                                                <option value="{{$dept->id}}">{{$dept->name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('department_id'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('department_id') }}</strong>
                                </span>
                                        @endif

                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-4 form-group">
                                        <label>Course Semester</label>
                                        <select id="semister" name="semister" class="form-control js-select2"
                                                data-placeholder="Select Semester" required>
                                            <option></option>
                                            <option value="1">Semester 1</option>
                                            <option value="2">Semester 2</option>
                                            <option value="3">Semester 3</option>
                                            <option value="4">Semester 4</option>
                                            <option value="5">Semester 5</option>
                                            <option value="6">Semester 6</option>
                                            <option value="7">Semester 7</option>
                                            <option value="8">Semester 8</option>
                                        </select>
                                        @if ($errors->has('semister'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('semister') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>Course Type</label>
                                        <select id="type" name="type" class="form-control js-select2"
                                                data-placeholder="Select Type of Course" required>
                                            <option></option>
                                            <option value="Theoretical">Theoretical</option>
                                            <option value="Practical">Practical</option>
                                        </select>
                                        @if ($errors->has('type'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('type') }}</strong>
                                </span>
                                        @endif
                                    </div>

                                    <div class="col-sm-4 form-group">
                                        <label>Course Credit</label>
                                        <input type="number" id="credit" name="credit" placeholder="Enter Course Credit"
                                               class="form-control" required>
                                        @if ($errors->has('credit'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('credit') }}</strong>
                                </span>
                                        @endif
                                    </div>

                                </div>


                                <input type="submit" class="btn btn-lg btn-primary btn-rounded" value="Add Course">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
    <!-- Page Content -->
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <!-- Striped Table -->
                <div class="block">
                    <div class="block-content">
                        <table id="manage-courses" class="table table-striped">
                            <thead>
                            <tr>
                                <th>Course Name</th>
                                <th>Course Code</th>
                                <th>Type</th>
                                <th>Semester</th>
                                <th>Credit</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($courses as $course)
                                <tr>
                                    <td>{{$course->name}}</td>
                                    <td>{{$course->code}}</td>
                                    <td>{{$course->type}}</td>
                                    <td>{{$course->semister}}</td>
                                    <td>{{$course->credit}}</td>
                                    <td class="text-center">
                                        <a href="{{url('edit/'.$course->id.'/course')}}" class="btn btn-xs btn-default"
                                           data-toggle="tooltip" title="Edit Course"><i class="fa fa-pencil"></i></a>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END Striped Table -->
            </div>
        </div>
    </div>
    <!-- END Page Content -->

@endsection
@section('custom-js')
    <script src="{{asset('public/adminassets/js/pages/manage_courses.js')}}"></script>
@endsection
