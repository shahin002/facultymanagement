@extends('users.deanOfficer.master')

@section('title')
    Hall List
@endsection

@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12 text-center">
                <h1 class="page-heading  font-w600">
                    <strong>Hall List</strong>
                </h1>
            </div>
        </div>
    </div>

    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Striped Table -->
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Hall Provost</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($halls as $hall)
                                <tr>
                                    <td>{{$hall->name}}</td>
                                    <td>{{$hall->hallOfficer?$hall->hallOfficer->name:"..."}}</td>
                                    <td>
                                        <a href="{{url('/hall/'.$hall->id.'/edit')}}"
                                           class="btn btn-xs btn-default" data-toggle="tooltip"
                                           title="Edit Hall"><i class="fa fa-pencil"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- END Striped Table -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection