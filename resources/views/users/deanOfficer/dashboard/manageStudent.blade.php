@extends('users.deanOfficer.master')

@section('title')
    Manage Student
@endsection
@section('maincontent')

    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Striped Table -->
                        <div class="block">
                            <div class="block-header">
                                <div class="block-options">
                                    <a href="{{route('student.addform')}}" class="btn btn-danger btn-rounded">Add
                                        Student</a>
                                </div>
                                <h3 class="block-title">Manage Student</h3>
                            </div>
                            <div class="block-content">
                                <table class="table table-striped student-list-table">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Roll No</th>
                                        <th>Registration No</th>
                                        <th>Hall Name</th>
                                        <th>Phone No</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($students as $student)
                                        <tr>
                                            <td>{{$student->name}}</td>
                                            <td>{{$student->roll_no}}</td>
                                            <td>{{$student->reg_no}}</td>
                                            <td>{{$student->hall->name}}</td>
                                            <td>{{@$student->studentInfo->phone_number}}</td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <a href="{{url('officer/'.$student->id.'/student')}}"
                                                       class="btn btn-xs btn-default" data-toggle="tooltip"
                                                       title="View Details"><i class="fa fa-eye"></i></a>
                                                </div>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END Striped Table -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
@section('custom-js')
    <script src="{{asset('public/adminassets/js/pages/manage_student_list.js')}}"></script>

@endsection