@extends('users.deanOfficer.master')

@section('title')
    Manage Enroll
@endsection
@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong> Enrollment
                        :</strong> {{$semester->month==0?'January-June, ':'July-December, '}}{{$semester->year}}
                </h1>
                <h1 class="page-heading text-center font-w600">
                    <strong> Semester :</strong> {{$semesterId}}
                </h1>

            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Striped Table -->
                        <div class="block">
                            <div class="block-content">
                                <table id="manage-enrollment" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Roll No</th>
                                        <th>Semester</th>
                                        <th>Account Status</th>
                                        <th>Hall Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($enrolls as $enroll)
                                        <tr>
                                            <td>{{$enroll->student->name}}</td>
                                            <td>{{$enroll->student->roll_no}}</td>
                                            <td>{{$enroll->semister}}</td>
                                            <td>{!! $enroll->account_status=="0"?"<span class='text-danger'>Disapproved</span>":"<span class='text-success'>Approved</span>" !!}</td>
                                            <td>{!! $enroll->hall_status=="0"?"<span class='text-danger'>Disapproved</span>":"<span class='text-success'>Approved</span>" !!}</td>
                                            <td class="text-center"><a href="{{url('/officer/'.$enroll->id.'/enroll')}}"
                                                                       class="btn btn-info">View</a> <a
                                                        href="{{url('edit/'.$enroll->id.'/enroll')}}"
                                                        class="btn btn-warning">Edit</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                </table>
                            </div>
                        </div>
                        <!-- END Striped Table -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

@section('custom-js')
    <script src="{{asset('public/adminassets/js/pages/manage_enrolls.js')}}"></script>

@endsection