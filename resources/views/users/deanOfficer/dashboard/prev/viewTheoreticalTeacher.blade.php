@extends('users.deanOfficer.master')

@section('title')
    Theory Course Teacher of Semester {{$semesterId}}
@endsection

@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12 text-center">
                <h1 class="page-heading  font-w600">
                    Question Setter and Script Evaluattor <a
                            href="{{route('prev.officer.download.theoreticalTeacher',array('semesterId'=>$semesterId))}}"
                            class="btn btn-primary btn-rounded pull-right">Download Full List</a>
                </h1>
                <h4><strong> Semister: {{$semesterId}}
                        , </strong> @if($semister) {{$semister->month==0?'January-June, ':'July-December, '}}{{$semister->year}}@endif
                </h4>


            </div>
        </div>
    </div>

    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Striped Table -->
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Course Code</th>
                                <th>Course Title</th>
                                <th>CR. HR.</th>
                                <th>Teachers</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($teachers)
                                @foreach($teachers as $teacher)
                                    <tr>
                                        <td>{{$teacher->course->code}}</td>
                                        <td>{{$teacher->course->name}}</td>
                                        <td>{{$teacher->course->credit}} </td>
                                        <td>
                                            @foreach($teacher->theoryAssignedTeachers as $teacherDetails)
                                                <p class="mb-0"><strong>{{$teacherDetails->teacher->name}}</strong></p>
                                                <p class="mb-0">{{$teacherDetails->teacher->designation}}</p>
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="btn-group">

                                                @if($teacher->status>=2)
                                                    <a href="{{route('prev.officer.mark.student.view',$teacher->course->id)}}"
                                                       class="btn btn-xs btn-default" data-toggle="tooltip"
                                                       title="View Student Marks"><i class="fa fa-eye"></i></a>
                                                @endif
                                                @if($teacher->status>2)
                                                    <a href="{{route('prev.officer.mark.student.download',$teacher->course->id)}}"
                                                       class="btn btn-xs btn-default" data-toggle="tooltip"
                                                       title="Download Student Marks"><i class="fa fa-download"></i></a>
                                                @endif
                                                @if($teacher->status==2)
                                                    <a href="{{route('prev.officer.approve.marks.review',$teacher->course->id)}}"
                                                       class="btn btn-xs btn-default js-alert" data-toggle="tooltip"
                                                       title="Accept Student Marks">
                                                        <i class="fa fa-check"></i>
                                                    </a>
                                                    <a href="{{route('prev.officer.cancel.marks.review',$teacher->course->id)}}"
                                                       class="btn btn-xs btn-default js-alert" data-toggle="tooltip"
                                                       title="Reject Student Marks">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        <!-- END Striped Table -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->

@endsection
@section('custom-js')
    <script>
        jQuery('.js-alert').on('click', function (e) {
            var href = $(this).attr('href');
            var title = $(this).data('original-title');
            e.preventDefault();
            swal({
                title: 'Are you sure?',
                text: 'To ' + title,
                showCancelButton: true,
                confirmButtonText: 'Yes, ' + title + '!',
                closeOnConfirm: false,
                html: false
            }, function () {
                window.location.href = href;
            });

        });
    </script>
@endsection