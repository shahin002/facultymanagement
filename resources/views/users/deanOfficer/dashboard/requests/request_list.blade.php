@extends('users.deanOfficer.master')

@section('title')
    Certificate Request List
@endsection

@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Certificate Request List</strong>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Striped Table -->
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Student Name</th>
                                <th>ID</th>
                                <th>Session</th>
                                <th>Certificate</th>
                                <th>Money(tk)</th>
                                <th>Received No</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($request_list as $c_request)
                                <tr>
                                    <td>{{$c_request->student->name}}</td>
                                    <td>{{$c_request->student->roll_no}}</td>
                                    <td>{{$c_request->student->session}}</td>
                                    <td>{{$c_request->certificate->name}}</td>
                                    <td>{{$c_request->money_amount}}</td>
                                    <td>{{$c_request->received_number}}</td>
                                    <td>{{$c_request->status==1?'Requested':($c_request->status==2?'Approved':'Downloaded')}}</td>
                                    <td class="text-center">
                                        <a href="javascript://"
                                           data-link="{{route('dean.view.certificate.request',$c_request->id)}}"
                                           class="certificateAjaxView btn btn-success" data-toggle="tooltip"
                                           title="view"><span
                                                    class="glyphicon glyphicon-eye-open"
                                                    aria-hidden="true"></span> </a>
                                        @if($c_request->status!=0)
                                            <a href="{{route('dean.approve.certificate.request',$c_request->id)}}"
                                               class="btn btn-{{$c_request->status==1?'info':'danger'}} {{(in_array($c_request->certificate_id,[7,9]) && $c_request->status==1)?' openDurationModal':' js-warning'}}"
                                               data-toggle="tooltip"
                                               title="{{$c_request->status==1?'Approve':'cancel'}}"><span
                                                        class="glyphicon {{$c_request->status==1?'glyphicon-ok-sign':'glyphicon-remove-sign'}}"
                                                        aria-hidden="true"></span> </a>
                                        @endif
                                        <a href="{{route('dean.delete.certificate.request',$c_request->id)}}"
                                           class="btn btn-danger js-warning" data-toggle="tooltip" title="Delete"><span
                                                    class="glyphicon glyphicon-trash"
                                                    aria-hidden="true"></span> </a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- END Striped Table -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
    <div class="modal fade" id="duration" tabindex="-1" role="dialog" aria-labelledby="createDurationLabel">
        <div class="modal-dialog" role="document">
            <form id="createDurationForm" class="modal-content" action="" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="createDurationLabel">Tour Duration</h4>
                </div>
                <div class="modal-body">
                    <div class="row">

                        {{csrf_field()}}
                        <div class="col-md-12">

                            <div class="form-group col-sm-6">
                                <label>Tour Start Date</label>
                                <input type="text" name="start_date" placeholder="Enter Starting Date"
                                       class="form-control js-date" required readonly>
                            </div>

                            <div class="form-group col-sm-6">
                                <label>Tour End Date</label>
                                <input type="text" name="end_date" placeholder="Enter End Date"
                                       class="form-control js-date" required readonly>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="modalContent" tabindex="-1" role="dialog" aria-labelledby="modalContentLabel">
        <div id="loadHtml" class="modal-dialog" role="document">

        </div>
    </div>
@endsection
@section('custom-js')
    <script>
        $(document).ready(function () {
            $(document).on("click", ".certificateAjaxView", function (e) {
                e.preventDefault();
                var link = $(this).data("link");
                $.ajax({
                    type: 'get',
                    url: link,
                    success: function (data) {
                        $('#loadHtml').html(data);
                        $('#modalContent').modal('show');
                    }
                });
            });
            $('.openDurationModal').click(function (e) {
                e.preventDefault();
                var href = $(this).attr('href');
                $('#createDurationForm').attr('action', href);
                $('#duration').modal('show');

            });
            $('.js-warning').on('click', function (e) {
                var href = $(this).attr('href');
                var title = $(this).data('original-title');
                e.preventDefault();
                swal({
                    title: 'Are you sure?',
                    text: 'To ' + title + ' This Request?',
                    showCancelButton: true,
                    confirmButtonText: 'Yes !',
                    closeOnConfirm: false,
                    html: false
                }, function () {
                    window.location.href = href;
                });

            });
            $('.js-date').datepicker({
                weekStart: 1,
                autoclose: true,
                todayHighlight: true
            });
        });
    </script>
@endsection