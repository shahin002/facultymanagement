@extends('users.deanOfficer.master')

@section('title')
    Update Exam Controller Info
@endsection

@section('maincontent')
    {{--{{dd($exam_controller)}}--}}
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12 text-center">
                <h1 class="page-heading  font-w600">
                    <strong>Update Exam Controller Info</strong>
                </h1>
            </div>
        </div>
    </div>

    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{route('officer.exam_controller.info')}}" id="dean-officer-form" method="post"
                              enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label>Name</label>
                                        <input type="text" id="name" name="name" placeholder="Enter a Name"
                                               value="{{old('name')?old('name'):@$exam_controller['name']}}"
                                               class="form-control" required>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label>Phone Number</label>
                                        <input type="text" id="phone" name="phone_number"
                                               placeholder="Enter Phone Number"
                                               value="{{old('phone_number')?old('phone_number'):@$exam_controller['phone_number']}}"
                                               class="form-control" required>
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                                        @endif
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label>Email</label>
                                        <input type="text" id="email" name="email" placeholder="Enter Email"
                                               value="{{old('email')?old('email'):@$exam_controller['email']}}"
                                               class="form-control" required>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label>Exam Controller Signature</label>
                                        <input type="file" id="signature" name="signature"
                                               accept="image/*" {{is_null(@$exam_controller['signature'])?'required':''}}>
                                        @if ($errors->has('signature'))
                                            <span class="help-block text-danger">
                                            <strong>{{ $errors->first('signature') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label>Current Signature</label><br>
                                        <img src="{{asset(@$exam_controller['signature'])}}" alt="Not Available">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-lg btn-primary" value="Save">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->


@endsection
@section('custom-js')
    <script>
        $(document).ready(function () {
            'use strict';
            jQuery('#dean-officer-form').validate();
            jQuery('.js-select2').select2();
            jQuery('.js-select2').on('change', function () {
                jQuery(this).valid();
            });

        });
    </script>
@endsection