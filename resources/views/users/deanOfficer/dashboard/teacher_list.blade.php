@extends('users.deanOfficer.master')

@section('title')
    Department List
@endsection

@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Department List</strong><a href="{{route('dean.create.teacher')}}"
                                                       class="btn btn-primary btn-rounded pull-right"> Add Account
                        Officer</a>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Striped Table -->
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Mobile</th>
                                <th>Department Name</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($teachers as $teacher)
                                <tr>
                                    <td>{{$teacher->name}}</td>
                                    <td>{{$teacher->designation}}</td>
                                    <td>{{$teacher->phone_number}}</td>
                                    <td>{{$teacher->department->name}}</td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- END Striped Table -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection