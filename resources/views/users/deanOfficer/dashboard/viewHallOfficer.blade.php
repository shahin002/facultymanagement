@extends('users.deanOfficer.master')

@section('title')
    Hall Provost List
@endsection

@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Hall Provost List</strong>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Striped Table -->
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Phone Number</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($hallOfficers as $hallOfficer)
                                <tr>
                                    <td>{{$hallOfficer->name}}</td>
                                    <td>{{$hallOfficer->phone_number}}</td>
                                    <td>{{$hallOfficer->email}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- END Striped Table -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection