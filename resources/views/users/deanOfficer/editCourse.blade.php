@extends('users.deanOfficer.master')
@section('mainContent')
    <div class="container">
        <div class="block">
            <div class="block-content">
                <h2 class="well text-center"><strong>Update Courses</strong></h2>
                <div class="col-lg-12 well">
                    <div class="row">
                        <form action="{{url('update/'.$course->id.'/course')}}" method="post">
                            {{csrf_field()}}
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <label>Course Name</label>
                                        <input type="text" id="name" name="name" placeholder="Enter Course Name"
                                               class="form-control" value="{{$course->name}}" required>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                        @endif
                                    </div>

                                    <div class="col-sm-6 form-group">
                                        <label>Course Code</label>
                                        <input type="text" id="code" name="code" placeholder="Enter Course Code"
                                               class="form-control" value="{{$course->code}}" required>
                                        @if ($errors->has('code'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('code') }}</strong>
                                </span>
                                        @endif
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-4 form-group">
                                        <label>Course Semester</label>
                                        <select id="semister" name="semister" class="form-control" required>
                                            <option selected disabled>Select Semester</option>
                                            <option value="1">Semester 1</option>
                                            <option value="2">Semester 2</option>
                                            <option value="3">Semester 3</option>
                                            <option value="4">Semester 4</option>
                                            <option value="5">Semester 5</option>
                                            <option value="6">Semester 6</option>
                                            <option value="7">Semester 7</option>
                                            <option value="8">Semester 8</option>
                                        </select>
                                        @if ($errors->has('semister'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('semister') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>Course Type</label>
                                        <select id="type" name="type" class="form-control" required>
                                            <option selected disabled>Select Type of Course</option>
                                            <option value="Theoretical">Theoretical</option>
                                            <option value="Practical">Practical</option>
                                        </select>
                                        @if ($errors->has('type'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('type') }}</strong>
                                </span>
                                        @endif
                                    </div>

                                    <div class="col-sm-4 form-group">
                                        <label>Course Credit</label>
                                        <input type="number" id="credit" name="credit" placeholder="Enter Course Credit"
                                               class="form-control" value="{{$course->credit}}" required>
                                        @if ($errors->has('credit'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('credit') }}</strong>
                                </span>
                                        @endif
                                    </div>

                                </div>
                                <input type="submit" id="submitBtn" class="btn btn-lg btn-info" value="Update"
                                       style="visibility:hidden">
                            </div>


                            <input type="submit" class="btn btn-lg btn-info" value="Update">
                        </form>

                    </div>
                </div>

            </div>
        </div>

    </div>
    <script>
        $(document).ready(function () {
            $('#semister').val({{$course->semister}});
            $('#type').val("{{$course->type}}");
        });
    </script>
@endsection

