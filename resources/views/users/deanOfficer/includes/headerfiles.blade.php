<meta charset="utf-8">

<meta name="robots" content="noindex, nofollow">
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
<title>@yield('title')-Faculty of Agriculture</title>
<!-- Icons -->
<!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
<link rel="shortcut icon" href="{{asset('public/adminassets/img/favicons/favicon.png')}}">

<!-- Stylesheets -->
<!-- Web fonts -->
<link rel="stylesheet"
      href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

<link rel="stylesheet" href="{{asset('public/adminassets/js/plugins/select2/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('public/adminassets/js/plugins/select2/select2-bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('public/adminassets/js/plugins/datatables/jquery.dataTables.min.css')}}">

<link rel="stylesheet" href="{{asset('public/adminassets/js/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css')}}">
<link rel="stylesheet" href="{{asset('public/adminassets/js/plugins/sweetalert/sweetalert.min.css')}}">

<!-- Bootstrap and OneUI CSS framework -->
<link rel="stylesheet" href="{{asset('public/adminassets/css/bootstrap.min.css')}}">
<link rel="stylesheet" id="css-main" href="{{asset('public/adminassets/css/oneui.css')}}">
@yield('custom-styles')
<!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
<!-- <link rel="stylesheet" id="css-theme" href="{{asset('public/adminassets/css/themes/flat.min.css')}}"> -->
<!-- END Stylesheets -->