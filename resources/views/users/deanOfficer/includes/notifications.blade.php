
@if(!empty(Session::get('mgs')))
    <script>
        $.notify({
            icon: 'glyphicon glyphicon-ok',
            title: '<strong>Success:</strong>',
            message: '{{Session::get('mgs')}}'
        },{
            type: 'success',
        });
    </script>

@elseif(!empty(Session::get('mgsErr')))
<script>
    $.notify({
        icon: 'glyphicon glyphicon-warning-sign',
        title: '<strong>Error:</strong>',
        message: '{{Session::get('mgsErr')}}'
    },{
        type: 'danger'
    });
</script>
@endif