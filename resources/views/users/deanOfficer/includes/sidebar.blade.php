<!-- Sidebar -->
<nav id="sidebar">
    <!-- Sidebar Scroll Container -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
        <div class="sidebar-content">
            <!-- Side Header -->
            <div class="side-header side-content bg-white-op">
                <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button"
                        data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times"></i>
                </button>
                <!-- Themes functionality initialized in App() -> uiHandleTheme() -->
                <a class="h5 text-white" href="{{route('admin.home')}}">
                    <span class="h5 text-primary font-w600">Dean</span> <span class="h5 font-w600 sidebar-mini-hide">Officer</span>
                </a>
            </div>
            <!-- END Side Header -->

            <!-- Side Content -->
            <div class="side-content">
                <ul class="nav-main">
                    <li>
                        <a href="{{route('admin.home')}}"><i class="si si-speedometer"></i><span
                                    class="sidebar-mini-hide">Dashboard</span></a>
                    </li>
                    {{--<li class="nav-main-heading"><span class="sidebar-mini-hide">User Interface</span></li>--}}
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-rocket"></i><span
                                    class="sidebar-mini-hide">Semester</span></a>
                        <ul>
                            <li><a href="{{route('add.semister')}}">Add Semester</a></li>
                            <li><a href="{{route('view.semister')}}">Current Semester</a></li>
                        </ul>
                    </li>
                    <li><a href="{{route('manage.student')}}"><i class="si si-layers"></i>
                            <span>Manage Student</span></a></li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-layers"></i><span
                                    class="sidebar-mini-hide">Manage Enrolls</span></a>
                        <ul>
                            @if(date('n')<7)
                                <li><a href="{{route('manage.enroll',1)}}">Semester 1</a></li>
                                <li><a href="{{route('manage.enroll',3)}}">Semester 3</a></li>
                                <li><a href="{{route('manage.enroll',5)}}">Semester 5</a></li>
                                <li><a href="{{route('manage.enroll',7)}}">Semester 7</a></li>
                            @else
                                <li><a href="{{route('manage.enroll',2)}}">Semester 2</a></li>
                                <li><a href="{{route('manage.enroll',4)}}">Semester 4</a></li>
                                <li><a href="{{route('manage.enroll',6)}}">Semester 6</a></li>
                                <li><a href="{{route('manage.enroll',8)}}">Semester 8</a></li>
                            @endif
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-bag"></i><span
                                    class="sidebar-mini-hide">Manage Course </span></a>
                        <ul>
                            <li><a href="{{route('manage.course')}}">Course List</a></li>
                            <li><a href="{{route('current.courses')}}">Current Courses</a></li>
                        </ul>
                    </li>


                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-bag"></i>
                            <span class="sidebar-mini-hide">Co. Teachers & Marks</span></a>
                        <ul>
                            @if(date('n')<7)
                                <li><a href="{{url('officer/theoryCourseTeachers/1')}}">Semester 1 Theory</a></li>
                                <li><a href="{{url('officer/practicalCourseTeachers/1')}}">Semester 1 Practical</a></li>
                                <li><a href="{{url('officer/theoryCourseTeachers/3')}}">Semester 3 Theory</a></li>
                                <li><a href="{{url('officer/practicalCourseTeachers/3')}}">Semester 3 Practical</a></li>
                                <li><a href="{{url('officer/theoryCourseTeachers/5')}}">Semester 5 Theory</a></li>
                                <li><a href="{{url('officer/practicalCourseTeachers/5')}}">Semester 5 Practical</a></li>
                                <li><a href="{{url('officer/theoryCourseTeachers/7')}}">Semester 7 Theory</a></li>
                                <li><a href="{{url('officer/practicalCourseTeachers/7')}}">Semester 7 Practical</a></li>
                            @else
                                <li><a href="{{url('officer/theoryCourseTeachers/2')}}">Semester 2 Theory</a></li>
                                <li><a href="{{url('officer/practicalCourseTeachers/2')}}">Semester 2 Practical</a></li>
                                <li><a href="{{url('officer/theoryCourseTeachers/4')}}">Semester 4 Theory</a></li>
                                <li><a href="{{url('officer/practicalCourseTeachers/4')}}">Semester 4 Practical</a></li>
                                <li><a href="{{url('officer/theoryCourseTeachers/6')}}">Semester 6 Theory</a></li>
                                <li><a href="{{url('officer/practicalCourseTeachers/6')}}">Semester 6 Practical</a></li>
                                <li><a href="{{url('officer/theoryCourseTeachers/8')}}">Semester 8 Theory</a></li>
                                <li><a href="{{url('officer/practicalCourseTeachers/8')}}">Semester 8 Practical</a></li>
                            @endif
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-bag"></i>
                            <span class="sidebar-mini-hide">Pre. Co. Teac. & Marks</span></a>
                        <ul>
                            @if(date('n')>6)
                                <li><a href="{{url('prev/officer/theoryCourseTeachers/1')}}">Semester 1 Theory</a></li>
                                <li><a href="{{url('prev/officer/practicalCourseTeachers/1')}}">Semester 1 Practical</a></li>
                                <li><a href="{{url('prev/officer/theoryCourseTeachers/3')}}">Semester 3 Theory</a></li>
                                <li><a href="{{url('prev/officer/practicalCourseTeachers/3')}}">Semester 3 Practical</a></li>
                                <li><a href="{{url('prev/officer/theoryCourseTeachers/5')}}">Semester 5 Theory</a></li>
                                <li><a href="{{url('prev/officer/practicalCourseTeachers/5')}}">Semester 5 Practical</a></li>
                                <li><a href="{{url('prev/officer/theoryCourseTeachers/7')}}">Semester 7 Theory</a></li>
                                <li><a href="{{url('prev/officer/practicalCourseTeachers/7')}}">Semester 7 Practical</a></li>
                            @else
                                <li><a href="{{url('prev/officer/theoryCourseTeachers/2')}}">Semester 2 Theory</a></li>
                                <li><a href="{{url('prev/officer/practicalCourseTeachers/2')}}">Semester 2 Practical</a></li>
                                <li><a href="{{url('prev/officer/theoryCourseTeachers/4')}}">Semester 4 Theory</a></li>
                                <li><a href="{{url('prev/officer/practicalCourseTeachers/4')}}">Semester 4 Practical</a></li>
                                <li><a href="{{url('prev/officer/theoryCourseTeachers/6')}}">Semester 6 Theory</a></li>
                                <li><a href="{{url('prev/officer/practicalCourseTeachers/6')}}">Semester 6 Practical</a></li>
                                <li><a href="{{url('prev/officer/theoryCourseTeachers/8')}}">Semester 8 Theory</a></li>
                                <li><a href="{{url('prev/officer/practicalCourseTeachers/8')}}">Semester 8 Practical</a></li>
                            @endif
                        </ul>
                    </li>

                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-bag"></i>
                            <span class="sidebar-mini-hide">Manage Halls</span></a>
                        <ul>
                            <li><a href="{{route('manage.hall')}}">Hall List</a></li>
                            <li><a href="{{route('showCreateProvostForm')}}">Create Hall Provost</a></li>
                            <li><a href="{{route('view.hallOfficer')}}">All Hall Provost</a></li>
                        </ul>
                    </li>


                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-bag"></i>
                            <span class="sidebar-mini-hide">Account Officers</span></a>
                        <ul>
                            <li><a href="{{route('form.account')}}">Add Account</a></li>
                            <li><a href="{{route('account.officers')}}">Account Officer List</a></li>
                        </ul>
                    </li>


                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-bag"></i>
                            <span class="sidebar-mini-hide">Manage Departments</span></a>
                        <ul>
                            <li><a href="{{route('form.department')}}">Add Department</a></li>
                            <li><a href="{{route('department.list')}}">Department List</a></li>
                        </ul>
                    </li>

                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-bag"></i>
                            <span class="sidebar-mini-hide">Manage Teachers</span></a>
                        <ul>
                            <li><a href="{{route('dean.create.teacher')}}">Add Teacher</a></li>
                            <li><a href="{{route('dean.teacher.list')}}">Teacher List</a></li>
                        </ul>
                    </li>

                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-bag"></i>
                            <span class="sidebar-mini-hide">Certificates/Requests</span></a>
                        <ul>
                            <li><a href="{{route('dean.manage.certificate')}}">Manage Certificates</a></li>
                            <li><a href="{{route('dean.manage.certificat.requests')}}">Manage Requests</a></li>
                        </ul>
                    </li>

                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-bag"></i>
                            <span class="sidebar-mini-hide">Others</span></a>
                        <ul>
                            <li><a href="{{route('officer.dean.info')}}">Update Dean Info</a></li>
                            <li><a href="{{route('officer.exam_controller.info')}}">Update Exam Controller Info</a></li>

                        </ul>
                    </li>

                </ul>
            </div>
            <!-- END Side Content -->
        </div>
        <!-- Sidebar Content -->
    </div>
    <!-- END Sidebar Scroll Container -->
</nav>
<!-- END Sidebar -->