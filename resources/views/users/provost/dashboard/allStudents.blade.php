@extends('users.provost.master')

@section('title')
    Student List
@endsection
@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Students
                        Of {{auth('provost')->user()->hall?auth('provost')->user()->hall->name:""}}</strong>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Roll No</th>
                                <th>Phone No</th>
                                <th>Session</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($students)
                                @foreach($students as $student)
                                    <tr>
                                        <td>{{$student->name}}</td>
                                        <td>{{$student->roll_no}}</td>
                                        <td>{{$student->phone_number}}</td>
                                        <td>{{$student->session}}</td>
                                        <td>{{$student->email}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection