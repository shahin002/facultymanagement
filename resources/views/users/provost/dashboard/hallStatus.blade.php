@extends('users.provost.master')

@section('title')
    Enrollment View
@endsection
@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Hall Status
                        :</strong> {!! $enroll->hall_status=="1"?"<span class='text-success'>Approved</span>":"<span class='text-danger'>Disapproved</span>" !!}
                    <span style="float:right"><a class="btn btn-primary js-alert"
                                                 href="{{url('/provost/'.$enroll->id.'/change')}}"
                                                 data-title="{{$enroll->hall_status=="1"?"Disapprove":"Approve"}}">{{$enroll->hall_status=="1"?"Disapprove":"Approve"}}
                                                 </a> </span>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-12 well">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="panel panel-default asses ">
                                        <div class="panel-body">
                                            <p><strong>Student Name :</strong> {{$enroll->student->name}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="panel panel-default asses ">
                                        <div class="panel-body">
                                            <p><strong>Student Roll :</strong> {{$enroll->student->roll_no}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="panel panel-default asses ">
                                        <div class="panel-body">
                                            <p><strong>Student Current Semester :</strong> {{$enroll->semister}}</p>
                                        </div>
                                    </div>
                                </div>
                                <table id="courseTable" class="table table-bordered table-hover">
                                    <caption class="text-center"><strong>Courses</strong></caption>

                                    <tr>
                                        <th>Course Code</th>
                                        <th>Course Title</th>
                                        <th>Course Type</th>
                                        <th>Course Credit</th>
                                        <th>Course Semester</th>
                                    </tr>
                                    @foreach($enroll->courses as $course)

                                        <tr>
                                            <td>{{$course->code}}</td>
                                            <td>{{$course->name}}</td>
                                            <td>{{$course->type}}</td>
                                            <td>{{$course->credit}}</td>
                                            <td>{{$course->semister}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>

                        <div class="col-lg-12">

                            <h5 class="text-center"><strong>Hall Money Receipt</strong></h5>
                            <div class="row">
                                @if(empty($receipt))
                                    <h4 class="text-danger text-center">No money Receipt found for
                                        <strong>{{$enroll->student->name}} </strong> yet!</h4>
                                @else
                                    <div class="col-md-4">
                                        <div class="panel panel-default asses ">
                                            <div class="panel-body">
                                                <p><strong>Receipt No :</strong> {{$receipt->receipt}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="panel panel-default asses ">
                                            <div class="panel-body">
                                                <p><strong>Amount (BDT) :</strong> {{$receipt->amount}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="panel panel-default asses ">
                                            <div class="panel-body">
                                                <p><strong>Hall :</strong> {{$enroll->student->hall->name}}</p>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

@section('custom-js')
    <script>
        jQuery('.js-alert').click(function (e) {
            var href = $(this).attr('href');
            var title = $(this).data('title');
            e.preventDefault();
            swal({
                title: 'Are you sure?',
                text: 'To ' + title + ' This Enrollment?',
                showCancelButton: true,
                confirmButtonText: 'Yes ' + title + '!',
                closeOnConfirm: false,
                html: false
            }, function () {
                window.location.href = href;
            });

        })
    </script>
@endsection