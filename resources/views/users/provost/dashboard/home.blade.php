@extends('users.provost.master')

@section('title')
    Dashboard
@endsection
@section('maincontent')
    <!-- Page Header -->
    <div class="content bg-image overflow-hidden"
         style="background-image: url('{{asset('public/adminassets/img/photos/photo3@2x.jpg')}}');">
        <div class="push-50-t push-15">
            <h1 class="h2 text-white animated zoomIn">Dashboard</h1>
            <h2 class="h5 text-white-op animated zoomIn">Welcome {{auth('provost')->user()->name}}</h2>
        </div>
    </div>
    <!-- END Page Header -->



    @if(!auth('provost')->user()->signature)
        <div class="content">
            <div class="block">
                <div class="block-content">
                    <div class="alert alert-danger">
                        <h3>You haven't Upload your <strong>Signature</strong> yet!<span class="pull-right"><a
                                        href="{{route('signatureForm.provost')}}" class="btn btn-success ">Upload It</a></span>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="content bg-gray-lighter hidden-print">
        <div class="block">
            <div class="block-content">
                <div class="row items-push">
                    <div class="col-sm-12 text-center">
                        <h1 class="page-heading font-w600">
                            <strong>Enrollment
                                : </strong>@if($semister) {{$semister->month==0?'January-June, ':'July-December, '}}{{$semister->year}}@endif
                        </h1>
                        @if(@$semesterId)<h2><strong> Semester :</strong> {{@$semesterId}}</h2>@endIf
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Roll No</th>
                                <th>Semester</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($enrolls)
                                @foreach($enrolls as $enroll)
                                    <tr>
                                        <td>{{$enroll->student->name}}</td>
                                        <td>{{$enroll->student->roll_no}}</td>
                                        <td>{{$enroll->semister}}</td>
                                        <td>{!! $enroll->account_status=="0"?"<span class='text-danger'>Disapproved</span>":"<span class='text-success'>Approved</span>" !!}</td>
                                        <td class="text-center"><a href="{{url('provost/'.$enroll->id.'/enroll')}}"
                                                                   class="btn btn-warning">View</a></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection