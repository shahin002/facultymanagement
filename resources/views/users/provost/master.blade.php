<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-focus"> <!--<![endif]-->
<head>
    {{--Header Files--}}
    @include('users.provost.includes.headerfiles')
    {{--End Header Files--}}

</head>
<body>
{{-- Page Container --}}
<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">

    {{--Sidebar--}}
    @include('users.provost.includes.sidebar')
    {{--END Sidebar--}}

    {{-- Header --}}
    @include('users.provost.includes.header')
    {{--END Header--}}

    {{--Main Container--}}
    <main id="main-container">
        @yield('maincontent')
    </main>
    {{--END Main Container--}}

    {{--Footer--}}
    @include('users.provost.includes.footer')
    {{--END Footer--}}
</div>
{{-- END Page Container --}}


{{--OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js --}}
{{--Footer Files--}}
@include('users.provost.includes.footerfiles')
@include('users.provost.includes.notifications')
{{--End Footer Files--}}
@yield('custom-js')
</body>
</html>