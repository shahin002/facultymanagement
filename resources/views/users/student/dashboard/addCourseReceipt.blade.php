@extends('users.student.master')

@section('title')
    Add Course Money
@endsection
@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Add Money Receipt Information</strong>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        @if(empty($retakeSemister))
                            <h3 class="text-success text-center">All Receipt are Submitted</h3>
                        @endif
                        @if(in_array($enroll->semister, $retakeSemister))
                            <div class="row">
                                <form action="{{route('save.courseMoney')}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="semister" value="{{$enroll->semister}}">
                                    <input type="hidden" name="enroll_id" value="{{$enroll->id}}">
                                    <h5 class="text-center">Semester {{$enroll->semister}}</h5>
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('receipt') ? ' has-error' : '' }}">
                                            <label>Receipt No.</label>
                                            <input type="text" id="receipt" name="receipt"
                                                   placeholder="Enter a Receipt No"
                                                   class="form-control" required autofocus>
                                            @if ($errors->has('receipt'))
                                                <span class="help-block text-danger">
                                        <strong>{{ $errors->first('receipt') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Amount (BDT)</label>
                                            <input type="number" id="amount" name="amount"
                                                   placeholder="Enter amount of money"
                                                   class="form-control" required>
                                            @if ($errors->has('amount'))
                                                <span class="help-block text-danger">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-lg btn-primary" value="Save">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endif

                        @foreach($retakeSemister as $semister)
                            @if($semister == $enroll->semister)
                                @continue
                            @endif
                            <div class="row" style="margin-top:10px">
                                <form action="{{route('save.courseMoney')}}" method="post">
                                    {{csrf_field()}}

                                    <input type="hidden" name="semister" value="{{$semister}}">
                                    <input type="hidden" name="enroll_id" value="{{$enroll->id}}">
                                    <h5 class="text-center">Semester {{$semister}}</h5>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Receipt No.</label>
                                            <input type="text" id="receipt" name="receipt"
                                                   placeholder="Enter a Receipt No"
                                                   class="form-control" required>
                                            @if ($errors->has('receipt'))
                                                <span class="help-block text-danger">
                                        <strong>{{ $errors->first('receipt') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Amount (BDT)</label>
                                            <input type="number" id="amount" name="amount"
                                                   placeholder="Enter amount of money"
                                                   class="form-control" required>
                                            @if ($errors->has('amount'))
                                                <span class="help-block text-danger">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-lg btn-primary" value="Save">
                                        </div>
                                    </div>

                                </form>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->

@endsection