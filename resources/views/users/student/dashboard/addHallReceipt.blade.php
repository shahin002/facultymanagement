@extends('users.student.master')

@section('title')
    Add Hall Money Receipt
@endsection
@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Add Hall Money Receipt Information</strong>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        @if($receipt)
                            <h3 class="text-success text-center">Hall Money Receipt is Submitted</h3>
                        @else
                            <div class="row">
                                <form action="{{route('save.hallMoney')}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="semister" value="{{$enroll->semister}}">
                                    <input type="hidden" name="enroll_id" value="{{$enroll->id}}">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Receipt No.</label>
                                            <input type="text" id="receipt" name="receipt"
                                                   placeholder="Enter a Receipt No"
                                                   class="form-control" required autofocus>
                                            @if ($errors->has('receipt'))
                                                <span class="help-block text-danger">
                                        <strong>{{ $errors->first('receipt') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Amount (BDT)</label>
                                            <input type="number" id="amount" name="amount"
                                                   placeholder="Enter amount of money"
                                                   class="form-control" required>
                                            @if ($errors->has('amount'))
                                                <span class="help-block text-danger">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-lg btn-primary" value="Save">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection