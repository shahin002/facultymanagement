@extends('users.student.master')

@section('title')
    Add Course Money
@endsection
@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Admit Card</strong>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        @foreach($admitSemister as $semister)

                            <div class="col-md-12">
                                <div class="page-header text-center">
                                    <h2>Admit Card</h2>
                                    <h4>Patuakhali Science and Technology University</h4>
                                    <h5>Dumki, Patuakhali-86-02</h5>
                                    <h3>Semester
                                        : {{$enroll->enrollSemister->month==0?'January-June, ':'July-December, '}}{{$enroll->enrollSemister->year}}</h3>
                                </div>


                                <div class="row ">
                                    <div class="col-md-6">
                                        <div class="panel panel-default asses ">
                                            <div class="panel-body">
                                                <p><strong>Semester Of Student :</strong> {{$semister}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="panel panel-default asses ">
                                            <div class="panel-body">
                                                <p><strong>Name Of Student :</strong> {{$enroll->student->name}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-md-6">
                                        <div class="panel panel-default asses ">
                                            <div class="panel-body">
                                                <p><strong>Roll No :</strong>{{$enroll->student->roll_no}}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="panel panel-default asses ">
                                            <div class="panel-body">
                                                <p><strong>Registration No :</strong> {{$enroll->student->reg_no}}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-md-6">
                                        <div class="panel panel-default asses ">
                                            <div class="panel-body">
                                                <p><strong>Session :</strong> {{$enroll->student->session}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="panel panel-default asses ">
                                            <div class="panel-body">
                                                <p><strong>Date Of Examination
                                                        :</strong> {{$enroll->enrollSemister->exam_date}}</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <br>
                                <div class="row text-center sigature">
                                    <div class="col-md-6">
                                        <p>Student</p>
                                        <hr>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Examination Controller</p>
                                        <hr>
                                    </div>
                                </div>


                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->



@endsection

