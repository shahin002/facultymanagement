<!DOCTYPE html>
<html lang="ban">
<head>
    <title>Admit Card</title>
    <meta charset="UTF-8">
    <style>
        @page {
            margin: 0 auto;
            padding: 0;
        }
    </style>
</head>
<body>
@foreach($admitSemister as $semister)
    <table width="90%" border="2" align="center">
        <tr>
            <td>
                <table height="150" width="100%" align="center">
                    <tr>
                        <td width="120">
                            <img src="{{asset('public/pdf/logo.png')}}" height="120" style="align:top"/>
                        </td>
                        <td align="center">
                            <p></p>
                            <br>
                            <h3><b>Patuakhali Science And Technology University</b></h3>
                            <h4>Dumki,Patuakhali-8602,Bangladesh</h4>
                            <br>
                            <br>
                            <h2>ADMIT CARD</h2>
                        </td>
                        <td width="120">
                            <img src="{{$enroll->student->image}}" height="120" style="align:top"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <p>Semester {{$semister}} ({{$semisterName[$semister]}}
                    ) {{$enroll->enrollSemister->month==0?'January-June, ':'July-December, '}}{{$enroll->enrollSemister->year}}
                    Final Examination of <b>B.Sc.Ag.(Hons.)</b>
                </p>
            </td>
        </tr>
        <tr style="height: 20px;">
            <td align="center">
                <p>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td>
                <table width="90%" align="center">
                    <tr>
                        <td colspan="2"><strong>Name:</strong> {{$enroll->student->name}}</td>
                        <td><strong>Examination Roll No/ID:</strong> {{$enroll->student->roll_no}}</td>
                    </tr>
                    <tr>
                        <td><strong> Reg No:</strong>{{$enroll->student->reg_no}}</td>
                        <td><strong> Session:</strong> {{$enroll->student->session}}</td>
                        <td><strong>Date of Examination:</strong>{{$enroll->enrollSemister->exam_date}}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <br>
                <br>
                <table width="90%" align="center">
                    <tr>
                        <td><img src="{{asset($enroll->student->signature)}}" height="35" width="80"
                                 style="margin-left:50px"/></td>
                        <td><img src="{{asset($exam_cntrl_sig)}}" height="35" width="80"
                                 style="margin-left:50px"/></td>
                    </tr>
                    <tr>
                        <td style="text-decoration: overline"> Signature of the Student</td>
                        <td style="text-decoration: overline">Controller of Examination</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="bangla-font">
            <td align="center">
                <br>
                <div style="font-family: 'freeserif';text-decoration: underline; width:90%;">পরীক্ষার্থীদের জন্য
                    নির্দেশাবলীঃ
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <table style="font-size: 14px;margin:auto; font-family: 'freeserif';" width="90%" align="center">
                    <tr>
                        <td width="50%">০১.ইস্যুকৃত প্রবেশপত্র ছাড়া কোন পরীক্ষার্থীকে পরিক্ষার হলে প্রবেশ করতে দেয়া হবে
                            না।
                        </td>
                        <td width="50%" text-align="letter-spacing">
                            <div style="margin-left:20px">০৫.পরিক্ষা কেন্দ্রের আইন শৃঙ্খলা ভঙ্গকারী ও শাস্তিযোগ্য
                                অপরাধকারী পরীক্ষার্থীর বিরুদ্ধে উপযুক্ত আইনানুগ ব্যবস্থা গ্রহণ করা হবে
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td>০২.পরীক্ষার সময়সূচী ও স্থান পরীক্ষার্থীকে নিজ দায়িত্বে জেনে নিতে হবে।</td>
                        <td>
                            <div style="margin-left:20px"> ০৬.আবেদনপত্রে কোন অসত্য তথ্য লিখিত হলে আইন মোতাবেক ব্যবস্থা
                                গ্রহণ করা হবে
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td>০৩.পরিক্ষা খাতার কভার পৃষ্ঠার মুদ্রিত নির্দেশাবলী পরীক্ষার্থীগণ যথাযথভাবে অনুসরণ করবে</td>
                        <td>
                            <div style="margin-left:20px">০৭. ডুপ্লিকেট প্রবেশ প্ত্রের জন্য ১০০ (একশত) টাকা ফি রশিদের
                                মাধ্যমে প্রদান করতে হবে
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td>০৪.পরীক্ষার্থী হলের দায়িত্বপ্রাপ্ত কর্মকর্তাদের নির্দেশাবলী যথাযথভাবে মেনে চলতে হবে</td>
                        <td>
                            <div style="margin-left:20px"> ০৮.ট্রান্সক্রিপ্ট /মার্ক্সসিট,সার্টিফিকেট উত্তোলনের সময়
                                ইস্যকৃত প্রবেশ প্রত্র প্রদর্শন করতে হবে
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    @if (!$loop->last)
        <div style="page-break-before:always">&nbsp;</div>
    @endif
@endforeach
</body>
</html>
