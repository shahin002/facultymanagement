@extends('users.student.master')

@section('title')
    Enrollment Form
@endsection
@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Enrollment Form</strong>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <form id="enrollment-form" action="{{route('student.enroll')}}" method="post">
                            {{csrf_field()}}
                            <div class="col-md-12">

                                <div class="form-group">
                                    <label>Current Semester</label>
                                    <input type="text" name="enroll_semister" class="form-control"
                                           value="{{$enrollSemister->month==0?'January-June, ':'July-December, '}}{{$enrollSemister->year}}"
                                           disabled>
                                    <input type="hidden" id="eSem" name="enroll_semister_id"
                                           value="{{$enrollSemister->id}}">
                                </div>

                                <div class="row">

                                    <div class="col-sm-6 form-group {{ $errors->has('previous_gpa') ? ' has-error' : '' }}">
                                        <label>Previous Semester GPA</label>
                                        <input type="number" id="pgpa" min="0" max="4" name="previous_gpa"
                                               placeholder="Enter your Previous Semester GPA" class="form-control"
                                               required>
                                        @if ($errors->has('previous_gpa'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('previous_gpa') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-6 form-group {{ $errors->has('cgpa') ? ' has-error' : '' }}">
                                        <label>CGPA</label>
                                        <input type="number" id="cgpa" name="cgpa" min="0" max="4"
                                               placeholder="Enter your CGPA"
                                               class="form-control" required>
                                        @if ($errors->has('cgpa'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('cgpa') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6 form-group {{ $errors->has('semister') ? ' has-error' : '' }}">
                                        <label>Your Semester</label>
                                        <select class="form-control js-select2" name="semister" required id="semister"
                                                data-placeholder="Select Your Semester">
                                            <option></option>
                                            @if($enrollSemister->month==0)
                                                <option value="1">Semester 1</option>
                                                <option value="3">Semester 3</option>
                                                <option value="5">Semester 5</option>
                                                <option value="7">Semester 7</option>
                                            @else
                                                <option value="2">Semester 2</option>
                                                <option value="4">Semester 4</option>
                                                <option value="6">Semester 6</option>
                                                <option value="8">Semester 8</option>
                                            @endif
                                        </select>
                                        @if ($errors->has('semister'))
                                            <span class="help-block">
										<strong>{{ $errors->first('semister') }}</strong>
									</span>
                                        @endif
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label>Student Status </label>
                                        <select class="form-control js-select2" name="student_status"
                                                data-placeholder="Select Student Status" required>
                                            <option></option>
                                            <option class="form-control" value="1">Regular Student</option>
                                            <option class="form-control" value="0">Irregular Student</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <select class="form-control js-select2" name="courses[]" id="course"
                                            multiple="multiple" data-placeholder="Select Your Courses" required>
                                        {{--  @foreach($courses as $course)
                                            <option name="{{$course->semister}}" value="{{$course->id}}">{{$course->name}} ( {{$course->code}} ) - <small>{{$course->semister}} semister</small></option>
                                        @endforeach  --}}
                                    </select>
                                </div>
                                <input type="submit" class="btn btn-lg btn-primary" value="Enroll">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->

@endsection
@section('custom-js')
    <script src="{{asset('public/adminassets/js/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <script type="text/javascript">
        var getCoursekUrl = "{{route('get.course')}}";
        var tok = "{{csrf_token()}}";
        jQuery('#enrollment-form').validate();
    </script>
    <script src="{{asset('public/adminassets/js/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('public/adminassets/js/pages/edit_enroll_course.js')}}"></script>
@endsection