@extends('users.student.master')

@section('title')
    Enrollment Form
@endsection
@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12 text-center">
                <h1 class="page-heading font-w600">
                    Enrollment Form : <span
                            class="text-warning"><strong>{{$enroll->account_status==1 && $enroll->hall_status==1?'Active':'Inactive'}}</strong></span>
                    <span style="float:right"><a class="btn btn-success" href="{{route('download.enrolled')}}">Download Pdf</a></span>
                </h1>
                <p><strong>Semester :</strong> {{$semisterName}}
                    , {{$enroll->enrollSemister->month==0?'January-June, ':'July-December, '}}{{$enroll->enrollSemister->year}}
                </p>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default asses ">
                            <div class="panel-body">
                                <p><strong>Semester :</strong> {{$semisterName}}
                                    , {{$enroll->enrollSemister->month==0?'January-June, ':'July-December, '}}{{$enroll->enrollSemister->year}}
                                </p>
                            </div>
                        </div>

                        <div class="panel panel-default  asses ">
                            <div class="panel-body">
                                <p><strong>Name Of Student :</strong> {{$enroll->student->name}}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="panel panel-default asses ">
                                    <div class="panel-body">
                                        <p><strong>Roll No :</strong>{{$enroll->student->roll_no}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="panel panel-default asses ">
                                    <div class="panel-body">
                                        <p><strong>Registration No :</strong> {{$enroll->student->reg_no}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="panel panel-default asses ">
                                    <div class="panel-body">
                                        <p><strong>Session :</strong> {{$enroll->student->session}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="panel panel-default asses ">
                                    <div class="panel-body">
                                        <p><strong>Student CGPA :</strong> {{$enroll->cgpa}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="panel panel-default asses ">
                                    <div class="panel-body">
                                        <p><strong>Previous Semester GPA :</strong> {{$enroll->previous_gpa}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="panel panel-default asses ">
                                    <div class="panel-body">
                                        <p><strong>Student
                                                Status:</strong> {{$enroll->student_status==1?'Regular':'Irregular'}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default asses ">
                            <div class="panel-body">
                                <p><strong>Name of the Hall :</strong> {{$enroll->student->hall->name}}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-default asses ">
                                    <div class="panel-body">
                                        <p><strong>Student Mobile No :</strong> {{$enroll->student->phone_number}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel panel-default asses ">
                                    <div class="panel-body">
                                        <p><strong>Date :</strong> {{$enroll->created_at->toFormattedDateString()}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default asses ">
                            <div class="panel-body">
                                <table class="table table-bordered table-hover">
                                    <caption class="text-center"><strong>Courses</strong></caption>

                                    <tr>
                                        <th>Course Code</th>
                                        <th>Course Title</th>
                                        <th>Course Type</th>
                                        <th>Course Credit</th>
                                    </tr>
                                    @php ($credit = 0 )
                                    @foreach($enroll->courses as $course)
                                        @if($enroll->semister != $course->semister)
                                            @continue
                                        @endif
                                        <tr>
                                            <td>{{$course->code}}</td>
                                            <td>{{$course->name}}</td>
                                            <td>{{$course->type}}</td>
                                            <td>{{$course->credit}}</td>
                                        </tr>
                                        @php($credit += $course->credit)
                                    @endforeach
                                    <tr>
                                        <td colspan="3" class="text-center"><strong>Total Credit</strong></td>
                                        <td>{{$credit}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        @if($enroll->has_retake)
                            @php($retakeCredit = 0)
                            <div class="panel panel-default asses ">
                                <div class="panel-body">
                                    <table class="table table-bordered table-hover">
                                        <caption class="text-center"><strong>Courses: Retake</strong></caption>
                                        <tr>
                                            <th>Course Code</th>
                                            <th>Course Title</th>
                                            <th>Course Type</th>
                                            <th>Semester</th>
                                            <th>Course Credit</th>
                                        </tr>
                                        @foreach($enroll->courses as $course)
                                            @if($enroll->semister == $course->semister)
                                                @continue
                                            @endif
                                            <tr>
                                                <td>{{$course->code}}</td>
                                                <td>{{$course->name}}</td>
                                                <td>{{$course->type}}</td>
                                                <td>{{$course->semister}}</td>
                                                <td>{{$course->credit}}</td>
                                            </tr>
                                            @php($retakeCredit += $course->credit)
                                        @endforeach
                                        <tr>
                                            <td colspan="4" class="text-center"><strong>Total Credit</strong></td>
                                            <td>{{$retakeCredit}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        @endif

                        <br>
                        <div class="row text-center sigature">
                            <div class="col-sm-3">

                                <p>Student</p>
                                <hr>
                                <img src="{{asset($enroll->student->signature)}}">


                            </div>
                            <div class="col-sm-3">
                                <p>Account Officer</p>
                                <hr>
                                @if($enroll->account_status == 1 )
                                    <img src="{{asset($enroll->accountOfficer->signature)}}">
                                @endif

                            </div>
                            <div class="col-sm-3">
                                <p>Hall Provost</p>
                                <hr>
                                @if($enroll->hall_status == 1 )
                                    <img src="{{asset($enroll->student->hall->hallOfficer->signature)}}">
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <p>Dean Faculty Of Agriculture</p>
                                <hr>
                                @if($enroll->account_status == 1 && $enroll->hall_status==1)
                                    <img src="{{asset($adminSignature)}}">
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection

