@extends('users.student.master')

@section('title')
    Image Upload
@endsection
@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Profile Image Upload</strong>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <form action="{{route('imageupload.student')}}" enctype="multipart/form-data" method="post">
                            {{csrf_field()}}
                            <div class="col-md-12">

                                <div class="form-group">
                                    <label>Your Profile Image</label>
                                    <input type="file" id="signature" name="image" accept="image/*" required>
                                    @if ($errors->has('image'))
                                        <span class="help-block text-danger">
                                    <strong>{{ $errors->first('image') }}</strong>
                                </span>
                                    @endif
                                </div>

                                <input type="submit" name="submit" class="btn  btn-info" value="Upload">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection