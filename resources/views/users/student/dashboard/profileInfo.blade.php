@extends('users.student.master')

@section('title')
    Edit Information
@endsection

@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Information Edit Form</strong>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <form id="studentForm" action="{{route('student.info')}}" method="post">
                            {{csrf_field()}}
                            <div class="col-md-12">

                                <div class="form-group">
                                    <label>Full Name</label>
                                    <input type="text" id="name" name="name"
                                           value="{{old('name')?old('name'):$student->name}}"
                                           placeholder="Enter First Name Here.." class="form-control" required>
                                    @if ($errors->has('name'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <label>Father's Name</label>
                                        <input type="text"
                                               value="{{old('father_name')?old('father_name'):@$student->studentInfo->father_name}}"
                                               id="fName" name="father_name"
                                               placeholder="Enter your Father's name" class="form-control" required>
                                        @if ($errors->has('father_name'))
                                            <span class="text-danger">
										        <strong>{{ $errors->first('father_name') }}</strong>
									        </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label>Mother's Name</label>
                                        <input type="text"
                                               value="{{old('mother_name')?old('mother_name'):@$student->studentInfo->mother_name}}"
                                               id="mName" name="mother_name"
                                               placeholder="Enter your Mother's name" class="form-control" required>
                                        @if ($errors->has('mother_name'))
                                            <span class="text-danger">
                                            <strong>{{ $errors->first('mother_name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <label>Roll No.</label>
                                        <input type="number" min="0"
                                               value="{{old('roll_no')?old('roll_no'):@$student->roll_no}}"
                                               id="roll" name="roll_no"
                                               placeholder="Enter your Roll no." class="form-control" required readonly
                                               disabled="true">
                                        @if ($errors->has('roll_no'))
                                            <span class="text-danger">
                                                    <strong>{{ $errors->first('roll_no') }}</strong>
                                             </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label>Registration No.</label>
                                        <input type="number" min="0"
                                               value="{{old('reg_no')?old('reg_no'):@$student->reg_no}}"
                                               id="reg"
                                               name="reg_no"
                                               placeholder="Enter your Registration no." class="form-control" required
                                               readonly disabled="true">
                                        @if ($errors->has('reg_no'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('reg_no') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type="text"
                                           value="{{old('phone_number')?old('phone_number'):@$student->studentInfo->phone_number}}"
                                           id="phone"
                                           name="phone_number"
                                           placeholder="Enter Phone Number Here.." class="form-control" required>
                                    @if ($errors->has('phone_number'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('phone_number') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <label>Gender </label>
                                        <select id="gender" class="form-control js-select2" name="gender"
                                                data-placeholder="Please Select Gender" required>
                                            <option></option>
                                            <option {{$student->gender==1?'selected':''}} value="1">Male</option>
                                            <option {{$student->gender==2?'selected':''}} value="2">Female</option>
                                            <option {{$student->gender==0?'selected':''}} value="0">Other</option>
                                        </select>
                                        @if ($errors->has('gender'))
                                            <span class="text-danger">
                                            <strong>{{ $errors->first('gender') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label>Hall Name</label>
                                        <select id="hallName" class="form-control js-select2" disabled="true" readonly
                                                data-placeholder="Select your Hall" name="hall_id" required>
                                            <option></option>
                                            @foreach($halls as $hall)
                                                <option {{$student->hall_id==$hall->id?'selected':''}} value="{{$hall->id}}">{{$hall->name}}</option>
                                            @endforeach

                                        </select>
                                        @if ($errors->has('hall_id'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('hall_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Session</label>
                                    <input type="text" required
                                           value="{{old('session')?old('session'):@$student->session}}"
                                           id="session" name="session"
                                           placeholder="Enter your Session" class="form-control">
                                    @if ($errors->has('session'))
                                        <span class="text-danger">
                                                <strong>{{ $errors->first('session') }}</strong>
                                            </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email"
                                           value="{{old('email')?old('email'):@$student->email}}"
                                           id="email"
                                           name="email"
                                           placeholder="Enter your Email" class="form-control" required>
                                    @if ($errors->has('email'))
                                        <span class="text-danger">
                                                <strong>{{ $errors->first('email') }}</strong>
                                         </span>
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 form-group">
                                        <label>SSC Roll No</label>
                                        <input type="number" min="0"
                                               value="{{old('ssc_roll')?old('ssc_roll'):@$student->studentInfo->ssc_roll}}"
                                               id="sroll" name="ssc_roll" placeholder="Enter your SSC Roll no"
                                               class="form-control" required>
                                        @if ($errors->has('ssc_roll'))
                                            <span class="text-danger">
										            <strong>{{ $errors->first('ssc_roll') }}</strong>
									            </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>SSC Registration No</label>
                                        <input type="number" min="0"
                                               value="{{old('ssc_reg')?old('ssc_reg'):@$student->studentInfo->ssc_reg}}"
                                               id="sreg" name="ssc_reg" placeholder="Enter your SSC Registration no"
                                               class="form-control" required>
                                        @if ($errors->has('ssc_reg'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('ssc_reg') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>SSC GPA</label>
                                        <input type="number" min="0" max="5"
                                               value="{{old('ssc_gpa')?old('ssc_gpa'):@$student->studentInfo->ssc_gpa}}"
                                               id="sgpa" name="ssc_gpa" placeholder="Enter your SSC GPA"
                                               class="form-control" required>
                                        @if ($errors->has('ssc_gpa'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('ssc_gpa') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4 form-group">
                                        <label>SSC Institute Name</label>
                                        <input type="text"
                                               value="{{old('ssc_institute')?old('ssc_institute'):@$student->studentInfo->ssc_institute}}"
                                               id="sinst"
                                               name="ssc_institute" placeholder="Enter your SSC Institute "
                                               class="form-control" required>
                                        @if ($errors->has('ssc_institute'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('ssc_institute') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>SSC Board Name</label>
                                        <input type="text"
                                               value="{{old('ssc_board')?old('ssc_board'):@$student->studentInfo->ssc_board}}"
                                               id="sboard"
                                               name="ssc_board" placeholder="Enter your SSC Board Name."
                                               class="form-control" required>
                                        @if ($errors->has('ssc_board'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('ssc_board') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>SSC Year</label>
                                        <input type="number" min="0"
                                               value="{{old('ssc_year')?old('ssc_year'):@$student->studentInfo->ssc_year}}"
                                               id="syear"
                                               name="ssc_year" placeholder="Enter your SSC Passing Year ."
                                               class="form-control" required>
                                        @if ($errors->has('ssc_year'))
                                            <span class="text-danger">
                                    <strong>{{ $errors->first('ssc_year') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4 form-group">
                                        <label>HSC Roll No</label>
                                        <input type="number" min="0"
                                               value="{{old('hsc_roll')?old('hsc_roll'):@$student->studentInfo->hsc_roll}}"
                                               id="hroll" name="hsc_roll" placeholder="Enter your HSC Roll no"
                                               class="form-control" required>
                                        @if ($errors->has('hsc_roll'))
                                            <span class="text-danger">
										<strong>{{ $errors->first('hsc_roll') }}</strong>
									</span>
                                        @endif
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>HSC Registration No</label>
                                        <input type="number" min="0"
                                               value="{{old('hsc_reg')?old('hsc_reg'):@$student->studentInfo->hsc_reg}}"
                                               id="hreg" name="hsc_reg" placeholder="Enter your HSC Registration no"
                                               class="form-control" required>
                                        @if ($errors->has('hsc_reg'))
                                            <span class="text-danger">
                                    <strong>{{ $errors->first('hsc_reg') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>HSC GPA</label>
                                        <input type="number" min="0" max="5"
                                               value="{{old('hsc_gpa')?old('hsc_gpa'):@$student->studentInfo->hsc_gpa}}"
                                               id="hgpa" name="hsc_gpa" placeholder="Enter your HSC GPA"
                                               class="form-control" required>
                                        @if ($errors->has('hsc_gpa'))
                                            <span class="text-danger">
                                    <strong>{{ $errors->first('hsc_gpa') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4 form-group">
                                        <label>HSC Institute Name</label>
                                        <input type="text"
                                               value="{{old('hsc_institute')?old('hsc_institute'):@$student->studentInfo->hsc_institute}}"
                                               id="hinst"
                                               name="hsc_institute" placeholder="Enter your SSC Institute "
                                               class="form-control" required>
                                        @if ($errors->has('hsc_institute'))
                                            <span class="text-danger">
                                    <strong>{{ $errors->first('hsc_institute') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>HSC Board Name</label>
                                        <input type="text"
                                               value="{{old('hsc_board')?old('hsc_board'):@$student->studentInfo->hsc_board}}"
                                               id="hboard"
                                               name="hsc_board" placeholder="Enter your HSC Board Name."
                                               class="form-control" required>
                                        @if ($errors->has('hsc_board'))
                                            <span class="text-danger">
                                    <strong>{{ $errors->first('hsc_board') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>HSC Year</label>
                                        <input type="number" min="0"
                                               value="{{old('hsc_year')?old('hsc_year'):@$student->studentInfo->hsc_year}}"
                                               id="hyear"
                                               name="hsc_year" placeholder="Enter your SSC Passing Year ."
                                               class="form-control" required>
                                        @if ($errors->has('hsc_year'))
                                            <span class="text-danger">
                                    <strong>{{ $errors->first('hsc_year') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>

                                <input type="submit" class="btn btn-lg btn-info" value="Register">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->

@endsection

@section('custom-js')
    <script src="{{asset('public/adminassets/js/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{asset('public/adminassets/js/plugins/select2/select2.full.min.js')}}"></script>

    <script>

        var token = "{{csrf_token()}}";
        var currentEmail = '{{$student->email}}';
        var currentRoll = '{{$student->roll_no}}';
        var currentReg = '{{$student->reg_no}}';
        var emailUrl = "{{route('check.studentEmail')}}";
        var rollUrl = "{{route('check.roll')}}";
        var regUrl = "{{route('check.reg')}}";

    </script>
    <script src="{{asset('public/adminassets/js/pages/student_form_validation.js')}}"></script>

@endsection