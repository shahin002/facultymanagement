@extends('users.student.master')

@section('title')
    Certificate Request
@endsection
@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Create Certificate Request</strong>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <form id="certificate-request" action="{{route('student.create.certificate.request')}}"
                              method="post">
                            {{csrf_field()}}

                            <div class="col-md-12">

                                <div class="form-group col-sm-6">

                                    <label>Select Certificate</label>
                                    <select name="certificate_id" class="form-control js-select2"
                                            data-placeholder="Select Certificate" required>
                                        <option></option>
                                        @foreach($certificates as $certificate)
                                            <option value="{{$certificate->id}}">{{$certificate->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label>Money Amount</label>
                                    <input type="number" name="money_amount" placeholder="Enter Money Amount"
                                           class="form-control" required>
                                    @if ($errors->has('money_amount'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('money_amount') }}</strong>
                                </span>
                                    @endif
                                </div>


                                <div class="form-group col-sm-6">
                                    <label>Comment</label>
                                    <textarea type="text" name="comment" placeholder="Enter Comments"
                                              value="{{old('comment')?old('comment'):''}}" required
                                              class="form-control"></textarea>
                                </div>


                                <div class="form-group col-sm-6">
                                    <label>Money Received Number</label>
                                    <input type="text" name="received_number" placeholder="Enter Money Received Number"
                                           value="{{old('received_number')?old('received_number'):''}}"
                                           class="form-control"
                                           required>
                                    @if ($errors->has('received_number'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('received_number') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="form-group col-sm-12">
                                    <input type="submit" class="btn btn-lg btn-info" value="Save">
                                </div>

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
@section('custom-js')
    <script src="{{asset('public/adminassets/js/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{asset('public/adminassets/js/plugins/select2/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        jQuery('#certificate-request').validate();
        jQuery('.js-select2').select2();

    </script>
@endsection