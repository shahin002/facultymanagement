@extends('users.student.master')

@section('title')
    Certificate Request
@endsection
@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Certificate Request List</strong><a class="btn btn-primary pull-right"
                                                                href="{{route('student.create.certificate.request')}}">Add
                        Request</a>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-bordered display" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Certificate</th>
                                <th>Money(tk)</th>
                                <th>Received No</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($request_list as $c_request)
                                <tr>
                                    <td>{{$c_request->certificate->name}}</td>
                                    <td>{{$c_request->money_amount}}</td>
                                    <td>{{$c_request->received_number}}</td>
                                    <td>{{$c_request->status==1?'Requested':($c_request->status==2?'Approved':'Downloaded')}}</td>

                                    <td class="text-center">
                                        @if($c_request->status==2)
                                            <a href="{{route('student.download.certificate',$c_request->id)}}"
                                               class="btn btn-primary" data-toggle="tooltip"
                                               title="Download"><i class="si si-cloud-download"></i></a>
                                        @endif
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection