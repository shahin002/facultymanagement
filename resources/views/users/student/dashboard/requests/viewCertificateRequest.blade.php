<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">View Certificate Request Details</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">

                <div class="form-group col-sm-6">
                    <label>Student Name</label>
                    <p>{{$certificateRequest->student->name}}</p>
                </div>
                <div class="form-group col-sm-6">
                    <label>Certificate</label>
                    <p>{{$certificateRequest->certificate->name}}</p>
                </div>
                <div class="form-group col-sm-6">
                    <label>Money Amount</label>
                    <p>{{$certificateRequest->money_amount}}</p>
                </div>
                <div class="form-group col-sm-6">
                    <label>Received Number</label>
                    <p>{{$certificateRequest->received_number}}</p>
                </div>

                <div class="form-group col-sm-6">
                    <label>Comment</label>
                    <p>{{$certificateRequest->comment}}</p>
                </div>
                <div class="form-group col-sm-6">
                    <label>Status</label>
                    <p>{{$certificateRequest->status==1?'Requested':($certificateRequest->status==2?'Approved':'Downloaded')}}</p>
                </div>
                <div class="form-group col-sm-6">
                    <label>Student Urgent Contact</label>
                    <p>{{$certificateRequest->student->phone_number}}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div>