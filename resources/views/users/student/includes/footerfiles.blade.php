<script src="{{asset('public/adminassets/js/core/jquery.min.js')}}"></script>
<script src = "{{asset('public/adminassets/js/core/bootstrap.min.js')}}" ></script>
<script src = "{{asset('public/adminassets/js/core/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('public/adminassets/js/core/jquery.scrollLock.min.js')}}"></script>
<script src="{{asset('public/adminassets/js/core/jquery.appear.min.js')}}"></script>
<script src="{{asset('public/adminassets/js/core/jquery.countTo.min.js')}}"></script>
<script src="{{asset('public/adminassets/js/core/jquery.placeholder.min.js')}}"></script>
<script src="{{asset('public/adminassets/js/core/js.cookie.min.js')}}"></script>
<script src="{{asset('public/adminassets/js/app.js')}}"></script>

<script src="{{asset('public/adminassets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/adminassets/js/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('public/adminassets/js/plugins/bootstrap-notify/bootstrap-notify.min.js')}}"></script>
{{--<script src="{{asset('public/adminassets/js/custom-script.js')}}"></script>--}}