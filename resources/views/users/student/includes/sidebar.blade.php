<!-- Sidebar -->
<nav id="sidebar">
    <!-- Sidebar Scroll Container -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
        <div class="sidebar-content">
            <!-- Side Header -->
            <div class="side-header side-content bg-white-op">
                <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button"
                        data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times"></i>
                </button>
                <!-- Themes functionality initialized in App() -> uiHandleTheme() -->
                <a class="h5 text-white" href="{{route('student.home')}}">
                    <span class="h5 text-primary font-w600">Student</span> <span class="h5 font-w600 sidebar-mini-hide"></span>
                </a>
            </div>
            <!-- END Side Header -->

            <!-- Side Content -->
            <div class="side-content">
                <ul class="nav-main">
                    <li>
                        <a href="{{route('student.home')}}"><i class="si si-speedometer"></i><span
                                    class="sidebar-mini-hide">Dashboard</span></a>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-rocket"></i><span
                                    class="sidebar-mini-hide">Enrollment</span></a>
                        <ul>
                            <li><a href="{{route('student.enroll')}}">Enroll Now</a> </li>
                            <li><a href="{{route('student.enrolled')}}">View Enroll</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-layers"></i><span
                                    class="sidebar-mini-hide">Add Money Receipt</span></a>
                        <ul>
                            <li><a href="{{route('add.courseReceipt')}}">Course</a> </li>
                            <li><a href="{{route('add.hallReceipt')}}">Hall</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-bag"></i><span
                                    class="sidebar-mini-hide">Admit Card</span></a>
                        <ul>
                            <li><a href="{{route('viewAdmit.student')}}">View Admit</a> </li>
                            <li><a href="{{route('downloadAdmit.student')}}">Download Admit Card</a> </li>
                        </ul>
                    </li>


                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-bag"></i>
                            <span class="sidebar-mini-hide">Request Certificates</span></a>
                        <ul>
                            <li><a href="{{route('student.create.certificate.request')}}">Add Request</a> </li>
                            <li><a href="{{route('student.manage.certificat.requests')}}">Manage Requests</a> </li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!-- END Side Content -->
        </div>
        <!-- Sidebar Content -->
    </div>
    <!-- END Sidebar Scroll Container -->
</nav>
<!-- END Sidebar -->