<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-focus"> <!--<![endif]-->
<head>
    {{--Header Files--}}
    @include('users.student.includes.headerfiles')
    {{--End Header Files--}}

</head>
<body>
{{-- Page Container --}}
<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">

    {{--Sidebar--}}
    @include('users.student.includes.sidebar')
    {{--END Sidebar--}}

    {{-- Header --}}
    @include('users.student.includes.header')
    {{--END Header--}}

    {{--Main Container--}}
    <main id="main-container">
        @yield('maincontent')
    </main>
    {{--END Main Container--}}

    {{--Footer--}}
    @include('users.student.includes.footer')
    {{--END Footer--}}
</div>
{{-- END Page Container --}}


{{--OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js --}}
{{--Footer Files--}}
@include('users.student.includes.footerfiles')
@include('users.student.includes.notifications')
{{--End Footer Files--}}
@yield('custom-js')
</body>
</html>