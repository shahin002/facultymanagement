@extends('users.teacher.master')

@section('title')
    Dashboard
@endsection
@section('maincontent')
    <!-- Page Header -->
    <div class="content bg-image overflow-hidden"
         style="background-image: url('{{asset('public/adminassets/img/photos/photo3@2x.jpg')}}');">
        <div class="push-50-t push-15">
            <h1 class="h2 text-white animated zoomIn">Dashboard</h1>
            <h2 class="h5 text-white-op animated zoomIn">Welcome {{auth('teacher')->user()->name}}</h2>
        </div>
    </div>
    <!-- END Page Header -->



    @if(!auth('teacher')->user()->signature)
        <div class="content">
            <div class="block">
                <div class="block-content">
                    <div class="alert alert-danger">
                        <h3>You haven't Upload your <strong>Signature</strong> yet!<span class="pull-right"><a
                                        href="{{route('signatureForm.teacher')}}" class="btn btn-success ">Upload It</a></span>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('custom-styles')

@endsection
@section('custom-js')
@endsection