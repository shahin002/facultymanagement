@extends('users.teacher.master')

@section('title')
    Students Marks ({{ucfirst($course->name.'('.$course->code.')')}})
@endsection
@section('maincontent')

    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>Students Marks ({{ucfirst($course->name.'('.$course->code.')')}})</strong>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <form class="table-responsive" method="post"
                              action="{{route('prev.teacher.mark.student.theory',$course->id)}}">
                            {{csrf_field()}}
                            @if($courseEnrolls->count()>0)
                                <button type="submit" class="btn btn-primary pull-right">Insert</button>
                            @endif
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Attendence(10)</th>
                                    <th>Mid Term(20)</th>
                                    <th>Final(70)</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($fi=array())
                                @php($repeat=array())
                                @if($courseEnrolls->count()>0)
                                    @foreach($courseEnrolls as $enroll)
                                        @if($enroll->semister !=$course->semister)
                                            @php($fi[]=$enroll)
                                            @continue
                                        @elseif($enroll->student_status !=1)
                                            @php($repeat[]=$enroll)
                                            @continue
                                        @endif
                                        <tr>
                                            <td>{{$enroll->student->roll_no}}</td>
                                            <td>{{$enroll->student->name}}</td>
                                            <td><input type="number" step=".1"
                                                       value="{{@$enroll->marks->assignment?$enroll->marks->assignment:0}}"
                                                       name="assignment[{{$enroll->student->id}}]" max="10" min="0"
                                                       required>
                                            </td>
                                            <td><input type="number" step=".1"
                                                       value="{{@$enroll->marks->mid?$enroll->marks->mid:0}}"
                                                       name="mid[{{$enroll->student->id}}]" max="20" min="0" required>
                                            </td>
                                            <td><input type="number" step=".1"
                                                       value="{{@$enroll->marks->final?$enroll->marks->final:0}}"
                                                       name="final[{{$enroll->student->id}}]" max="70" min="0" required>
                                            </td>
                                        </tr>
                                    @endforeach
                                    @foreach($repeat as $enroll)
                                        <tr>
                                            <td>{{$enroll->student->roll_no}}</td>
                                            <td>{{$enroll->student->name}}</td>
                                            <td><input type="number" step=".1"
                                                       value="{{@$enroll->marks->assignment?$enroll->marks->assignment:0}}"
                                                       name="assignment[{{$enroll->student->id}}]" max="10" min="0"
                                                       required>
                                            </td>
                                            <td><input type="number" step=".1"
                                                       value="{{@$enroll->marks->mid?$enroll->marks->mid:0}}"
                                                       name="mid[{{$enroll->student->id}}]" max="20" min="0" required>
                                            </td>
                                            <td><input type="number" step=".1"
                                                       value="{{@$enroll->marks->final?$enroll->marks->final:0}}"
                                                       name="final[{{$enroll->student->id}}]" max="70" min="0" required>
                                            </td>
                                        </tr>
                                    @endforeach
                                    @foreach($fi as $enroll)
                                        <tr>
                                            <td>{{$enroll->student->roll_no}}</td>
                                            <td>{{$enroll->student->name}}</td>
                                            <td><input type="number" step=".1"
                                                       value="{{@$enroll->marks->assignment?$enroll->marks->assignment:0}}"
                                                       name="assignment[{{$enroll->student->id}}]" max="10" min="0"
                                                       required>
                                            </td>
                                            <td><input type="number" step=".1"
                                                       value="{{@$enroll->marks->mid?$enroll->marks->mid:0}}"
                                                       name="mid[{{$enroll->student->id}}]" max="20" min="0" required>
                                            </td>
                                            <td><input type="number" step=".1"
                                                       value="{{@$enroll->marks->final?$enroll->marks->final:0}}"
                                                       name="final[{{$enroll->student->id}}]" max="70" min="0" required>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="text-center">No Student Found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            @if($courseEnrolls->count()>0)
                                <button type="submit" class="btn btn-primary pull-right">Insert</button>
                            @endif
                        </form><!-- /.table-responsive -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection