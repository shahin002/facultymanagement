@extends('users.teacher.master')

@section('title')
    Semester {{$semester_id}} Assigned Courses
@endsection
@section('maincontent')
    <div class="content bg-gray-lighter hidden-print">
        <div class="row items-push">
            <div class="col-sm-12">
                <h1 class="page-heading text-center font-w600">
                    <strong>You Assigned Theoritical Courses of Semester {{$semester_id}}</strong>
                </h1>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="content content-narrow">
        <div class="block">
            <div class="block-content">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Course Name</th>
                                <th>Course Code</th>
                                <th>Credit</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($courses as $temp)
                                @php($course=$temp->course)
                                <tr>
                                    <td>{{$course->name}}</td>
                                    <td>{{$course->code}}</td>
                                    <td>{{$course->credit}}</td>
                                    <td>{{$temp->status==0?'Not Submitted':($temp->status==1?'Submitted':($temp->status==2?'Approved':'Final'))}}</td>
                                    <td>
                                        @if($course->courseEnrolls->count()>0)
                                            @if($temp->status>1)
                                                <a href="{{route('prev.teacher.mark.student.theory.view',$course->id)}}"
                                                   class="btn btn-primary">View
                                                    Marks</a>
                                            @else
                                                <a href="{{route('prev.teacher.mark.student.theory',$course->id)}}"
                                                   class="btn btn-primary">Insert Marks</a>
                                            @endif
                                            @if($temp->status==0)
                                                <a href="{{route('prev.teacher.submit.marks.review',$course->id)}}"
                                                   class="btn btn-primary submit-alert">Submit
                                                    Marks</a>
                                            @endif
                                        @else
                                            No Student Enrolled
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" align="center">You have not assigned any theoritical subject in this
                                        semester
                                    </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
@section('custom-js')
    <script>
        jQuery('.submit-alert').click(function (e) {
            var href = $(this).attr('href');
            e.preventDefault();
            swal({
                title: 'Are you sure?',
                text: 'To Submit Students Marks To Dept Chairman',
                showCancelButton: true,
                confirmButtonText: 'Yes!',
                closeOnConfirm: false,
                html: false
            }, function () {
                window.location.href = href;
            });

        })
    </script>
@endsection