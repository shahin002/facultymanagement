<!-- Sidebar -->
<nav id="sidebar">
    <!-- Sidebar Scroll Container -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
        <div class="sidebar-content">
            <!-- Side Header -->
            <div class="side-header side-content bg-white-op">
                <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button"
                        data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times"></i>
                </button>
                <!-- Themes functionality initialized in App() -> uiHandleTheme() -->
                <a class="h5 text-white" href="{{route('teacher.home')}}">
                    <span class="h5 text-primary font-w600">Teacher</span> <span
                            class="h5 font-w600 sidebar-mini-hide"></span>
                </a>
            </div>
            <!-- END Side Header -->

            <!-- Side Content -->
            <div class="side-content">
                <ul class="nav-main">
                    <li>
                        <a href="{{route('teacher.home')}}"><i class="si si-speedometer"></i><span
                                    class="sidebar-mini-hide">Dashboard</span></a>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-rocket"></i><span
                                    class="sidebar-mini-hide">Assigned Courses</span></a>
                        <ul>
                            @if(date('n')<7)
                                <li><a href="{{route('teacher.assigned.theory.courses',1)}}">Semester 1 Theory</a></li>
                                <li><a href="{{route('teacher.assigned.practical.courses',1)}}">Semester 1 Practical</a>
                                </li>
                                <li><a href="{{route('teacher.assigned.theory.courses',3)}}">Semester 3 Theory</a></li>
                                <li><a href="{{route('teacher.assigned.practical.courses',3)}}">Semester 3 Practical</a>
                                </li>
                                <li><a href="{{route('teacher.assigned.theory.courses',5)}}">Semester 5 Theory</a></li>
                                <li><a href="{{route('teacher.assigned.practical.courses',5)}}">Semester 5 Practical</a>
                                </li>
                                <li><a href="{{route('teacher.assigned.theory.courses',7)}}">Semester 7 Theory</a></li>
                                <li><a href="{{route('teacher.assigned.practical.courses',7)}}">Semester 7 Practical</a>
                                </li>
                            @else
                                <li><a href="{{route('teacher.assigned.theory.courses',2)}}">Semester 2 Theory</a></li>
                                <li><a href="{{route('teacher.assigned.practical.courses',2)}}">Semester 2 Practical</a>
                                </li>
                                <li><a href="{{route('teacher.assigned.theory.courses',4)}}">Semester 4 Theory</a></li>
                                <li><a href="{{route('teacher.assigned.practical.courses',4)}}">Semester 4 Practical</a>
                                </li>
                                <li><a href="{{route('teacher.assigned.theory.courses',6)}}">Semester 6 Theory</a></li>
                                <li><a href="{{route('teacher.assigned.practical.courses',6)}}">Semester 6 Practical</a>
                                </li>
                                <li><a href="{{route('teacher.assigned.theory.courses',8)}}">Semester 8 Theory</a></li>
                                <li><a href="{{route('teacher.assigned.practical.courses',8)}}">Semester 8 Practical</a>
                                </li>
                            @endif
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-rocket"></i><span
                                    class="sidebar-mini-hide">Prev. Courses</span></a>
                        <ul>
                            @if(date('n')>6)
                                <li><a href="{{route('prev.teacher.assigned.theory.courses',1)}}">Semester 1 Theory</a></li>
                                <li><a href="{{route('prev.teacher.assigned.practical.courses',1)}}">Semester 1 Practical</a>
                                </li>
                                <li><a href="{{route('prev.teacher.assigned.theory.courses',3)}}">Semester 3 Theory</a></li>
                                <li><a href="{{route('prev.teacher.assigned.practical.courses',3)}}">Semester 3 Practical</a>
                                </li>
                                <li><a href="{{route('prev.teacher.assigned.theory.courses',5)}}">Semester 5 Theory</a></li>
                                <li><a href="{{route('prev.teacher.assigned.practical.courses',5)}}">Semester 5 Practical</a>
                                </li>
                                <li><a href="{{route('prev.teacher.assigned.theory.courses',7)}}">Semester 7 Theory</a></li>
                                <li><a href="{{route('prev.teacher.assigned.practical.courses',7)}}">Semester 7 Practical</a>
                                </li>
                            @else
                                <li><a href="{{route('prev.teacher.assigned.theory.courses',2)}}">Semester 2 Theory</a></li>
                                <li><a href="{{route('prev.teacher.assigned.practical.courses',2)}}">Semester 2 Practical</a>
                                </li>
                                <li><a href="{{route('prev.teacher.assigned.theory.courses',4)}}">Semester 4 Theory</a></li>
                                <li><a href="{{route('prev.teacher.assigned.practical.courses',4)}}">Semester 4 Practical</a>
                                </li>
                                <li><a href="{{route('prev.teacher.assigned.theory.courses',6)}}">Semester 6 Theory</a></li>
                                <li><a href="{{route('prev.teacher.assigned.practical.courses',6)}}">Semester 6 Practical</a>
                                </li>
                                <li><a href="{{route('prev.teacher.assigned.theory.courses',8)}}">Semester 8 Theory</a></li>
                                <li><a href="{{route('prev.teacher.assigned.practical.courses',8)}}">Semester 8 Practical</a>
                                </li>
                            @endif
                        </ul>
                    </li>

                </ul>
            </div>
            <!-- END Side Content -->
        </div>
        <!-- Sidebar Content -->
    </div>
    <!-- END Sidebar Scroll Container -->
</nav>
<!-- END Sidebar -->