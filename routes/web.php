<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/student', 'studentController@index')->name('student.home');
//Route::get('/student/register','Auth\StudentRegisterController@showRegistrationForm')->name('student.regidterForm');
//Route::post('/student/register','Auth\StudentRegisterController@register')->name('student.register');
Route::get('/student/login', 'Auth\StudentLoginController@showLoginForm')->name('student.loginForm');
Route::post('/student/login', 'Auth\StudentLoginController@login')->name('student.login');
Route::post('/student/logout', 'Auth\StudentLoginController@logout')->name('student.logout');
Route::get('/student/profile-info', 'studentController@addInfo')->name('student.info');
Route::post('/student/profile-info', 'studentController@saveInfo');
Route::get('/student/enroll', 'studentController@showEnrollForm')->name('student.enrollForm');
Route::post('/student/enroll', 'studentController@saveEnrollForm')->name('student.enroll');
Route::get('/student/enrolled', 'studentController@showEnrolledForm')->name('student.enrolled');
Route::get('/download/enrolled', 'pdfController@downloadEnrollPdf')->name('download.enrolled');
Route::get('student/profile', 'studentController@profileForm')->name('profile.student');
Route::post('student/profile', 'studentController@updateProfile')->name('update.student');
Route::get('student/receipt/course', 'studentController@addCourseReceipt')->name('add.courseReceipt');
Route::post('student/receipt/save', 'studentController@saveCourseReceipt')->name('save.courseMoney');
Route::get('student/receipt/hall', 'studentController@addHallReceipt')->name('add.hallReceipt');
Route::post('student/receipt/hall', 'studentController@saveHallReceipt')->name('save.hallMoney');
Route::get('student/signature', 'studentController@showAddSignatureForm')->name('signatureForm.student');
Route::post('student/signature', 'studentController@addSignature')->name('signature.student');
Route::get('student/image-upload', 'studentController@imageUpload')->name('imageupload.student');
Route::post('student/image-upload', 'studentController@saveImage');
Route::get('student/admit', 'studentController@viewAdmitCard')->name('viewAdmit.student');
Route::get('student/admit/download', 'studentController@downloadAdmit')->name('downloadAdmit.student');
//student certificates requests
Route::get('student/manage-certificate-request', 'studentController@certificateRequests')->name('student.manage.certificat.requests');
Route::get('student/certificate-request/create', 'studentController@createCertificateRequests')->name('student.create.certificate.request');
Route::post('student/certificate-request/create', 'studentController@saveCertificateRequests');
Route::get('student/download/{id}/certificate', 'studentController@downloadCertificateRequest')->name('student.download.certificate');


//end student certificates requests
//user pass reset
Route::get('student/reset', 'studentForgotController@showResetForm');
Route::post('student/email', 'studentForgotController@sendResetLinkEmail');
Route::get('student/reset/{token}', 'studentResetController@showResetForm');
Route::post('student/reset', 'studentResetController@reset');


//dean Officer
Route::get('officer/', 'DeanOfficerController@index')->name('admin.home');
Route::get('officer/login', 'Auth\DeanOfficerLoginController@showLoginForm')->name('admin.login');
Route::post('officer/login', 'Auth\DeanOfficerLoginController@login')->name('admin.login');
Route::post('officer/logout', 'Auth\DeanOfficerLoginController@logout')->name('admin.logout');
Route::get('officer/semister/add', 'DeanOfficerController@showAddSemisterForm')->name('add.semister');
Route::post('officer/semister/add', 'DeanOfficerController@saveEnrollSemister')->name('add.semister');
Route::get('officer/semister/view', 'DeanOfficerController@viewEnrollSemister')->name('view.semister');
Route::get('officer/{semister}/semister', 'DeanOfficerController@updateEnrollSemisterStatus');
Route::post('officer/{semister}/exam', 'DeanOfficerController@setExamDate');
Route::get('officer/{semister}/admit', 'DeanOfficerController@updateAdmitStatus');
Route::get('officer/student/manage', 'DeanOfficerController@manageStudent')->name('manage.student');
Route::get('officer/student/add', 'DeanOfficerController@addStudent')->name('student.addform');
Route::post('officer/student/add', 'DeanOfficerController@saveStudent')->name('student.add');
Route::get('officer/{student}/student', 'DeanOfficerController@viewStudent');
Route::post('update/{student}/student', 'DeanOfficerController@updateStudent');
Route::get('officer/enroll/{semester}/manage', 'DeanOfficerController@manageEnroll')->name('manage.enroll');
Route::get('officer/{enroll}/enroll', 'DeanOfficerController@viewEnroll');
Route::get('edit/{enroll}/enroll', 'DeanOfficerController@viewEditEnrollForm');
Route::post('update/{enroll}/enroll', 'DeanOfficerController@updateEnroll');
Route::get('download/{enroll}/enroll', 'getPdfController@downloadEnrollPdfByAdmin');
Route::get('officer/course/', 'DeanOfficerController@manageCourse')->name('manage.course');
Route::post('officer/course/add', 'DeanOfficerController@saveCourse')->name('add.course');
Route::get('edit/{course}/course', 'DeanOfficerController@viewEditCourseForm');
Route::post('update/{course}/course', 'DeanOfficerController@updateCourse');
Route::get('create/provost', 'DeanOfficerController@showCreateProvostForm')->name('showCreateProvostForm');
Route::post('create/provost', 'DeanOfficerController@createProvost')->name('create.hallOfficer');
Route::get('officer/provost', 'DeanOfficerController@viewProvost')->name('view.hallOfficer');
Route::get('officer/hall/manage', 'DeanOfficerController@manageHall')->name('manage.hall');
Route::get('hall/{hall}/edit', 'DeanOfficerController@showHallEditForm');
Route::post('hall/{hall}/update', 'DeanOfficerController@updateHall');
Route::get('officer/department-list', 'DeanOfficerController@departmentList')->name('department.list');
Route::get('officer/department/create', 'DeanOfficerController@showCreateDepartmentForm')->name('form.department');
Route::post('officer/department/create', 'DeanOfficerController@createDepartment')->name('create.department');
Route::get('officer/account-officers', 'DeanOfficerController@accountOfficerList')->name('account.officers');
Route::get('officer/account/create', 'DeanOfficerController@showCreateAccountForm')->name('form.account');
Route::post('officer/account/create', 'DeanOfficerController@createAccount')->name('create.account');
Route::get('officer/current_courses', 'DeanOfficerController@current_courses')->name('current.courses');
Route::get('officer/{course}/fi-list', 'DeanOfficerController@courseFiList')->name('officer.course.fi');
Route::get('officer/testfail', 'DeanOfficerController@testfail')->name('test.fail');
//Start Teacher section
Route::get('officer/teacher/create', 'DeanOfficerController@teacherCreateForm')->name('dean.create.teacher');
Route::post('officer/teacher/create', 'DeanOfficerController@saveTeacher');
Route::get('officer/teacher/list', 'DeanOfficerController@teacherList')->name('dean.teacher.list');
//End Teacher Section
//Start Certificate/Requests
//certificates
Route::get('officer/manage-certificate', 'DeanOfficerController@manageCertificate')->name('dean.manage.certificate');
Route::get('officer/certificate/create', 'DeanOfficerController@createCertificate')->name('dean.create.certificate');
Route::post('officer/certificate/create', 'DeanOfficerController@saveCertificate');
Route::get('officer/certificate/{id}/view', 'DeanOfficerController@showCertificate')->name('dean.view.certificate');
Route::get('officer/certificate/{id}/edit', 'DeanOfficerController@editCertificate')->name('dean.edit.certificate');
Route::post('officer/certificate/{id}/edit', 'DeanOfficerController@updateCertificate');
Route::get('officer/certificate/{id}/delete', 'DeanOfficerController@deleteCertificate')->name('dean.delete.certificate');
//end certificate
//requests
Route::get('officer/manage-certificate-request', 'DeanOfficerController@certificateRequests')->name('dean.manage.certificat.requests');
Route::get('officer/certificate-request/{id}/view', 'DeanOfficerController@showCertificateRequests')->name('dean.view.certificate.request');
Route::get('officer/certificate-request/{id}/approve', 'DeanOfficerController@approveCertificateRequest')->name('dean.approve.certificate.request');
Route::post('officer/certificate-request/{id}/approve', 'DeanOfficerController@approveCertificateRequestInput');
Route::get('officer/certificate-request/{id}/delete', 'DeanOfficerController@deleteCertificateRequest')->name('dean.delete.certificate.request');
//end requests
//End Certificate/Requests
/*Start CourseTeachers by Semester*/
Route::get('officer/theoryCourseTeachers/{semesterId}', 'DeanOfficerController@theoryCourseTeacherBySemesterId');
Route::get('officer/practicalCourseTeachers/{semesterId}', 'DeanOfficerController@practicalCourseTeacherBySemesterId');


//Student marks Accept-reject
Route::get('officer/mark/{course}/students', 'DeanOfficerController@viewStudentsMark')->name('officer.mark.student.view');
Route::get('officer/mark/{course}/download', 'DeanOfficerController@downloadStudentsMark')->name('officer.mark.student.download');
Route::get('officer/cancel-student-marks-review/{course}', 'DeanOfficerController@cancelStudentsMarkReview')->name('officer.cancel.marks.review');
Route::get('officer/approve-student-marks-review/{course}', 'DeanOfficerController@approveStudentsMarkReview')->name('officer.approve.marks.review');


Route::get('officer/download/{semesterId}/theoretical', 'DeanOfficerController@downloadTheoreticalPdf')->name('officer.download.theoreticalTeacher');
Route::get('officer/download/{semesterId}/internal', 'DeanOfficerController@downloadInternalPdf')->name('officer.download.internalTeacher');
Route::get('officer/download/{semesterId}/external', 'DeanOfficerController@downloadExternalPdf')->name('officer.download.externalTeacher');
/*End CourseTeachers by Semester*/

/*Start Prev CourseTeachers by Semester*/
Route::get('prev/officer/theoryCourseTeachers/{semesterId}', 'DeanOfficerController@prevtheoryCourseTeacherBySemesterId');
Route::get('prev/officer/practicalCourseTeachers/{semesterId}', 'DeanOfficerController@prevpracticalCourseTeacherBySemesterId');


//Student Prev marks Accept-reject
Route::get('prev/officer/mark/{course}/students', 'DeanOfficerController@prevviewStudentsMark')->name('prev.officer.mark.student.view');
Route::get('prev/officer/mark/{course}/download', 'DeanOfficerController@prevdownloadStudentsMark')->name('prev.officer.mark.student.download');
Route::get('prev/officer/cancel-student-marks-review/{course}', 'DeanOfficerController@prevcancelStudentsMarkReview')->name('prev.officer.cancel.marks.review');
Route::get('prev/officer/approve-student-marks-review/{course}', 'DeanOfficerController@prevapproveStudentsMarkReview')->name('prev.officer.approve.marks.review');


Route::get('prev/officer/download/{semesterId}/theoretical', 'DeanOfficerController@prevdownloadTheoreticalPdf')->name('prev.officer.download.theoreticalTeacher');
Route::get('prev/officer/download/{semesterId}/internal', 'DeanOfficerController@prevdownloadInternalPdf')->name('prev.officer.download.internalTeacher');
Route::get('prev/officer/download/{semesterId}/external', 'DeanOfficerController@prevdownloadExternalPdf')->name('prev.officer.download.externalTeacher');
/*End CourseTeachers by Semester*/

Route::get('officer/profile', 'DeanOfficerController@profileForm')->name('profile.officer');
Route::post('officer/profile', 'DeanOfficerController@updateProfile')->name('update.officer');
Route::get('officer/edit-profile', 'DeanOfficerController@editProfileForm')->name('edit.profile.officer');
Route::post('officer/edit-profile', 'DeanOfficerController@updateOfficerProfile');
Route::get('officer/signature', 'DeanOfficerController@showAddSignatureForm')->name('signatureForm.admin');
Route::post('officer/signature', 'DeanOfficerController@addSignature')->name('signature.admin');

// Extra Routes
Route::get('officer/dean/update-info', 'DeanOfficerController@deanInfo')->name('officer.dean.info');
Route::post('officer/dean/update-info', 'DeanOfficerController@updateDeanInfo');
Route::get('officer/exam-controller/update-info', 'DeanOfficerController@examControllerInfo')->name('officer.exam_controller.info');
Route::post('officer/exam-controller/update-info', 'DeanOfficerController@updateExamControllerInfo');
// End Extra Routes

//pass reset
Route::get('officer/reset', 'adminForgotController@showResetForm');
Route::post('officer/email', 'adminForgotController@sendResetLinkEmail');
Route::get('officer/reset/{token}', 'adminResetController@showResetForm');
Route::post('officer/reset', 'adminResetController@reset');


//account user
Route::get('account/', 'AccountController@index')->name('account.home');
Route::get('account/enroll/{semester}/manage', 'AccountController@manageEnroll')->name('account.manage.enroll');
Route::get('account/login', 'Auth\AccountLoginController@showLoginForm')->name('account.login');
Route::get('account/enroll/manage', 'Auth\AccountLoginController@showLoginForm')->name('account.login');
Route::post('account/login', 'Auth\AccountLoginController@login')->name('account.login');
Route::post('account/logout', 'Auth\AccountLoginController@logout')->name('account.logout');
Route::get('account/{enroll}/enroll', 'AccountController@viewEditEnrollForm');
Route::get('enroll/{enroll}/change', 'AccountController@changeEnrollStatus');
Route::get('account/profile', 'AccountController@profileForm')->name('profile.account');
Route::post('account/profile', 'AccountController@updateProfile')->name('update.account');
Route::get('account/signature', 'AccountController@showAddSignatureForm')->name('signatureForm.account');
Route::post('account/signature', 'AccountController@addSignature')->name('signature.account');
//acc pass reset
Route::get('account/reset', 'AccountForgotController@showResetForm');
Route::post('account/email', 'AccountForgotController@sendResetLinkEmail');
Route::get('account/reset/{token}', 'AccountResetController@showResetForm');
Route::post('account/reset', 'AccountResetController@reset');

//hall provost
Route::get('provost/', 'ProvostController@index')->name('provost.home');
Route::get('provost/enroll/{semester}/manage', 'ProvostController@manageEnroll')->name('provost.manage.enroll');
Route::get('provost/login', 'Auth\ProvostLoginController@showLoginForm')->name('provost.login');
Route::post('provost/login', 'Auth\ProvostLoginController@login')->name('provost.login');
Route::post('provost/logout', 'Auth\ProvostLoginController@logout')->name('provost.logout');
Route::get('provost/profile', 'ProvostController@profileForm')->name('profile.provost');
Route::post('provost/profile', 'ProvostController@updateProfile')->name('update.provost');
Route::get('provost/{enroll}/enroll', 'ProvostController@viewEditEnrollForm');
Route::get('provost/{enroll}/change', 'ProvostController@changeEnrollStatus');
Route::get('provost/students', 'ProvostController@allStudents')->name('all.students');
Route::get('provost/signature', 'ProvostController@showAddSignatureForm')->name('signatureForm.provost');
Route::post('provost/signature', 'ProvostController@addSignature')->name('signature.provost');
//provost pass reset
Route::get('provost/reset', 'ProvostForgotController@showResetForm');
Route::post('provost/email', 'ProvostForgotController@sendResetLinkEmail');
Route::get('provost/reset/{token}', 'ProvostResetController@showResetForm');
Route::post('provost/reset', 'ProvostResetController@reset');


//department chairman
Route::get('chairman/', 'DepartmentChairmanController@index')->name('chairman.home');
Route::get('chairman/profile', 'DepartmentChairmanController@profileForm')->name('profile.chairman');
Route::post('chairman/profile', 'DepartmentChairmanController@updateProfile');
Route::get('chairman/login', 'Auth\DepartmentChairmanLoginController@showLoginForm')->name('chairman.login');
Route::post('chairman/login', 'Auth\DepartmentChairmanLoginController@login')->name('chairman.login');
Route::post('chairman/logout', 'Auth\DepartmentChairmanLoginController@logout')->name('chairman.logout');
Route::post('chairman/add/theoretical', 'DepartmentChairmanController@addTheoriticalTeacher')->name('add.theoriticalTeacher');
Route::get('chairman/practical', 'DepartmentChairmanController@addPracticalTeacherForm')->name('form.practicalTeacher');
Route::post('chairman/add/practical', 'DepartmentChairmanController@addPracticalTeacher')->name('add.practicalTeacher');
Route::get('chairman/view/internal', 'DepartmentChairmanController@viewIntPracticalTeacher')->name('view.intPracticalTeacher');
Route::get('chairman/view/external', 'DepartmentChairmanController@viewExtPracticalTeacher')->name('view.extPracticalTeacher');
Route::get('chairman/view/theoretical', 'DepartmentChairmanController@viewTheoreticalTeacher')->name('view.theoreticalTeacher');
Route::get('chairman/download/theoretical', 'DepartmentChairmanController@downloadTheoreticalPdf')->name('download.theoreticalTeacher');
Route::get('chairman/download/internal', 'DepartmentChairmanController@downloadInternalPdf')->name('download.internalTeacher');
Route::get('chairman/download/external', 'DepartmentChairmanController@downloadExternalPdf')->name('download.externalTeacher');

//student marks

Route::get('chairman/assigned/{semester}/t-courses', 'DepartmentChairmanController@chairmanAssignedTheoryCourses')->name('chairman.assigned.theory.courses');
Route::get('chairman/assigned/{semester}/p-courses', 'DepartmentChairmanController@chairmanAssignedPracticalCourses')->name('chairman.assigned.practical.courses');
Route::get('chairman/mark/{course}/students', 'DepartmentChairmanController@viewStudentsMark')->name('chairman.mark.student.view');
Route::get('chairman/cancel-student-marks-review/{course}', 'DepartmentChairmanController@cancelStudentsMarkReview')->name('chairman.cancel.marks.review');
Route::get('chairman/approve-student-marks-review/{course}', 'DepartmentChairmanController@approveStudentsMarkReview')->name('chairman.approve.marks.review');

//End Student marks

//Prev student marks

Route::get('prev/chairman/assigned/{semester}/t-courses', 'DepartmentChairmanController@prevchairmanAssignedTheoryCourses')->name('prev.chairman.assigned.theory.courses');
Route::get('prev/chairman/assigned/{semester}/p-courses', 'DepartmentChairmanController@prevchairmanAssignedPracticalCourses')->name('prev.chairman.assigned.practical.courses');
Route::get('prev/chairman/mark/{course}/students', 'DepartmentChairmanController@prevviewStudentsMark')->name('prev.chairman.mark.student.view');
Route::get('prev/chairman/cancel-student-marks-review/{course}', 'DepartmentChairmanController@prevcancelStudentsMarkReview')->name('prev.chairman.cancel.marks.review');
Route::get('prev/chairman/approve-student-marks-review/{course}', 'DepartmentChairmanController@prevapproveStudentsMarkReview')->name('prev.chairman.approve.marks.review');

//End Prev Student marks

Route::get('chairman/signature', 'DepartmentChairmanController@showAddSignatureForm')->name('signatureForm.chairman');
Route::post('chairman/signature', 'DepartmentChairmanController@addSignature');

//chairman pass reset
Route::get('chairman/reset', 'ChairmanForgotController@showResetForm');
Route::post('chairman/email', 'ChairmanForgotController@sendResetLinkEmail');
Route::get('chairman/reset/{token}', 'ChairmanResetController@showResetForm');
Route::post('chairman/reset', 'ChairmanResetController@reset');


// teacher

Route::get('teacher/', 'TeacherController@index')->name('teacher.home');
Route::get('teacher/signature', 'TeacherController@showAddSignatureForm')->name('signatureForm.teacher');
Route::post('teacher/signature', 'TeacherController@addSignature')->name('signature.teacher');
Route::get('teacher/assigned/{semester}/t-courses', 'TeacherController@teacherAssignedTheoryCourses')->name('teacher.assigned.theory.courses');
Route::get('teacher/assigned/{semester}/p-courses', 'TeacherController@teacherAssignedPracticalCourses')->name('teacher.assigned.practical.courses');

Route::get('teacher/mark/{course}/students', 'TeacherController@teacherMarkStudent')->name('teacher.mark.student.theory');
Route::post('teacher/mark/{course}/students', 'TeacherController@saveTeacherMarkStudent');
Route::get('teacher/mark/{course}/students/view', 'TeacherController@teacherMarkStudentView')->name('teacher.mark.student.theory.view');
Route::get('teacher/submit-marks-for-review/{course}', 'TeacherController@submitMarksForReview')->name('teacher.submit.marks.review');

//Previous Semester Teacher
Route::get('prev/teacher/assigned/{semester}/t-courses', 'TeacherController@prevTeacherAssignedTheoryCourses')->name('prev.teacher.assigned.theory.courses');
Route::get('prev/teacher/assigned/{semester}/p-courses', 'TeacherController@prevTeacherAssignedPracticalCourses')->name('prev.teacher.assigned.practical.courses');
Route::get('prev/teacher/mark/{course}/students', 'TeacherController@prevTeacherMarkStudent')->name('prev.teacher.mark.student.theory');
Route::post('prev/teacher/mark/{course}/students', 'TeacherController@prevSaveTeacherMarkStudent');
Route::get('prev/teacher/mark/{course}/students/view', 'TeacherController@prevTeacherMarkStudentView')->name('prev.teacher.mark.student.theory.view');
Route::get('prev/teacher/submit-marks-for-review/{course}', 'TeacherController@prevSubmitMarksForReview')->name('prev.teacher.submit.marks.review');
//End Previous Semester Teacher
Route::get('teacher/login', 'Auth\TeacherLoginController@showLoginForm')->name('teacher.login');
Route::post('teacher/login', 'Auth\TeacherLoginController@login');
Route::post('teacher/logout', 'Auth\TeacherLoginController@logout')->name('teacher.logout');
Route::get('teacher/profile', 'TeacherController@profileForm')->name('profile.teacher');
Route::post('teacher/profile', 'TeacherController@updateProfile');

// end teacher

//teacher pass reset
Route::get('teacher/reset', 'TeacherForgotController@showResetForm');
Route::post('teacher/email', 'TeacherForgotController@sendResetLinkEmail');
Route::get('teacher/reset/{token}', 'TeacherResetController@showResetForm');
Route::post('teacher/reset', 'TeacherResetController@reset');

//End Prev Student marks




//form validator
Route::get('/getcourse', 'PublicController@getCourses')->name('get.course');
Route::post('/check/studentemail', 'validatorController@studentEmail')->name('check.studentEmail');
Route::post('/check/roll', 'validatorController@roll')->name('check.roll');
Route::post('/check/reg', 'validatorController@reg')->name('check.reg');
Route::get('/check', 'validatorController@check')->name('check');


